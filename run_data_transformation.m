% starting
clear all; clc; close all;
restoredefaultpath;
addpath('d:\XXXX-XX_software\dynare\4.6_latest\matlab\');
dynare_config;
% path for interpolated series
% addpath ..\subroutines\subroutines_interpol_EMQuilis\;
addpath(genpath('..\subroutines'));
% path for series backcast with Kalman filter and Denton, has to be
% reviewed which procedures we would like to use
addpath('p:\ECB business areas\DGE\DED\Monitoring and Analysis\09. Fiscal Policies (FIP)\FIP platform\interpolation\Guillermo_Codes\Kalman _new\');
addpath('p:\ECB business areas\DGE\DED\Monitoring and Analysis\09. Fiscal Policies (FIP)\FIP platform\interpolation\Guillermo_Codes\Denton vs Cholette\');

%% initialise dseries object
Qdataset = dseries();
Adataset = dseries();


% ************************************************************************************
% Section 1: Importing potential GDP from xls (interpolated in Eviews for
% now and it can't be retrieved from a UNIX database)
% ************************************************************************************
% [num,txt] = xlsread('data_counterfactual.xlsx','data_q_toMatlab_potGDP','a2:by12');
% Qdataset = [Qdataset dseries(num', '1999Q1', txt')];



% ************************************************************************************
% Section 2: Retrieving series from UNIX databases
% ************************************************************************************
%
% AVAILABLE SERIES (* indicates that the series is downloaded and not derived)
% - lr(real)    cv(current)   df(deflator)   pr(Ratio)   qq(pctLogDiff)  qq(pctLogDiffPc)  NAME 
% - POP_lr*     .             .              POP_pr      P0P_qq          .                 population (to derive per capita series)
% - potGDP_lr*  .             .              .           .               .                 potential GDP
% - GDP_lr*     GDP_cv*       GDP_df         GDP_pr (OG) .               .                 actual GDP
% - gcons_lr    .             .              gcons_pr    .               .                 government consumption
% - ginv_lr     ginv_cv       .              ginv_pr     .               .                 government investment
% - tinv_lr     tinv_cv       tinv_df        .           .               .                 total investment
% - hhcons_lr   hhcons_cv     hhcons_df      .           .               .                 private consumption
% - gtrnsf_lr   gtrnsf_cv     .              gtrnsf_pr   .               .                 government cash transfers
% - .           dtx_cv        .              dtx_pr      .               .                 current taxes on income and wealth, receivable
% - .           dtl_cv        .              dtl_pr      .               .                 current taxes on income and wealth, receivable - labour part proxy
% - .           itx_cv        .              itx_pr      .               .                 taxes on production and imports, receivable
% - .           sct_cv        .              sct_pr      .               .                 net social contributions, receivable
% - .           sch_cv        .              sch_pr      .               .                 net social contributions paid by households, receivable
% - .           scf_cv        .              scf_pr      .               .                 net social contributions paid by firms, receivable
% - .           tcomp_cv      .              .           .               .                 total compensation of employees
% - .           gbb_cv        .              .           .               .                 government budget balance
% - .           inp_cv        .              .           .               .                 interest payments
% - .           gdebt_cv      .              gdebt_pr    .               .                 gross government debt
% - .           .             stn_df         .           .               .                 short term interst rate

% define list of countries
ctry_core_list    = {'AT','BE','DE','FI','FR','NL'};
ctry_core_list_EC = {'AUT','BEL','DEU','FIN','FRA','NLD'};
ctry_peri_list    = {'ES','GR','IE','IT','PT'};
ctry_peri_list_EC = {'ESP','GRC','IRL','ITA','PRT'};
ctry_all_list     = [ctry_core_list ctry_peri_list];
ctry_all_list_EC  = [ctry_core_list_EC ctry_peri_list_EC];
ctry_agg_list     = {'CR', 'PR', 'EA'};
ctry_grand_list   = [ctry_all_list ctry_agg_list 'DR'];

% specify the server and addresses of various databases
server   = '2969@Stat-prod.ecb.de';
db_mna   = '/global/disdatadg/esdb/unrestricted/internal/esa2010.db';
db_qgfs  = '/global/proddatadg/sdpe/production/EFS/common/GFS/GFSQ_SA_ECB.db';
db_ameco = '/global/disdatadg/esdb/unrestricted/other/ameco.db';
db_ecb   = '/global/disdatadg/esdb/unrestricted/ecb/ecb.db';
db_GFS   = '/global/disdatadg/esdb/unrestricted/ecb/gfs.db';
db_out   = '/dissuser/ec_fpo/common/databases/output_database.db';
db_lfs   = '/global/disdatadg/esdb/unrestricted/internal/lfs.db';



% population (ENA.A.N.BE.W0.S1.S1._Z.POP._Z._Z._Z.PS._Z.N) including interpolation
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['ENA.A.N.',ctry_all_list{i},'.W0.S1.S1._Z.POP._Z._Z._Z.PS._Z.N'];
    series_name_target = ['POP_lr_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_mna,'TS',series_name_source,server);

    % cumulating single elements
    Adataset = [Adataset ts.A];
    Adataset = Adataset.rename(series_name_source,series_name_target);
    
    % interpolating the annual into quarterly
    series_date_range = firstdate(ts.A):lastdate(ts.A);
    evalc(['res = bfl(Adataset.POP_lr_' ctry_all_list{i} '(series_date_range).data,2,2,4)']);
    intSer = dseries(res.y, strcat(num2str(series_date_range.time(1)),'Q1') , series_name_target);
    
    % updating the Qdataset
    Qdataset = [Qdataset intSer];
end

% real potential GDP (ame.a.AUT.1.0.0.0.ovgdp*1000) including interpolation
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['AME.A.',ctry_all_list_EC{i},'.1.0.0.0.OVGDP'];
    series_name_target = ['potGDP_lr_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_ameco,'TS',series_name_source,server);

    % cumulating single elements
    Adataset = [Adataset ts.A*1000];
    Adataset = Adataset.rename(series_name_source,series_name_target);
    
    % interpolating the annual into quarterly
    series_date_range = firstdate(ts.A):lastdate(ts.A);
    evalc(['res = bfl(Adataset.potGDP_lr_' ctry_all_list{i} '(series_date_range).data,1,2,4)']);
    intSer = dseries(res.y, strcat(num2str(series_date_range.time(1)),'Q1') , series_name_target);
    
    % updating the Qdataset
    Qdataset = [Qdataset intSer];
end

% nominal GDP (MNA.Q.Y.I8.W2.S1.S1.B.B1GQ._Z._Z._Z.XDC.V.N)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['MNA.Q.Y.',ctry_all_list{i},'.W2.S1.S1.B.B1GQ._Z._Z._Z.EUR.V.N'];
    series_name_target = ['GDP_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_mna,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end
 % adjustment for the outliers in Ireland
 Qdataset.GDP_cv_IE(dates('2016Q4')) = (Qdataset.GDP_cv_IE(dates('2016Q3')).data+Qdataset.GDP_cv_IE(dates('2017Q1')).data)/2;

 % removinging the permanent increase
  temp0 = Qdataset.GDP_cv_IE;
  temp1 = temp0(dates('2015Q1'):lastdate(temp0));
  temp2 = temp0(firstdate(temp0):dates('2015Q1'));
  temp2(dates('2015Q1')) = temp2(dates('2014Q4')).data*(temp2(dates('2014Q4')).data/temp2(dates('2014Q3')).data);
  temp3 = chain(temp2,temp1);
  Qdataset = merge(Qdataset, temp3);
  

% real GDP (MNA.Q.Y.I8.W2.S1.S1.B.B1GQ._Z._Z._Z.XDC.LR.N) 
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['MNA.Q.Y.',ctry_all_list{i},'.W2.S1.S1.B.B1GQ._Z._Z._Z.EUR.LR.N'];
    series_name_target = ['GDP_lr_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_mna,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end
 % adjustment for the outliers in Ireland
 Qdataset.GDP_lr_IE(dates('2016Q4')) = (Qdataset.GDP_lr_IE(dates('2016Q3')).data+Qdataset.GDP_lr_IE(dates('2017Q1')).data)/2;

  % removinging the permanent increase
  temp0 = Qdataset.GDP_lr_IE;
  temp1 = temp0(dates('2015Q1'):lastdate(temp0));
  temp2 = temp0(firstdate(temp0):dates('2015Q1'));
  temp2(dates('2015Q1')) = temp2(dates('2014Q4')).data*(temp2(dates('2014Q4')).data/temp2(dates('2014Q3')).data);
  temp3 = chain(temp2,temp1);
  Qdataset = merge(Qdataset, temp3);

% nominal government consumption (MNA.Q.y.AT.W0.S13.S1.D.P3._Z._Z._T.XDC.V.N)
 for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['MNA.Q.Y.',ctry_all_list{i},'.W0.S13.S1.D.P3._Z._Z._T.XDC.V.N'];
    series_name_target = ['gcons_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_mna,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
 end
 
% % real government consumption (MNA.Q.y.AT.W0.S13.S1.D.P3._Z._Z._T.XDC.LR.N)
%  for i=1:length(ctry_all_list)
%     % defining country names and series names
%     series_name_source = ['MNA.Q.Y.',ctry_all_list{i},'.W0.S13.S1.D.P3._Z._Z._T.XDC.LR.N'];
%     series_name_target = ['gcons_lr_' ctry_all_list{i}];
%     
%     % retrieve the series
%     ts = db2ts(db_mna,'TS',series_name_source,server);
% 
%     % cumulating single elements
%     Qdataset = [Qdataset ts.Q];
%     Qdataset = Qdataset.rename(series_name_source,series_name_target);
%  end

 % gov intermediate consumption (GFS.Q.Y.AT.W0.S13.S1.N.D.P2._Z._Z._T.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list{i},'.W0.S13.S1.N.D.P2._Z._Z._T.XDC._Z.S.V.N._T'];
    series_name_target = ['ginc_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

 % social transfers in kind (GFS.Q.Y.AT.W0.S13.S1.N.D.D632._Z._Z._T.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list{i},'.W0.S13.S1.N.D.D632._Z._Z._T.XDC._Z.S.V.N._T'];
    series_name_target = ['gstm_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% nominal government investment (GFS.Q.Y.AT.W0.S13.S1.N.D.P51G._Z._Z._T.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list{i},'.W0.S13.S1.N.D.P51G._Z._Z._T.XDC._Z.S.V.N._T'];
    series_name_target = ['ginv_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

 % adjustment for the outlier in Italy
 Qdataset.ginv_cv_IT(dates('2002Q4')) = (Qdataset.ginv_cv_IT(dates('2002Q3')).data+Qdataset.ginv_cv_IT(dates('2003Q1')).data)/2;
 Qdataset.ginv_cv_GR(dates('2017Q3')) = (Qdataset.ginv_cv_GR(dates('2017Q2')).data+Qdataset.ginv_cv_GR(dates('2017Q4')).data)/2;
 Qdataset.ginv_cv_ES(dates('2015Q3')) = (Qdataset.ginv_cv_ES(dates('2015Q2')).data+Qdataset.ginv_cv_ES(dates('2015Q4')).data)/2;

% nominal total investment (MNA.Q.Y.DE.W0.S1.S1.D.P51G.N11G._T._Z.EUR.V.N)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['MNA.Q.Y.',ctry_all_list{i},'.W0.S1.S1.D.P51G.N11G._T._Z.EUR.V.N'];
    series_name_target = ['tinv_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_mna,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end
% adjustment for outliers in the Netherlands
% we do not adjust GDP as it seems to be unaffected
 Qdataset.tinv_cv_NL(dates('2015Q1')) = 2/3*Qdataset.tinv_cv_NL(dates('2014Q4')).data+1/3*Qdataset.tinv_cv_NL(dates('2015Q3')).data;
 Qdataset.tinv_cv_NL(dates('2015Q2')) = 1/3*Qdataset.tinv_cv_NL(dates('2014Q4')).data+2/3*Qdataset.tinv_cv_NL(dates('2015Q3')).data;

   % assuming something more reasonable for Ireland given that investment
   % is shooting through the roof, we assume the growht of Austrian
   % investment, another small open economy
  temp1 = Qdataset.tinv_cv_AT(dates('2014Q4'):lastdate(Qdataset.tinv_cv_AT));
  temp2 = Qdataset.tinv_cv_IE(firstdate(Qdataset.tinv_cv_IE):dates('2014Q4'));
  temp3 = chain(temp2,temp1);
  Qdataset = merge(Qdataset, temp3);

% real total investment (MNA.Q.y.AT.W0.S1.S1.D.P5.N1G._T._Z.XDC.LR.N)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['MNA.Q.Y.',ctry_all_list{i},'.W0.S1.S1.D.P5.N1G._T._Z.XDC.LR.N'];
    series_name_target = ['tinv_lr_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_mna,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end
 Qdataset.tinv_lr_NL(dates('2015Q1')) = 2/3*Qdataset.tinv_lr_NL(dates('2014Q4')).data+1/3*Qdataset.tinv_lr_NL(dates('2015Q3')).data;
 Qdataset.tinv_lr_NL(dates('2015Q2')) = 1/3*Qdataset.tinv_lr_NL(dates('2014Q4')).data+2/3*Qdataset.tinv_lr_NL(dates('2015Q3')).data;

    % assuming something more reasonable for Ireland given that investment
   % is shooting through the roof, we assume the growht of Austrian
   % investment, another small open economy
  temp1 = Qdataset.tinv_lr_AT(dates('2014Q4'):lastdate(Qdataset.tinv_lr_AT));
  temp2 = Qdataset.tinv_lr_IE(firstdate(Qdataset.tinv_lr_IE):dates('2014Q4'));
  temp3 = chain(temp2,temp1);
  Qdataset = merge(Qdataset, temp3);

% nominal private consumption (MNA.Q.Y.FI.W0.S1M.S1.D.P31._Z._Z._T.EUR.V.N)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['MNA.Q.Y.',ctry_all_list{i},'.W0.S1M.S1.D.P31._Z._Z._T.EUR.V.N'];
    series_name_target = ['hhcons_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_mna,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% % real private consumption (MNA.Q.Y.FI.W0.S1M.S1.D.P31._Z._Z._T.XDC.LR.N)
% for i=1:length(ctry_all_list)
%     % defining country names and series names
%     series_name_source = ['MNA.Q.Y.',ctry_all_list{i},'.W0.S1M.S1.D.P31._Z._Z._T.XDC.LR.N'];
%     series_name_target = ['hhcons_lr_' ctry_all_list{i}];
%     
%     % retrieve the series
%     ts = db2ts(db_mna,'TS',series_name_source,server);
% 
%     % cumulating single elements
%     Qdataset = [Qdataset ts.Q];
%     Qdataset = Qdataset.rename(series_name_source,series_name_target);
% end

% nominal cash transfers (GFS.Q.Y.AT.W0.S13.S1.N.D.D62._Z._Z._T.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list{i},'.W0.S13.S1.N.D.D62._Z._Z._T.XDC._Z.S.V.N._T'];
    series_name_target = ['gtrnsf_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% Employees (ENA.Q.Y.I8.W2.S1.S1._Z.SAL._Z._T._Z.PS._Z.N) 
% Unit multiplier: Thousands 
for i=1:length(ctry_all_list)
    % defining country names and series names
    if strcmp(ctry_all_list{i},'FR') || strcmp(ctry_all_list{i},'GR') || strcmp(ctry_all_list{i},'PT')
        series_name_source = ['ENA.Q.S.',ctry_all_list{i},'.W2.S1.S1._Z.SAL._Z._T._Z.PS._Z.N'];
    else
        series_name_source = ['ENA.Q.Y.',ctry_all_list{i},'.W2.S1.S1._Z.SAL._Z._T._Z.PS._Z.N'];
    end
    series_name_target = ['templ_lr_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_mna,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% public employment (zzz0_g19_a_n_fr_lng) including interpolation
% Unit multiplier: Thousands
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['ZZZ0_G19_A_N_',ctry_all_list{i},'_LNG'];
    series_name_target = ['gempl_lr_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_out,'TS',series_name_source,server);

    % cumulating single elements
    Adataset = [Adataset ts.A];
    Adataset = Adataset.rename(series_name_source,series_name_target);

    % interpolating the annual into quarterly
    series_date_range_a = dates('1999Y'):dates('2018Y');
    series_date_range_q = dates('1999Q1'):dates('2018Q4');
      
    clear res
    evalc(['res = denton_d_Guillermo(Adataset.gempl_lr_' ctry_all_list{i} '(series_date_range_a),[dseries(0,series_date_range_q)],Qdataset.templ_lr_' ctry_all_list{i} '(series_date_range_q),2,2,4,2,2,0,1);']);
    res = res.set_names(series_name_target);
    Qdataset = [Qdataset res];
end

        
% direct taxes (gfs.q.y.AT.W0.S13.S1.N.C.D5._Z._Z._Z.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list{i},'.W0.S13.S1.N.C.D5._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['dtx_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% indirect taxes (gfs.q.y.AT.W0.S13.S1.N.C.D2._Z._Z._Z.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list{i},'.W0.S13.S1.N.C.D2._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['itx_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% consumption taxes (taxes on products for now, E09.A.N.ES._Z.SZV._Z.N.C.D21._Z._Z._Z.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['E09.A.N.',ctry_all_list{i},'._Z.SZV._Z.N.C.D21._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['taxprod_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_GFS,'TS',series_name_source,server);

    % cumulating single elements
%     Adataset = [Adataset ts.A];
    temp_ygrowth = ygrowth(ts.A);
    temp_ext = dseries(ts.A(dates('2017Y')).data*(1+temp_ygrowth(dates('2017Y')).data), dates('2018Y'), series_name_source);
    ts.A = [ts.A; temp_ext];
    Adataset = [Adataset ts.A];
    Adataset = Adataset.rename(series_name_source,series_name_target);
    
    % interpolating the annual into quarterly
    series_date_range_a = dates('1999Y'):dates('2018Y');
    series_date_range_q = dates('1999Q1'):dates('2018Q4');
      
    clear res
    evalc(['res = denton_d_Guillermo(Adataset.taxprod_cv_' ctry_all_list{i} '(series_date_range_a),[dseries(0,series_date_range_q)],Qdataset.hhcons_cv_' ctry_all_list{i} '(series_date_range_q),1,2,4,2,2,0,1);']);
    res = res.set_names(series_name_target);
    Qdataset = [Qdataset res];
    
%     % interpolating the annual into quarterly
%       series_date_range_a = dates('1999Y'):dates('2018Y');
%       series_date_range_q = dates('1999Q1'):dates('2018Q4');
%       
%       evalc(['res = chowlin(Adataset.taxprod_cv_' ctry_all_list{i} '(series_date_range_a).data,Qdataset.hhcons_cv_' ctry_all_list{i} '(series_date_range_q).data,1,4,1,-1,[0.50 0.999999999 1000])']);
%       intSer = dseries(res.y, strcat(num2str(series_date_range_a.time(1)),'Q1') , series_name_target);
%     
%     % updating the Qdataset
%     Qdataset = [Qdataset intSer];
end

% wages and salaries (MNA.Q.Y.AT.W2.S1.S1.D.D11._Z._T._Z.EUR.V.N)
% Unit multiplier: Millions
for i=1:length(ctry_all_list)
    % defining country names and series names
    if strcmp(ctry_all_list{i},'DE') || strcmp(ctry_all_list{i},'FR')
        series_name_source = ['MNA.Q.S.',ctry_all_list{i},'.W2.S1.S1.D.D11._Z._T._Z.EUR.V.N'];
    else
        series_name_source = ['MNA.Q.Y.',ctry_all_list{i},'.W2.S1.S1.D.D11._Z._T._Z.EUR.V.N'];
    end
    series_name_target = ['tcomp_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_mna,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% household income taxes (E09.A.N.ES._Z.SZV._Z.N.C.D51M._Z._Z._Z.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['E09.A.N.',ctry_all_list{i},'._Z.SZV._Z.N.C.D51M._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['taxhhinc_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_GFS,'TS',series_name_source,server);

    % cumulating single elements
%     Adataset = [Adataset ts.A];
    temp_ygrowth = ygrowth(ts.A);
    temp_ext = dseries(ts.A(dates('2017Y')).data*(1+temp_ygrowth(dates('2017Y')).data), dates('2018Y'), series_name_source);
    ts.A = [ts.A; temp_ext];
    Adataset = [Adataset ts.A];
    Adataset = Adataset.rename(series_name_source,series_name_target);
    
    % interpolating the annual into quarterly
      series_date_range_a = dates('1999Y'):dates('2018Y');
      series_date_range_q = dates('1999Q1'):dates('2018Q4');

    clear res
    evalc(['res = denton_d_Guillermo(Adataset.taxhhinc_cv_' ctry_all_list{i} '(series_date_range_a),[dseries(0,series_date_range_q)],Qdataset.tcomp_cv_' ctry_all_list{i} '(series_date_range_q),1,2,4,2,2,0,1);']);
    res = res.set_names(series_name_target);
    Qdataset = [Qdataset res];
      
%       evalc(['res = chowlin(Adataset.taxhhinc_cv_' ctry_all_list{i} '(series_date_range_a).data,Qdataset.tcomp_cv_' ctry_all_list{i} '(series_date_range_q).data,1,4,1,-1,[0.50 0.999999999 1000])']);
%       intSer = dseries(res.y, strcat(num2str(series_date_range_a.time(1)),'Q1') , series_name_target);
%     
%     % updating the Qdataset
%     Qdataset = [Qdataset intSer];
end

% household SSC (E09.A.N.ES._Z.SZV._Z.N.C.D613._Z._Z._Z.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['E09.A.N.',ctry_all_list{i},'._Z.SZV._Z.N.C.D613._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['sschhinc_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_GFS,'TS',series_name_source,server);

    % cumulating single elements
%     Adataset = [Adataset ts.A];
    temp_ygrowth = ygrowth(ts.A);
    temp_ext = dseries(ts.A(dates('2017Y')).data*(1+temp_ygrowth(dates('2017Y')).data), dates('2018Y'), series_name_source);
    ts.A = [ts.A; temp_ext];
    Adataset = [Adataset ts.A];
    Adataset = Adataset.rename(series_name_source,series_name_target);
    
    % interpolating the annual into quarterly
      series_date_range_a = dates('1999Y'):dates('2018Y');
      series_date_range_q = dates('1999Q1'):dates('2018Q4');

    clear res
    evalc(['res = denton_d_Guillermo(Adataset.sschhinc_cv_' ctry_all_list{i} '(series_date_range_a),[dseries(0,series_date_range_q)],Qdataset.tcomp_cv_' ctry_all_list{i} '(series_date_range_q),1,2,4,2,2,0,1);']);
    res = res.set_names(series_name_target);
    Qdataset = [Qdataset res];

%       evalc(['res = chowlin(Adataset.sschhinc_cv_' ctry_all_list{i} '(series_date_range_a).data,Qdataset.tcomp_cv_' ctry_all_list{i} '(series_date_range_q).data,1,4,1,-1,[0.50 0.999999999 1000])']);
%       intSer = dseries(res.y, strcat(num2str(series_date_range_a.time(1)),'Q1') , series_name_target);
%     
%     % updating the Qdataset
%     Qdataset = [Qdataset intSer];
end

% employers actual SSC (E09.A.N.ES._Z.SZV._Z.N.C.D611._Z._Z._Z.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    
    % defining country names and series names
    series_name_source = ['E09.A.N.',ctry_all_list{i},'._Z.SZV._Z.N.C.D611._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['sscfirma_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_GFS,'TS',series_name_source,server);

    % cumulating single elements
%     Adataset = [Adataset ts.A];
    temp_ygrowth = ygrowth(ts.A);
    temp_ext = dseries(ts.A(dates('2017Y')).data*(1+temp_ygrowth(dates('2017Y')).data), dates('2018Y'), series_name_source);
    ts.A = [ts.A; temp_ext];
    Adataset = [Adataset ts.A];
    Adataset = Adataset.rename(series_name_source,series_name_target);
    
end

% employers imputed SSC (E09.A.N.ES._Z.SZV._Z.N.C.D612._Z._Z._Z.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    
    % defining country names and series names
    series_name_source = ['E09.A.N.',ctry_all_list{i},'._Z.SZV._Z.N.C.D612._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['sscfirmi_cv_' ctry_all_list{i}];
    
    % the data are not available for Finland
    if strcmp(ctry_all_list{i},'FI')
        ts.A = dseries(0,dates('1999Y'):dates('2018Y'),series_name_source);
    else
    % retrieve the series
    ts = db2ts(db_GFS,'TS',series_name_source,server);
    end
    
    % cumulating single elements
%     Adataset = [Adataset ts.A];
    temp_ygrowth = ygrowth(ts.A);
    temp_ext = dseries(ts.A(dates('2017Y')).data*(1+temp_ygrowth(dates('2017Y')).data), dates('2018Y'), series_name_source);
    ts.A = [ts.A; temp_ext];
    Adataset = [Adataset ts.A];
    Adataset = Adataset.rename(series_name_source,series_name_target);
 
end

% employers actual and imputed SSC (E09.A.N.ES._Z.SZV._Z.N.C.D612._Z._Z._Z.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    series_name_target = ['sscfirm_cv_' ctry_all_list{i}];
    evalc(['Adataset.sscfirm_cv_' ctry_grand_list{i} '=Adataset.sscfirma_cv_' ctry_grand_list{i} '-Adataset.sscfirmi_cv_' ctry_grand_list{i}]);
    
    % interpolating the annual into quarterly
    series_date_range_a = dates('1999Y'):dates('2018Y');
    series_date_range_q = dates('1999Q1'):dates('2018Q4');
      
    clear res
    evalc(['res = denton_d_Guillermo(Adataset.sscfirm_cv_' ctry_all_list{i} '(series_date_range_a),[dseries(0,series_date_range_q)],Qdataset.tcomp_cv_' ctry_all_list{i} '(series_date_range_q),1,2,4,2,2,0,1);']);
    res = res.set_names(series_name_target);
    Qdataset = [Qdataset res];
end

% social security contributions (gfs.q.y.AT.W0.S13.S1.N.C.D61._Z._Z._Z.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list{i},'.W0.S13.S1.N.C.D61._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['sct_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% social security contributions (gfs.q.y.SK.W0.S13.S1.N.C.D611._Z._Z._Z.XDC._Z.S.V.N._T) paid by firms
ctry_all_list_temp = ctry_all_list(1:end ~=find(contains(ctry_all_list,'IT'))); 
for i=1:length(ctry_all_list_temp)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list_temp{i},'.W0.S13.S1.N.C.D611._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['scf_cv_' ctry_all_list_temp{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end
  
% social security contributions
% (gfs.q.y.SK.W0.S13.S1.N.C.D613._Z._Z._Z.XDC._Z.S.V.N._T) paid by HH
for i=1:length(ctry_all_list_temp)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list_temp{i},'.W0.S13.S1.N.C.D613._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['sch_cv_' ctry_all_list_temp{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% for DE, IE and AT the series have to be backcasted with Kalman filter
% for IT the series has to be entirely interpolated given no data
% availability
ctry_list_temp = {'DE','IE','IT','AT'};
for i=1:length(ctry_list_temp)
    % defining country names and series names
    series_name_source_scf = ['GFS.A.N.',ctry_list_temp{i},'.W0.S13.S1.N.C.D611._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target_scf = ['scf_cv_' ctry_list_temp{i}];
    series_name_source_sch = ['GFS.A.N.',ctry_list_temp{i},'.W0.S13.S1.N.C.D613._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target_sch = ['sch_cv_' ctry_list_temp{i}];    
    % retrieve the series
    ts_scf = db2ts(db_GFS,'TS',series_name_source_scf,server);
    ts_sch = db2ts(db_GFS,'TS',series_name_source_sch,server);
    % cumulating single elements
    Adataset = [Adataset ts_scf.A ts_sch.A];
    Adataset = Adataset.rename(series_name_source_scf,series_name_target_scf);
    Adataset = Adataset.rename(series_name_source_sch,series_name_target_sch);
    % interpolating the annual into quarterly
    if ctry_list_temp{i} == 'IT'
        series_date_range_scf = firstobservedperiod(Adataset.scf_cv_IT):lastobservedperiod(Adataset.scf_cv_IT);
        series_date_range_sch = firstobservedperiod(Adataset.sch_cv_IT):lastobservedperiod(Adataset.sch_cv_IT);
%         res_scf = bfl(Adataset.scf_cv_IT(series_date_range_scf).data,1,2,4);
%         res_sch = bfl(Adataset.sch_cv_IT(series_date_range_sch).data,1,2,4);
        res_scf = denton_d_Guillermo(Adataset.scf_cv_IT(dates('1999Y'):dates('2018Y')),[dseries(0,dates('1999q1'):dates('2018q4'))],Qdataset.sct_cv_IT(dates('1999q1'):dates('2018q4')),1,2,4,1,2,0,1);
        res_sch = denton_d_Guillermo(Adataset.sch_cv_IT(dates('1999Y'):dates('2018Y')),[dseries(0,dates('1999q1'):dates('2018q4'))],Qdataset.sct_cv_IT(dates('1999q1'):dates('2018q4')),1,2,4,1,2,0,1);
        intSer_scf = dseries(res_scf.Variable_1.data, '1999q1' , series_name_target_scf);
        intSer_sch = dseries(res_sch.Variable_1.data, '1999q1', series_name_target_sch);
        % updating the Qdataset
        Qdataset = [Qdataset intSer_scf intSer_sch];
    elseif ctry_list_temp{i} == 'DE'
        Qdataset.scf_cv_DE_Kalman = LLT(Adataset.scf_cv_DE(dates('1995Y'):dates('2018Y')),...
                     Qdataset.scf_cv_DE(dates('1995q1'):dates('2018q4')),...
                     dseries(0,dates('1995q1'):dates('2018q4')));
        Qdataset.scf_cv_DE(dates('1999q1'):dates('2001Q4')) = Qdataset.scf_cv_DE_Kalman(dates('1999q1'):dates('2001Q4')).data;
        Qdataset.sch_cv_DE_Kalman = LLT(Adataset.sch_cv_DE(dates('1995Y'):dates('2018Y')),...
                     Qdataset.sch_cv_DE(dates('1995q1'):dates('2018q4')),...
                     dseries(0,dates('1995q1'):dates('2018q4')));
        Qdataset.sch_cv_DE(dates('1999q1'):dates('2001Q4')) = Qdataset.sch_cv_DE_Kalman(dates('1999q1'):dates('2001Q4')).data;
    elseif ctry_list_temp{i} == 'IE'
        Qdataset.scf_cv_IE_Kalman = LLT(Adataset.scf_cv_IE(dates('1995Y'):dates('2018Y')),...
                     Qdataset.scf_cv_IE(dates('1995q1'):dates('2018q4')),...
                     dseries(0,dates('1995q1'):dates('2018q4')));
        Qdataset.scf_cv_IE(dates('1999q1'):dates('2001Q4')) = Qdataset.scf_cv_IE_Kalman(dates('1999q1'):dates('2001Q4')).data;
        Qdataset.sch_cv_IE_Kalman = LLT(Adataset.sch_cv_IE(dates('1995Y'):dates('2018Y')),...
                     Qdataset.sch_cv_IE(dates('1995q1'):dates('2018q4')),...
                     dseries(0,dates('1995q1'):dates('2018q4')));
        Qdataset.sch_cv_IE(dates('1999q1'):dates('2001Q4')) = Qdataset.sch_cv_IE_Kalman(dates('1999q1'):dates('2001Q4')).data;
    elseif ctry_list_temp{i} == 'AT'
        Qdataset.scf_cv_AT_Kalman = LLT(Adataset.scf_cv_AT(dates('1995Y'):dates('2018Y')),...
                     Qdataset.scf_cv_AT(dates('1995q1'):dates('2018q4')),...
                     dseries(0,dates('1995q1'):dates('2018q4')));
        Qdataset.scf_cv_AT(dates('1999q1'):dates('2000Q4')) = Qdataset.scf_cv_AT_Kalman(dates('1999q1'):dates('2000Q4')).data;
        Qdataset.sch_cv_AT_Kalman = LLT(Adataset.sch_cv_AT(dates('1995Y'):dates('2018Y')),...
                     Qdataset.sch_cv_AT(dates('1995q1'):dates('2018q4')),...
                     dseries(0,dates('1995q1'):dates('2018q4')));
        Qdataset.sch_cv_AT(dates('1999q1'):dates('2000Q4')) = Qdataset.sch_cv_AT_Kalman(dates('1999q1'):dates('2000Q4')).data;
    end
end
       
% government compensation of employees (GFS.Q.Y.FI.W0.S13.S1.N.D.D1._Z._Z._T.XDC._Z.S.V.N._T)
% Unit multiplier: Millions
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list{i},'.W0.S13.S1.N.D.D1._Z._Z._T.XDC._Z.S.V.N._T'];
    series_name_target = ['gcomp_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% imputed social security contributions
% Unit multiplier: EUR millions
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.A.N.',ctry_all_list{i},'.W0.S13.S1.N.C.D612._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['impssc_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_GFS,'TS',series_name_source,server);

    % cumulating single elements
    Adataset = [Adataset ts.A];
    Adataset = Adataset.rename(series_name_source,series_name_target);

    % interpolating the annual into quarterly
    series_date_range_a = dates('1999Y'):dates('2018Y');
    series_date_range_q = dates('1999Q1'):dates('2018Q4');
      
    clear res
    evalc(['res = denton_d_Guillermo(Adataset.impssc_cv_' ctry_all_list{i} '(series_date_range_a),[dseries(0,series_date_range_q)],Qdataset.gcomp_cv_' ctry_all_list{i} '(series_date_range_q),1,2,4,2,2,0,1);']);
    res = res.set_names(series_name_target);
    Qdataset = [Qdataset res];
end

% government budget balance (GFS.Q.Y.FI.W0.S13.S1._Z.B.B9._Z._Z._Z.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list{i},'.W0.S13.S1._Z.B.B9._Z._Z._Z.XDC._Z.S.V.N._T'];
    series_name_target = ['gbbraw_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% Net acquisition of non-financial, non-produced assets (NP) (QSA.Q.N.ES.W0.S13.S1.N.D.NP._Z._Z._T.XDC._T.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['QSA.Q.N.',ctry_all_list{i},'.W0.S13.S1.N.D.NP._Z._Z._T.XDC._T.S.V.N._T'];
    series_name_target = ['nafa_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_ecb,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% Other capital transfers (D.92), receivable by financial corporations (S12) (QSA.Q.N.ES.W0.S12.S1.N.C.D9N._Z._Z._Z.XDC._T.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['QSA.Q.N.',ctry_all_list{i},'.W0.S12.S1.N.C.D9N._Z._Z._Z.XDC._T.S.V.N._T'];
    series_name_target = ['ctrnsf_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_ecb,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% interest payments (GFS.Q.Y.FI.W0.S13.S1.C.D.D41._Z._Z._T.XDC._Z.S.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list{i},'.W0.S13.S1.C.D.D41._Z._Z._T.XDC._Z.S.V.N._T'];
    series_name_target = ['inp_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% unemployment number (LFSI.Q.FR.S.UNEMPL.TOTAL0.15_74.T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['LFSI.Q.',ctry_all_list{i},'.S.UNEMPL.TOTAL0.15_74.T'];
    series_name_target = ['unempl_lr_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_lfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% active population (LFSI.Q.FR.S.ACTIVE.TOTAL0.15_74.T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['LFSI.Q.',ctry_all_list{i},'.S.ACTIVE.TOTAL0.15_74.T'];
    series_name_target = ['activepop_lr_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_lfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% gross government debt (GFS.Q.Y.AT.W0.S13.S1.C.L.LE.GD.T._Z.XDC._T.F.V.N._T)
for i=1:length(ctry_all_list)
    % defining country names and series names
    series_name_source = ['GFS.Q.Y.',ctry_all_list{i},'.W0.S13.S1.C.L.LE.GD.T._Z.XDC._T.F.V.N._T'];
    series_name_target = ['gdebt_cv_' ctry_all_list{i}];
    
    % retrieve the series
    ts = db2ts(db_qgfs,'TS',series_name_source,server);

    % cumulating single elements
    Qdataset = [Qdataset ts.Q];
    Qdataset = Qdataset.rename(series_name_source,series_name_target);
end

% backcasting the series in line with the debt accumulation eq.
% gdebt(t)=gdebt(t-1)-gbb(t) -> gdebt(t-1)=gdebt(t)+gbb(t)
% this assumes DDA=0, which should be true only over time
Qdataset.gdebt_cv_FI(dates('1999q4')) = Qdataset.gdebt_cv_FI(dates('2000q1')).data + Qdataset.gbbraw_cv_FI(dates('2000q1')).data;
Qdataset.gdebt_cv_FI(dates('1999q3')) = Qdataset.gdebt_cv_FI(dates('1999q4')).data + Qdataset.gbbraw_cv_FI(dates('1999q4')).data;
Qdataset.gdebt_cv_FI(dates('1999q2')) = Qdataset.gdebt_cv_FI(dates('1999q3')).data + Qdataset.gbbraw_cv_FI(dates('1999q3')).data;
Qdataset.gdebt_cv_FI(dates('1999q1')) = Qdataset.gdebt_cv_FI(dates('1999q2')).data + Qdataset.gbbraw_cv_FI(dates('1999q2')).data;

% downloading the shadow interest rate from Lemke
Qdataset.stn_df_EA = dseries(xlsread('LemkeVladu_SSR_1999-2018_adaptive.xlsx','SSR quarterly','B2:B81'), dates('1999Q1'));

% Exports of Germany to the RoE and the other direction
Qdataset.exp_cv_DE = dseries(xlsread('data_non-fame.xlsx','Data','B7:B86'), dates('1999Q1'));
Qdataset.exp_cv_DR = dseries(xlsread('data_non-fame.xlsx','Data','C7:C86'), dates('1999Q1'));

% RoW variables
Qdataset.GDP_qc_RW = dseries(xlsread('RoW_from_EXT.xlsx','Calculations_GDP','C24:C103'), dates('1999Q1'));
Qdataset.GDP_df_RW = dseries(xlsread('RoW_Haver.xlsx','Data','C6:C85'), dates('1999Q1'));
Qdataset.stn_df_RW = dseries(xlsread('RoW_Haver.xlsx','FFR','B6:B85'), dates('1999Q1'));

save Datasets.mat Qdataset Adataset
fprintf('\nData download complated!\n');
return

%% Section 3: Specifying the time period for the analysis after the download
% ************************************************************************************
% we do not want to download the data each time
clear Qdataset Adataset Qdataset_ref
load Datasets.mat
load Datasets_ref.mat
% in May 2019 this is 1999Q1:2018Q4
Qdataset = Qdataset(firstobservedperiod(Qdataset):lastobservedperiod(Qdataset));
% ************************************************************************************
% Section 4: Aggregating the data for the core, periphery and the euro area
% ************************************************************************************

% define list of series to be aggregated
ser_agg_list = {'POP_lr',...
                'gcons_cv',...
                'ginv_cv',...
                'tinv_cv',...
                'tinv_lr',...
                'hhcons_cv',...
                'gtrnsf_cv',...
                'GDP_cv',...
                'GDP_lr',...
                'potGDP_lr',...
                'taxprod_cv',...
                'taxhhinc_cv',...
                'sschhinc_cv',...
                'sscfirm_cv',...
                'dtx_cv',...
                'itx_cv',...
                'sct_cv',...
                'scf_cv',...
                'sch_cv',...
                'tcomp_cv',...
                'gcomp_cv',...      
                'impssc_cv',...     
                'gbbraw_cv',...
                'nafa_cv',...
                'ctrnsf_cv',...
                'inp_cv',...
                'gdebt_cv',...
                'templ_lr',...
                'gempl_lr',...
                'ginc_cv',...
                'gstm_cv',...              
                'unempl_lr',...       
                'activepop_lr',...       
                };

for i=1:length(ser_agg_list)
	evalc(['Qdataset.' ser_agg_list{i} '_CR=Qdataset.' ser_agg_list{i} '_AT+Qdataset.' ser_agg_list{i} '_BE+Qdataset.' ser_agg_list{i} '_DE+Qdataset.' ser_agg_list{i} '_FI+Qdataset.' ser_agg_list{i} '_FR+Qdataset.' ser_agg_list{i} '_NL']);
	evalc(['Qdataset.' ser_agg_list{i} '_PR=Qdataset.' ser_agg_list{i} '_ES+Qdataset.' ser_agg_list{i} '_GR+Qdataset.' ser_agg_list{i} '_IE+Qdataset.' ser_agg_list{i} '_IT+Qdataset.' ser_agg_list{i} '_PT']);
	evalc(['Qdataset.' ser_agg_list{i} '_EA=Qdataset.' ser_agg_list{i} '_CR+Qdataset.' ser_agg_list{i} '_PR']);
	evalc(['Qdataset.' ser_agg_list{i} '_DR=Qdataset.' ser_agg_list{i} '_AT+Qdataset.' ser_agg_list{i} '_BE+Qdataset.' ser_agg_list{i} '_DE+Qdataset.' ser_agg_list{i} '_FI+Qdataset.' ser_agg_list{i} '_FR+Qdataset.' ser_agg_list{i} '_NL+Qdataset.' ser_agg_list{i} '_ES+Qdataset.' ser_agg_list{i} '_GR+Qdataset.' ser_agg_list{i} '_IE+Qdataset.' ser_agg_list{i} '_IT+Qdataset.' ser_agg_list{i} '_PT']);
end
         
% ************************************************************************************
% Section 5: Calculating necessary series (e.g. ratios to potential GDP, deflators)
% ************************************************************************************
for i=1:length(ctry_grand_list)
    % the graphs with core vs. periphery are based on pr and qq series so
    % pr needs to be calculated even if equivalent to lr
%     evalc(['Qdataset.POP_pr_' ctry_grand_list{i} '=Qdataset.POP_lr_' ctry_grand_list{i} '/1']);
%     evalc(['plot_single_var(Qdataset.POP_pr_' ctry_grand_list{i} ')']);
    evalc(['Qdataset.POP_qq_' ctry_grand_list{i} '=log(Qdataset.POP_lr_' ctry_grand_list{i} '/lag(Qdataset.POP_lr_' ctry_grand_list{i} ',1))']);
%     evalc(['plot_single_var(Qdataset.POP_qq_' ctry_grand_list{i} ',Qdataset.POP_lr_' ctry_grand_list{i} ')']);
    %     evalc(['plot_single_var(Qdataset.POP_qq_' ctry_grand_list{i} ')']);
    evalc(['Qdataset.GDP_lc_' ctry_grand_list{i} '=(Qdataset.GDP_lr_' ctry_grand_list{i} '/Qdataset.POP_lr_' ctry_grand_list{i} ')']);
    evalc(['Qdataset.GDP_qc_' ctry_grand_list{i} '=log(Qdataset.GDP_lc_' ctry_grand_list{i} '/lag(Qdataset.GDP_lc_' ctry_grand_list{i} ',1))']);
%     evalc(['plot_single_var(Qdataset.GDP_qc_' ctry_grand_list{i} ',Qdataset.GDP_lc_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.GDP_cv_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.GDP_lr_' ctry_grand_list{i} ')']);    
%     evalc(['plot_single_var(Qdataset.GDP_qc_' ctry_grand_list{i} ')']);    
    evalc(['Qdataset.GDP_df_' ctry_grand_list{i} '=Qdataset.GDP_cv_' ctry_grand_list{i} '/Qdataset.GDP_lr_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.GDP_dq_' ctry_grand_list{i} '=log(Qdataset.GDP_df_' ctry_grand_list{i} '/lag(Qdataset.GDP_df_' ctry_grand_list{i} ',1))']);
%     evalc(['plot_single_var(Qdataset.GDP_dq_' ctry_grand_list{i} ',Qdataset.GDP_df_' ctry_grand_list{i} ')']);
    evalc(['Qdataset.potGDP_cv_' ctry_grand_list{i} '=Qdataset.potGDP_lr_' ctry_grand_list{i} '*Qdataset.GDP_df_' ctry_grand_list{i} '/100']);
    evalc(['Qdataset.hhcons_lr_' ctry_grand_list{i} '=Qdataset.hhcons_cv_' ctry_grand_list{i} '/Qdataset.GDP_df_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.hhcons_lc_' ctry_grand_list{i} '=(Qdataset.hhcons_lr_' ctry_grand_list{i} '/Qdataset.POP_lr_' ctry_grand_list{i} ')']);
    %     evalc(['Qdataset.hhcons_pr_' ctry_grand_list{i} '=Qdataset.hhcons_lr_' ctry_grand_list{i} '/Qdataset.potGDP_lr_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.hhcons_qc_' ctry_grand_list{i} '=log(Qdataset.hhcons_lc_' ctry_grand_list{i} '/lag(Qdataset.hhcons_lc_' ctry_grand_list{i} ',1))']);
    
    evalc(['Qdataset.tinv_df_' ctry_grand_list{i} '=Qdataset.tinv_cv_' ctry_grand_list{i} '/Qdataset.tinv_lr_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.pinv_cv_' ctry_grand_list{i} '=Qdataset.tinv_cv_' ctry_grand_list{i} '-Qdataset.ginv_cv_' ctry_grand_list{i}]);
    evalc(['Qdataset.pinv_lr_' ctry_grand_list{i} '=Qdataset.pinv_cv_' ctry_grand_list{i} '/Qdataset.GDP_df_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.pinv_lc_' ctry_grand_list{i} '=(Qdataset.pinv_lr_' ctry_grand_list{i} '/Qdataset.POP_lr_' ctry_grand_list{i} ')']);
    evalc(['Qdataset.pinv_qc_' ctry_grand_list{i} '=log(Qdataset.pinv_lc_' ctry_grand_list{i} '/lag(Qdataset.pinv_lc_' ctry_grand_list{i} ',1))']);
    evalc(['Qdataset.pinv_pr_' ctry_grand_list{i} '=Qdataset.pinv_lr_' ctry_grand_list{i} '/Qdataset.potGDP_lr_' ctry_grand_list{i} '*100']);
%     evalc(['plot_single_var(Qdataset.pinv_qc_' ctry_grand_list{i} ',Qdataset.pinv_lc_' ctry_grand_list{i} ')']);

    %     evalc(['plot_single_var(Qdataset.templ_lr_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.gempl_lr_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.tinv_cv_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.pinv_cv_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.tinv_cv_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.pinv_cv_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.ginv_cv_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.pinv_lr_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.pinv_qc_' ctry_grand_list{i} ')']);
    %     evalc(['plot_single_var(Qdataset.ginv_cv_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.pinv_qc_' ctry_grand_list{i} ')']);
    
    evalc(['Qdataset.pempl_lr_' ctry_grand_list{i} '=Qdataset.templ_lr_' ctry_grand_list{i} '-Qdataset.gempl_lr_' ctry_grand_list{i}]);
    evalc(['Qdataset.pempl_lc_' ctry_grand_list{i} '=(Qdataset.pempl_lr_' ctry_grand_list{i} '/Qdataset.POP_lr_' ctry_grand_list{i} ')']);

    %     evalc(['Qdataset.pempl_qc_' ctry_grand_list{i} '=log(Qdataset.pempl_lr_' ctry_grand_list{i} '/lag(Qdataset.pempl_lr_' ctry_grand_list{i} ',1))-Qdataset.POP_qq_' ctry_grand_list{i}]);
    evalc(['Qdataset.wgs_cv_' ctry_grand_list{i} '=Qdataset.gcomp_cv_' ctry_grand_list{i} '-Qdataset.impssc_cv_' ctry_grand_list{i}]);
    evalc(['Qdataset.wgs_pe_' ctry_grand_list{i} '=Qdataset.wgs_cv_' ctry_grand_list{i} '/Qdataset.gempl_lr_' ctry_grand_list{i}]);
    evalc(['Qdataset.wgs_qe_' ctry_grand_list{i} '=log(Qdataset.wgs_pe_' ctry_grand_list{i} '/lag(Qdataset.wgs_pe_' ctry_grand_list{i} ',1))']);
%     evalc(['plot_single_var(Qdataset.wgs_cv_' ctry_grand_list{i} ',Qdataset.gempl_lr_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.wgs_qe_' ctry_grand_list{i} ',Qdataset.wgs_pe_' ctry_grand_list{i} ')']);
    evalc(['Qdataset.pcomp_cv_' ctry_grand_list{i} '=Qdataset.tcomp_cv_' ctry_grand_list{i} '-Qdataset.wgs_cv_' ctry_grand_list{i}]);
%     evalc(['Qdataset.pcomp_qc_' ctry_grand_list{i} '=log(Qdataset.pcomp_cv_' ctry_grand_list{i} '/lag(Qdataset.pcomp_cv_' ctry_grand_list{i} ',1))*100-Qdataset.POP_qq_' ctry_grand_list{i}]);
%     evalc(['Qdataset.pcomp_pr_' ctry_grand_list{i} '=Qdataset.pcomp_cv_' ctry_grand_list{i} '/Qdataset.potGDP_cv_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.pcomp_pe_' ctry_grand_list{i} '=Qdataset.pcomp_cv_' ctry_grand_list{i} '/Qdataset.pempl_lr_' ctry_grand_list{i}]);
    evalc(['Qdataset.pcomp_qe_' ctry_grand_list{i} '=log(Qdataset.pcomp_pe_' ctry_grand_list{i} '/lag(Qdataset.pcomp_pe_' ctry_grand_list{i} ',1))']);
%     evalc(['plot_single_var(Qdataset.pcomp_qe_' ctry_grand_list{i} ',Qdataset.pcomp_pe_' ctry_grand_list{i} ')']);

%     evalc(['plot_single_var(Qdataset.pcomp_cv_' ctry_grand_list{i} ',Qdataset.pempl_lr_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.pcomp_qe_' ctry_grand_list{i} ',Qdataset.pcomp_pe_' ctry_grand_list{i} ')']);

    evalc(['Qdataset.gpur_cv_' ctry_grand_list{i} '=Qdataset.ginc_cv_' ctry_grand_list{i} '+Qdataset.gstm_cv_' ctry_grand_list{i}]);
    evalc(['Qdataset.hhcons_df_' ctry_grand_list{i} '=Qdataset.hhcons_cv_' ctry_grand_list{i} '/Qdataset.hhcons_lr_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gpur_lr_' ctry_grand_list{i} '=Qdataset.gpur_cv_' ctry_grand_list{i} '/Qdataset.hhcons_df_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gstm_lr_' ctry_grand_list{i} '=Qdataset.gstm_cv_' ctry_grand_list{i} '/Qdataset.hhcons_df_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.ginc_lr_' ctry_grand_list{i} '=Qdataset.ginc_cv_' ctry_grand_list{i} '/Qdataset.hhcons_df_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gpur_pr_' ctry_grand_list{i} '=Qdataset.gpur_lr_' ctry_grand_list{i} '/Qdataset.potGDP_lr_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gstm_pr_' ctry_grand_list{i} '=Qdataset.gstm_lr_' ctry_grand_list{i} '/Qdataset.potGDP_lr_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.ginc_pr_' ctry_grand_list{i} '=Qdataset.ginc_lr_' ctry_grand_list{i} '/Qdataset.potGDP_lr_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gpur_qc_' ctry_grand_list{i} '=log(Qdataset.gpur_lr_' ctry_grand_list{i} '/lag(Qdataset.gpur_lr_' ctry_grand_list{i} ',1))*100-Qdataset.POP_qq_' ctry_grand_list{i}]);
    evalc(['Qdataset.gstm_qc_' ctry_grand_list{i} '=log(Qdataset.gstm_lr_' ctry_grand_list{i} '/lag(Qdataset.gstm_lr_' ctry_grand_list{i} ',1))*100-Qdataset.POP_qq_' ctry_grand_list{i}]);
    evalc(['Qdataset.ginc_qc_' ctry_grand_list{i} '=log(Qdataset.ginc_lr_' ctry_grand_list{i} '/lag(Qdataset.ginc_lr_' ctry_grand_list{i} ',1))*100-Qdataset.POP_qq_' ctry_grand_list{i}]);

    evalc(['Qdataset.gcons_lr_' ctry_grand_list{i} '=Qdataset.gcons_cv_' ctry_grand_list{i} '/Qdataset.GDP_df_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gcons_df_' ctry_grand_list{i} '=Qdataset.gcons_cv_' ctry_grand_list{i} '/Qdataset.gcons_lr_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gcons_pr_' ctry_grand_list{i} '=Qdataset.gcons_lr_' ctry_grand_list{i} '/Qdataset.potGDP_lr_' ctry_grand_list{i} '*100']);
    
    evalc(['Qdataset.gcons_lc_' ctry_grand_list{i} '=(Qdataset.gcons_lr_' ctry_grand_list{i} '/Qdataset.POP_lr_' ctry_grand_list{i} ')']);
    evalc(['Qdataset.gcons_qc_' ctry_grand_list{i} '=log(Qdataset.gcons_lc_' ctry_grand_list{i} '/lag(Qdataset.gcons_lc_' ctry_grand_list{i} ',1))']);
     evalc(['plot_single_var(Qdataset.gcons_qc_' ctry_grand_list{i} ',Qdataset.gcons_lc_' ctry_grand_list{i} ')']);

    
    evalc(['Qdataset.ginv_lr_' ctry_grand_list{i} '=Qdataset.ginv_cv_' ctry_grand_list{i} '/Qdataset.GDP_df_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.ginv_pr_' ctry_grand_list{i} '=Qdataset.ginv_lr_' ctry_grand_list{i} '/Qdataset.potGDP_lr_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.ginv_lc_' ctry_grand_list{i} '=(Qdataset.ginv_lr_' ctry_grand_list{i} '/Qdataset.POP_lr_' ctry_grand_list{i} ')']);
    evalc(['Qdataset.ginv_qc_' ctry_grand_list{i} '=log(Qdataset.ginv_lc_' ctry_grand_list{i} '/lag(Qdataset.ginv_lc_' ctry_grand_list{i} ',1))']);
     evalc(['plot_single_var(Qdataset.ginv_qc_' ctry_grand_list{i} ',Qdataset.ginv_lc_' ctry_grand_list{i} ')']);

%     evalc(['Qdataset.gcomp_qc_' ctry_grand_list{i} '=log(Qdataset.gcomp_cv_' ctry_grand_list{i} '/lag(Qdataset.gcomp_cv_' ctry_grand_list{i} ',1))*100-Qdataset.POP_qq_' ctry_grand_list{i}]);
%     evalc(['Qdataset.gcomp_pr_' ctry_grand_list{i} '=Qdataset.gcomp_cv_' ctry_grand_list{i} '/Qdataset.potGDP_cv_' ctry_grand_list{i} '*100']);
%     evalc(['Qdataset.gempl_qc_' ctry_grand_list{i} '=log(Qdataset.gempl_lr_' ctry_grand_list{i} '/lag(Qdataset.gempl_lr_' ctry_grand_list{i} ',1))-Qdataset.POP_qq_' ctry_grand_list{i}]);
%     evalc(['Qdataset.gempl_pr_' ctry_grand_list{i} '=Qdataset.gempl_lr_' ctry_grand_list{i} '/Qdataset.templ_lr_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gempl_lc_' ctry_grand_list{i} '=(Qdataset.gempl_lr_' ctry_grand_list{i} '/Qdataset.POP_lr_' ctry_grand_list{i} ')']);
%     evalc(['Qdataset.gcomp_pe_' ctry_grand_list{i} '=Qdataset.gcomp_cv_' ctry_grand_list{i} '/Qdataset.gempl_lr_' ctry_grand_list{i}]);
%     evalc(['Qdataset.gcomp_qe_' ctry_grand_list{i} '=log(Qdataset.gcomp_pe_' ctry_grand_list{i} '/lag(Qdataset.gcomp_pe_' ctry_grand_list{i} ',1))']);
    evalc(['Qdataset.gtrnsf_lr_' ctry_grand_list{i} '=Qdataset.gtrnsf_cv_' ctry_grand_list{i} '/Qdataset.GDP_df_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gtrnsf_pr_' ctry_grand_list{i} '=Qdataset.gtrnsf_lr_' ctry_grand_list{i} '/Qdataset.potGDP_lr_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gtrnsf_lc_' ctry_grand_list{i} '=(Qdataset.gtrnsf_lr_' ctry_grand_list{i} '/Qdataset.POP_lr_' ctry_grand_list{i} ')']);
    evalc(['Qdataset.gtrnsf_qc_' ctry_grand_list{i} '=log(Qdataset.gtrnsf_lc_' ctry_grand_list{i} '/lag(Qdataset.gtrnsf_lc_' ctry_grand_list{i} ',1))']);
    evalc(['Qdataset.GDP_pr_' ctry_grand_list{i} '=(Qdataset.GDP_lr_' ctry_grand_list{i} '/Qdataset.potGDP_lr_' ctry_grand_list{i} '-1)*100']);
    evalc(['Qdataset.dtx_pr_' ctry_grand_list{i} '=Qdataset.dtx_cv_' ctry_grand_list{i} '/Qdataset.tcomp_cv_' ctry_grand_list{i} '*100']);

    evalc(['Qdataset.taxprod_pr_' ctry_grand_list{i} '=Qdataset.taxprod_cv_' ctry_grand_list{i} '/Qdataset.hhcons_cv_' ctry_grand_list{i} '*100']);
%     evalc(['plot_single_var(Qdataset.taxprod_cv_' ctry_grand_list{i} ',Qdataset.hhcons_cv_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.taxprod_pr_' ctry_grand_list{i} ',Qdataset_ref.taxprod_pr_' ctry_grand_list{i} ')']);

    evalc(['Qdataset.tschhinc_cv_' ctry_grand_list{i} '=Qdataset.taxhhinc_cv_' ctry_grand_list{i} '+Qdataset.sschhinc_cv_' ctry_grand_list{i}]);
    evalc(['Qdataset.tschhinc_pr_' ctry_grand_list{i} '=Qdataset.tschhinc_cv_' ctry_grand_list{i} '/Qdataset.tcomp_cv_' ctry_grand_list{i} '*1']);
    evalc(['Qdataset.sscfirm_pr_' ctry_grand_list{i} '=Qdataset.sscfirm_cv_' ctry_grand_list{i} '/Qdataset.tcomp_cv_' ctry_grand_list{i} '*1']);
%     evalc(['plot_single_var(Qdataset.sscfirm_cv_' ctry_grand_list{i} ',Qdataset.tcomp_cv_' ctry_grand_list{i} ')']);
%     evalc(['plot_single_var(Qdataset.sscfirm_pr_' ctry_grand_list{i} ',Qdataset_ref.sscfirm_pr_' ctry_grand_list{i} ')']);
    evalc(['Qdataset.tschhinc_qc_' ctry_grand_list{i} '=log(Qdataset.tschhinc_cv_' ctry_grand_list{i} '/lag(Qdataset.tschhinc_cv_' ctry_grand_list{i} ',1))*100-Qdataset.POP_qq_' ctry_grand_list{i}]);
    evalc(['Qdataset.sscfirm_qc_' ctry_grand_list{i} '=log(Qdataset.sscfirm_cv_' ctry_grand_list{i} '/lag(Qdataset.sscfirm_cv_' ctry_grand_list{i} ',1))*100-Qdataset.POP_qq_' ctry_grand_list{i}]);

    evalc(['Qdataset.itx_pr_' ctry_grand_list{i} '=Qdataset.taxprod_cv_' ctry_grand_list{i} '/Qdataset.hhcons_cv_' ctry_grand_list{i} '']);
    evalc(['Qdataset.unempl_pr_' ctry_grand_list{i} '=Qdataset.unempl_lr_' ctry_grand_list{i} '/Qdataset.activepop_lr_' ctry_grand_list{i} '']);
    evalc(['plot_single_var(Qdataset.unempl_pr_' ctry_grand_list{i} ',Qdataset.unempl_lr_' ctry_grand_list{i} ')']);
    %     evalc(['plot_single_var(Qdataset.unempl_pr_' ctry_grand_list{i} ')']);
    % temporary adjustment for labour taxes
    evalc(['Qdataset.dtl_cv_' ctry_grand_list{i} '=0.7*Qdataset.dtx_cv_' ctry_grand_list{i}]);
    % tmeporary fix to split SSC before Linda comes up with the seasonal
    % adjustment
    evalc(['Qdataset.sct_pr_' ctry_grand_list{i} '=Qdataset.sct_cv_' ctry_grand_list{i} '/Qdataset.tcomp_cv_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.sch_pr_' ctry_grand_list{i} '=Qdataset.sch_cv_' ctry_grand_list{i} '/Qdataset.tcomp_cv_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.scf_pr_' ctry_grand_list{i} '=Qdataset.scf_cv_' ctry_grand_list{i} '/Qdataset.tcomp_cv_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.dtl_pr_' ctry_grand_list{i} '=Qdataset.dtl_cv_' ctry_grand_list{i} '/Qdataset.tcomp_cv_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.pdf_cv_' ctry_grand_list{i} '=Qdataset.gbbraw_cv_' ctry_grand_list{i} '+Qdataset.inp_cv_' ctry_grand_list{i}]);
    evalc(['Qdataset.inp_pr_' ctry_grand_list{i} '=Qdataset.inp_cv_' ctry_grand_list{i} '/Qdataset.potGDP_cv_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gbb_cv_' ctry_grand_list{i} '=Qdataset.gbbraw_cv_' ctry_grand_list{i} '+0*Qdataset.ctrnsf_cv_' ctry_grand_list{i} '+Qdataset.nafa_cv_' ctry_grand_list{i}]);
    evalc(['Qdataset.gbb_pr_' ctry_grand_list{i} '=Qdataset.gbb_cv_' ctry_grand_list{i} '/Qdataset.GDP_cv_' ctry_grand_list{i} '']);
    evalc(['Qdataset.nafa_pr_' ctry_grand_list{i} '=Qdataset.nafa_cv_' ctry_grand_list{i} '/Qdataset.GDP_cv_' ctry_grand_list{i} '']);
%       evalc(['plot_single_var(Qdataset.gbb_pr_' ctry_grand_list{i} ',Qdataset_ref.gbb_pr_' ctry_grand_list{i} ')']);
    evalc(['Qdataset.ctrnsf_pr_' ctry_grand_list{i} '=Qdataset.ctrnsf_cv_' ctry_grand_list{i} '/Qdataset.GDP_cv_' ctry_grand_list{i} '']);
%      evalc(['plot_single_var(Qdataset.ctrnsf_pr_' ctry_grand_list{i} ',Qdataset_ref.ctrnsf_pr_' ctry_grand_list{i} ')']);

    evalc(['Qdataset.pdf_pr_' ctry_grand_list{i} '=Qdataset.pdf_cv_' ctry_grand_list{i} '/Qdataset.potGDP_cv_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gdebt_pr_' ctry_grand_list{i} '=Qdataset.gdebt_cv_' ctry_grand_list{i} '/Qdataset.GDP_cv_' ctry_grand_list{i} '*100']);
    evalc(['Qdataset.gdebt_qc_' ctry_grand_list{i} '=log(Qdataset.gdebt_cv_' ctry_grand_list{i} '/lag(Qdataset.gdebt_cv_' ctry_grand_list{i} ',1))*100-Qdataset.POP_qq_' ctry_grand_list{i}]);
end

Qdataset.stn_qd_EA=(Qdataset.stn_df_EA-lag(Qdataset.stn_df_EA,1));

% calculation of exports
Qdataset.exp_lr_DE=Qdataset.exp_cv_DE/Qdataset.GDP_df_DE*100;
Qdataset.exp_lr_DR=Qdataset.exp_cv_DR/Qdataset.GDP_df_DR*100;
Qdataset.exp_lc_DE=Qdataset.exp_lr_DE/Qdataset.POP_lr_DE;
Qdataset.exp_lc_DR=Qdataset.exp_lr_DR/Qdataset.POP_lr_DR;
Qdataset.exp_qc_DE=log(Qdataset.exp_lc_DE/lag(Qdataset.exp_lc_DE,1));
Qdataset.exp_qc_DR=log(Qdataset.exp_lc_DR/lag(Qdataset.exp_lc_DR,1));
% plot_single_var(Qdataset.exp_qc_DE,Qdataset.exp_lc_DE);
% plot_single_var(Qdataset.exp_qc_DR,Qdataset.exp_lc_DR);

% evalc(['Qdataset.hhcons_lr_' ctry_grand_list{i} '=Qdataset.hhcons_cv_' ctry_grand_list{i} '/Qdataset.GDP_df_' ctry_grand_list{i} '*100']);
% evalc(['Qdataset.hhcons_qc_' ctry_grand_list{i} '=log(Qdataset.hhcons_lr_' ctry_grand_list{i} '/lag(Qdataset.hhcons_lr_' ctry_grand_list{i} ',1))-Qdataset.POP_qq_' ctry_grand_list{i}]);

% Saving quarterly dataset for a reference, which must take place before
% demeaning, which cuts 1999q1
% Qdataset_ref = Qdataset;
% save Datasets_ref.mat Qdataset_ref

fprintf('\nData calculations (stage 1/2) performed!\n');

%% 
Qdataset = Qdataset(firstobservedperiod(Qdataset):lastobservedperiod(Qdataset));

Qdataset.GDP_d__DE=Qdataset.GDP_dq_DE-mean(Qdataset.GDP_dq_DE);
Qdataset.GDP_d__DR=Qdataset.GDP_dq_DR-mean(Qdataset.GDP_dq_DR);
Qdataset.GDP_d__CR=Qdataset.GDP_dq_CR-mean(Qdataset.GDP_dq_CR);
Qdataset.GDP_d__PR=Qdataset.GDP_dq_PR-mean(Qdataset.GDP_dq_PR);

Qdataset.pempl_l__DE=Qdataset.pempl_lc_DE-mean(Qdataset.pempl_lc_DE);
Qdataset.pempl_l__DR=Qdataset.pempl_lc_DR-mean(Qdataset.pempl_lc_DR);
Qdataset.pempl_l__CR=Qdataset.pempl_lc_CR-mean(Qdataset.pempl_lc_CR);
Qdataset.pempl_l__PR=Qdataset.pempl_lc_PR-mean(Qdataset.pempl_lc_PR);

Qdataset.unempl_p__DE=Qdataset.unempl_pr_DE-mean(Qdataset.unempl_pr_DE);
Qdataset.unempl_p__DR=Qdataset.unempl_pr_DR-mean(Qdataset.unempl_pr_DR);
Qdataset.unempl_p__CR=Qdataset.unempl_pr_CR-mean(Qdataset.unempl_pr_CR);
Qdataset.unempl_p__PR=Qdataset.unempl_pr_PR-mean(Qdataset.unempl_pr_PR);

Qdataset.gempl_l__DE=Qdataset.gempl_lc_DE-mean(Qdataset.gempl_lc_DE);
Qdataset.gempl_l__DR=Qdataset.gempl_lc_DR-mean(Qdataset.gempl_lc_DR);
Qdataset.gempl_l__CR=Qdataset.gempl_lc_CR-mean(Qdataset.gempl_lc_CR);
Qdataset.gempl_l__PR=Qdataset.gempl_lc_PR-mean(Qdataset.gempl_lc_PR);

Qdataset.gbb_p__DE=Qdataset.gbb_pr_DE-mean(Qdataset.gbb_pr_DE);
Qdataset.gbb_p__DR=Qdataset.gbb_pr_DR-mean(Qdataset.gbb_pr_DR);
Qdataset.gbb_p__CR=Qdataset.gbb_pr_CR-mean(Qdataset.gbb_pr_CR);
Qdataset.gbb_p__PR=Qdataset.gbb_pr_PR-mean(Qdataset.gbb_pr_PR);

Qdataset.itx_p__DE=Qdataset.itx_pr_DE-mean(Qdataset.itx_pr_DE);
Qdataset.itx_p__DR=Qdataset.itx_pr_DR-mean(Qdataset.itx_pr_DR);
Qdataset.itx_p__CR=Qdataset.itx_pr_CR-mean(Qdataset.itx_pr_CR);
Qdataset.itx_p__PR=Qdataset.itx_pr_PR-mean(Qdataset.itx_pr_PR);

Qdataset.ZeroSeries = dseries(0,firstdate(Qdataset):lastdate(Qdataset));

Qdataset.stn_d__EA=(Qdataset.stn_df_EA-mean(Qdataset.stn_df_EA))/100;

Qdataset.tschhinc_p__DE=Qdataset.tschhinc_pr_DE-mean(Qdataset.tschhinc_pr_DE);
Qdataset.tschhinc_p__DR=Qdataset.tschhinc_pr_DR-mean(Qdataset.tschhinc_pr_DR);
Qdataset.tschhinc_p__CR=Qdataset.tschhinc_pr_CR-mean(Qdataset.tschhinc_pr_CR);
Qdataset.tschhinc_p__PR=Qdataset.tschhinc_pr_PR-mean(Qdataset.tschhinc_pr_PR);

Qdataset.sscfirm_p__DE=Qdataset.sscfirm_pr_DE-mean(Qdataset.sscfirm_pr_DE);
Qdataset.sscfirm_p__DR=Qdataset.sscfirm_pr_DR-mean(Qdataset.sscfirm_pr_DR);
Qdataset.sscfirm_p__CR=Qdataset.sscfirm_pr_CR-mean(Qdataset.sscfirm_pr_CR);
Qdataset.sscfirm_p__PR=Qdataset.sscfirm_pr_PR-mean(Qdataset.sscfirm_pr_PR);

Qdataset.GDP_qc_RW = (Qdataset.GDP_qc_RW-mean(Qdataset.GDP_qc_RW))/100;
Qdataset.GDP_df_RW = (Qdataset.GDP_df_RW-mean(Qdataset.GDP_df_RW));
Qdataset.stn_d__RW=(Qdataset.stn_df_RW-mean(Qdataset.stn_df_RW));

% for i=1:length(ctry_agg_list)
%     evalc(['Qdataset.gcons_prln_' ctry_agg_list{i} ' =detrend(Qdataset.gcons_pr_' ctry_agg_list{i} ',1)']);
%     evalc(['Qdataset.ginv_prln_' ctry_agg_list{i} '  =detrend(Qdataset.ginv_pr_' ctry_agg_list{i} ',1)']);
%     evalc(['Qdataset.gtrnsf_prln_' ctry_agg_list{i} '=detrend(Qdataset.gtrnsf_pr_' ctry_agg_list{i} ',1)']);
%     evalc(['Qdataset.GDP_prln_' ctry_agg_list{i} '=detrend(Qdataset.GDP_lr_' ctry_agg_list{i} ',1)/(Qdataset.GDP_lr_' ctry_agg_list{i} '-detrend(Qdataset.GDP_lr_' ctry_agg_list{i} ',1))*100']);
%     evalc(['Qdataset.gcons_prhp_' ctry_agg_list{i} ' =Qdataset.gcons_pr_'  ctry_agg_list{i} '- hptrend(Qdataset.gcons_pr_'  ctry_agg_list{i} ')' ]);
%     evalc(['Qdataset.ginv_prhp_' ctry_agg_list{i} '  =Qdataset.ginv_pr_'   ctry_agg_list{i} '- hptrend(Qdataset.ginv_pr_'   ctry_agg_list{i} ')' ]);
%     evalc(['Qdataset.gtrnsf_prhp_' ctry_agg_list{i} '=Qdataset.gtrnsf_pr_' ctry_agg_list{i} '- hptrend(Qdataset.gtrnsf_pr_' ctry_agg_list{i} ')' ]);
%     evalc(['Qdataset.GDP_prhp_' ctry_agg_list{i} '=(Qdataset.GDP_lr_' ctry_agg_list{i} '/hptrend(Qdataset.GDP_lr_' ctry_agg_list{i} ')-1)*100']);
% end                

fprintf('\nData calculations (stage 2/2) performed!\n');

%% ************************************************************************************
% Section 6: Creating plots of data series for the appendix
% ************************************************************************************
if false
data_transformation_plot_annex_charts(Qdataset,ctry_grand_list,'xxx');
data_transformation_plot_annex_charts1(Qdataset,ctry_grand_list,'GDP');
data_transformation_plot_annex_charts1(Qdataset,ctry_grand_list,'gcons');
data_transformation_plot_annex_charts1(Qdataset,ctry_grand_list,'ginv');
data_transformation_plot_annex_charts1(Qdataset,ctry_grand_list,'gtrnsf');
data_transformation_plot_annex_charts1(Qdataset,ctry_grand_list,'dtl');
data_transformation_plot_annex_charts1(Qdataset,ctry_grand_list,'itx');
data_transformation_plot_annex_charts1(Qdataset,ctry_grand_list,'sch');
data_transformation_plot_annex_charts1(Qdataset,ctry_grand_list,'scf');
data_transformation_plot_annex_charts1(Qdataset,ctry_grand_list,'gdebt');

data_transformation_plot_annex_slides(Qdataset,ctry_grand_list,'GDP');
data_transformation_plot_annex_slides(Qdataset,ctry_grand_list,'dtl');
data_transformation_plot_annex_slides(Qdataset,ctry_grand_list,'itx');
data_transformation_plot_annex_slides(Qdataset,ctry_grand_list,'sch');
data_transformation_plot_annex_slides(Qdataset,ctry_grand_list,'scf');
data_transformation_plot_annex_slides(Qdataset,ctry_grand_list,'ginv');
data_transformation_plot_annex_slides(Qdataset,ctry_grand_list,'gcons');
data_transformation_plot_annex_slides(Qdataset,ctry_grand_list,'gtrnsf');
data_transformation_plot_annex_slides(Qdataset,ctry_grand_list,'gdebt');
end

%% ************************************************************************************
% Section 7: Creating plots of data for illustrations in the introduction
% ************************************************************************************
if false
% starting
% clear all; clc; close all;
% addpath ../data/altmany-export_fig-7940863\

% importing xls data and transforming it into dseries object
% [num,txt] = xlsread('data_counterfactual.xlsx','data_q_toMatlab_illustrations','a2:by22');
% data_illust = dseries(num', '1999Q1', txt');

% setting up x axis for figures
T=Qdataset.dates.time(:,1)+Qdataset.dates.time(:,2)./4-0.25*0;
% T_old_to_be_romoved=data_illust.dates.time(:,1)+data_illust.dates.time(:,2)./4-0.25;
x_axis=(T(1)+5-mod(T(1),5):5:T(end)-mod(T(end),5))';
x_labels=num2str(x_axis);

% ************************************************************************************
% ploting the series for the output gap
figure
    plot(T,Qdataset.GDP_pr_EA.data,'Color',rgb('black'),'Linewidth',3,'Linestyle','-');
    hold on;
    plot(T,Qdataset.GDP_pr_CR.data,'Color',rgb('blue'),'Linewidth',3,'Linestyle',':');
    hold on;
    plot(T,Qdataset.GDP_pr_PR.data,'Color',rgb('red'),'Linewidth',3,'Linestyle','-.');
    hold off;
grid on;
axis('tight');
ylabel('% of potential GDP')
legend('euro area', 'core', 'periphery');
export_fig run_data_transformation_chart_output_gap.pdf -transparent
copyfile('run_data_transformation_chart_output_gap.pdf','../latex/XXXX-XX_draft-live/run_data_transformation_chart_output_gap.pdf')
copyfile('run_data_transformation_chart_output_gap.pdf','../latex/XXXX-XX_slides-live/run_data_transformation_chart_output_gap.pdf')
close(1);

% ************************************************************************************
% plotting series for spending
figure
subplot(1,3,1);
plot(T,Qdataset.gcons_pr_EA.data-Qdataset.gcons_pr_EA('1999Q1').data,'Color',rgb('black'),'Linewidth',1,'Linestyle','-');
hold on;
plot(T,Qdataset.gcons_pr_CR.data-Qdataset.gcons_pr_CR('1999Q1').data,'Color',rgb('blue'),'Linewidth',1,'Linestyle',':');
hold on;
plot(T,Qdataset.gcons_pr_PR.data-Qdataset.gcons_pr_PR('1999Q1').data,'Color',rgb('red'),'Linewidth',1,'Linestyle','-.');
hold off;
title({'Government consumption /', 'potential GDP'});
ylabel('change in pp. compared to 1999Q1')
xticks(x_axis)
xticklabels(x_labels)
axis('tight')
grid on
ax = gca;
ax.LineWidth=0.4;
ax.FontSize = 6; 
pbaspect([1 1 1])

subplot(1,3,2);
plot(T,Qdataset.ginv_pr_EA.data-Qdataset.ginv_pr_EA('1999Q1').data,'Color',rgb('black'),'Linewidth',1,'Linestyle','-');
hold on;
plot(T,Qdataset.ginv_pr_CR.data-Qdataset.ginv_pr_CR('1999Q1').data,'Color',rgb('blue'),'Linewidth',1,'Linestyle',':');
hold on;
plot(T,Qdataset.ginv_pr_PR.data-Qdataset.ginv_pr_PR('1999Q1').data,'Color',rgb('red'),'Linewidth',1,'Linestyle','-.');
hold off;
title({'Government investment /', 'potential GDP'});
legend({'euro area', 'core', 'periphery'},'FontSize',4,'location','southwest')
xticks(x_axis)
xticklabels(x_labels)
axis('tight')
grid on
ax = gca;
ax.LineWidth=0.4;
ax.FontSize = 6; 
pbaspect([1 1 1])

subplot(1,3,3);
plot(T,Qdataset.gtrnsf_pr_EA.data-Qdataset.gtrnsf_pr_EA('1999Q1').data,'Color',rgb('black'),'Linewidth',1,'Linestyle','-');
hold on;
plot(T,Qdataset.gtrnsf_pr_CR.data-Qdataset.gtrnsf_pr_CR('1999Q1').data,'Color',rgb('blue'),'Linewidth',1,'Linestyle',':');
hold on;
plot(T,Qdataset.gtrnsf_pr_PR.data-Qdataset.gtrnsf_pr_PR('1999Q1').data,'Color',rgb('red'),'Linewidth',1,'Linestyle','-.');
hold off;
title({'Cash transfers /', 'potential GDP'});
xticks(x_axis)
xticklabels(x_labels)
axis('tight')
grid on
ax = gca;
ax.LineWidth=0.4;
ax.FontSize = 6; 
pbaspect([1 1 1])


h = gcf;

X = 12.8;                  %# these are dimensions of the slides
Y = 9.6/2;                  %# change proportions by dividing one of the dimentsions
xMargin = -1;               %# left/right margins from page borders
yMargin = -1;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (widht & hieght)
ySize = Y - 2*yMargin;     %# figure size on paper (widht & hieght)

%# figure size displayed on screen (50% scaled, but same aspect ratio)
set(h, 'Units','centimeters', 'Position',[0 0 xSize ySize])
movegui(h, 'center')

%# figure size printed on paper
set(h, 'PaperUnits','centimeters')
set(h, 'PaperSize',[X Y])
set(h, 'PaperPosition',[xMargin yMargin xSize ySize])
set(h, 'PaperOrientation','portrait')

%# export to PDF and open file
print -dpdf -r0 run_data_transformation_chart_gov_spend.pdf
copyfile('run_data_transformation_chart_gov_spend.pdf','../latex/XXXX-XX_draft-live/run_data_transformation_chart_gov_spend.pdf')
copyfile('run_data_transformation_chart_gov_spend.pdf','../latex/XXXX-XX_slides-live/run_data_transformation_chart_gov_spend.pdf')
close(1);

% ************************************************************************************
% plotting series for revenue
figure
subplot(1,4,1);
plot(T,Qdataset.dtl_pr_EA.data-Qdataset.dtl_pr_EA('1999Q1').data,'Color',rgb('black'),'Linewidth',1,'Linestyle','-');
hold on;
plot(T,Qdataset.dtl_pr_CR.data-Qdataset.dtl_pr_CR('1999Q1').data,'Color',rgb('blue'),'Linewidth',1,'Linestyle',':');
hold on;
plot(T,Qdataset.dtl_pr_PR.data-Qdataset.dtl_pr_PR('1999Q1').data,'Color',rgb('red'),'Linewidth',1,'Linestyle','-.');
hold off;
ylabel('change in pp. compared to 1999Q1')
title({'Direct tax', 'to wages and salaries'});
xticks(x_axis)
xticklabels(x_labels)
axis('tight')
grid on
ax = gca;
ax.LineWidth=0.4;
ax.FontSize = 6; 
pbaspect([1 1 1])

subplot(1,4,2);
plot(T,Qdataset.itx_pr_EA.data-Qdataset.itx_pr_EA('1999Q1').data,'Color',rgb('black'),'Linewidth',1,'Linestyle','-');
hold on;
plot(T,Qdataset.itx_pr_CR.data-Qdataset.itx_pr_CR('1999Q1').data,'Color',rgb('blue'),'Linewidth',1,'Linestyle',':');
hold on;
plot(T,Qdataset.itx_pr_PR.data-Qdataset.itx_pr_PR('1999Q1').data,'Color',rgb('red'),'Linewidth',1,'Linestyle','-.');
hold off;
title({'Indirect tax', 'to consumption'});
xticks(x_axis)
xticklabels(x_labels)
axis('tight')
grid on
ax = gca;
ax.LineWidth=0.4;
ax.FontSize = 6; 
pbaspect([1 1 1])

subplot(1,4,3);
plot(T,Qdataset.scf_pr_EA.data-Qdataset.scf_pr_EA('1999Q1').data,'Color',rgb('black'),'Linewidth',1,'Linestyle','-');
hold on;
plot(T,Qdataset.scf_pr_CR.data-Qdataset.scf_pr_CR('1999Q1').data,'Color',rgb('blue'),'Linewidth',1,'Linestyle',':');
hold on;
plot(T,Qdataset.scf_pr_PR.data-Qdataset.scf_pr_PR('1999Q1').data,'Color',rgb('red'),'Linewidth',1,'Linestyle','-.');
hold off;
title({'SSC by firms', 'to wages and salaries'});
xticks(x_axis)
xticklabels(x_labels)
axis('tight')
grid on
ax = gca;
ax.LineWidth=0.4;
ax.FontSize = 6; 
pbaspect([1 1 1])

subplot(1,4,4);
plot(T,Qdataset.sch_pr_EA.data-Qdataset.sch_pr_EA('1999Q1').data,'Color',rgb('black'),'Linewidth',1,'Linestyle','-');
hold on;
plot(T,Qdataset.sch_pr_CR.data-Qdataset.sch_pr_CR('1999Q1').data,'Color',rgb('blue'),'Linewidth',1,'Linestyle',':');
hold on;
plot(T,Qdataset.sch_pr_PR.data-Qdataset.sch_pr_PR('1999Q1').data,'Color',rgb('red'),'Linewidth',1,'Linestyle','-.');
hold off;
title({'SSC by HH', 'to wages and salaries'});
xticks(x_axis)
xticklabels(x_labels)
axis('tight')
grid on
ax = gca;
ax.LineWidth=0.4;
ax.FontSize = 6; 
pbaspect([1 1 1])

h = gcf;

X = 12.8;                  %# these are dimensions of the slides
Y = 9.6/2;                  %# change proportions by dividing one of the dimentsions
xMargin = -1;               %# left/right margins from page borders
yMargin = -1;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (widht & hieght)
ySize = Y - 2*yMargin;     %# figure size on paper (widht & hieght)

%# figure size displayed on screen (50% scaled, but same aspect ratio)
set(h, 'Units','centimeters', 'Position',[0 0 xSize ySize])
movegui(h, 'center')

%# figure size printed on paper
set(h, 'PaperUnits','centimeters')
set(h, 'PaperSize',[X Y])
set(h, 'PaperPosition',[xMargin yMargin xSize ySize])
set(h, 'PaperOrientation','portrait')

%# export to PDF and open file
print -dpdf -r0 run_data_transformation_chart_gov_rev.pdf

copyfile('run_data_transformation_chart_gov_rev.pdf','../latex/XXXX-XX_draft-live/run_data_transformation_chart_gov_rev.pdf')
copyfile('run_data_transformation_chart_gov_rev.pdf','../latex/XXXX-XX_slides-live/run_data_transformation_chart_gov_rev.pdf')
close(1);

% ************************************************************************************
% plotting series for primary, interest and debt
figure
subplot(1,3,1);
plot(T,Qdataset.pdf_pr_EA.data-Qdataset.pdf_pr_EA('1999Q1').data,'Color',rgb('black'),'Linewidth',1,'Linestyle','-');
hold on;
plot(T,Qdataset.pdf_pr_CR.data-Qdataset.pdf_pr_CR('1999Q1').data,'Color',rgb('blue'),'Linewidth',1,'Linestyle',':');
hold on;
plot(T,Qdataset.pdf_pr_PR.data-Qdataset.pdf_pr_PR('1999Q1').data,'Color',rgb('red'),'Linewidth',1,'Linestyle','-.');
hold off;
ylabel('change in pp. compared to 1999Q1')
title({'Primary balance /', 'potential GDP'});
legend({'euro area', 'core', 'periphery'},'FontSize',4,'location','northeast')
xticks(x_axis)
xticklabels(x_labels)
axis('tight')
grid on
ax = gca;
ax.LineWidth=0.4;
ax.FontSize = 6; 
pbaspect([1 1 1])

subplot(1,3,2);
plot(T,Qdataset.inp_pr_EA.data-Qdataset.inp_pr_EA('1999Q1').data,'Color',rgb('black'),'Linewidth',1,'Linestyle','-');
hold on;
plot(T,Qdataset.inp_pr_CR.data-Qdataset.inp_pr_CR('1999Q1').data,'Color',rgb('blue'),'Linewidth',1,'Linestyle',':');
hold on;
plot(T,Qdataset.inp_pr_PR.data-Qdataset.inp_pr_PR('1999Q1').data,'Color',rgb('red'),'Linewidth',1,'Linestyle','-.');
hold off;
title({'Interest payments /', 'potential GDP'});
xticks(x_axis)
xticklabels(x_labels)
axis('tight')
grid on
ax = gca;
ax.LineWidth=0.4;
ax.FontSize = 6; 
pbaspect([1 1 1])

subplot(1,3,3);
plot(T,Qdataset.gdebt_pr_EA.data-Qdataset.gdebt_pr_EA('1999Q1').data,'Color',rgb('black'),'Linewidth',1,'Linestyle','-');
hold on;
plot(T,Qdataset.gdebt_pr_CR.data-Qdataset.gdebt_pr_CR('1999Q1').data,'Color',rgb('blue'),'Linewidth',1,'Linestyle',':');
hold on;
plot(T,Qdataset.gdebt_pr_PR.data-Qdataset.gdebt_pr_PR('1999Q1').data,'Color',rgb('red'),'Linewidth',1,'Linestyle','-.');
hold off;
title({'Government debt /', 'potential GDP'});
xticks(x_axis)
xticklabels(x_labels)
axis('tight')
grid on
ax = gca;
ax.LineWidth=0.4;
ax.FontSize = 6; 
pbaspect([1 1 1])

h = gcf;

X = 12.8;                  %# these are dimensions of the slides
Y = 9.6/2;                  %# change proportions by dividing one of the dimentsions
xMargin = -1;               %# left/right margins from page borders
yMargin = -1;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (widht & hieght)
ySize = Y - 2*yMargin;     %# figure size on paper (widht & hieght)

%# figure size displayed on screen (50% scaled, but same aspect ratio)
set(h, 'Units','centimeters', 'Position',[0 0 xSize ySize])
movegui(h, 'center')

%# figure size printed on paper
set(h, 'PaperUnits','centimeters')
set(h, 'PaperSize',[X Y])
set(h, 'PaperPosition',[xMargin yMargin xSize ySize])
set(h, 'PaperOrientation','portrait')

%# export to PDF and open file
print -dpdf -r0 run_data_transformation_chart_gov_defdebt.pdf

copyfile('run_data_transformation_chart_gov_defdebt.pdf','../latex/XXXX-XX_draft-live/run_data_transformation_chart_gov_defdebt.pdf')
copyfile('run_data_transformation_chart_gov_defdebt.pdf','../latex/XXXX-XX_slides-live/run_data_transformation_chart_gov_defdebt.pdf')
close(1);

% ************************************************************************************
% ploting the series for gross government debt
figure
    plot(T,Qdataset.gdebt_pr_EA.data,'Color',rgb('black'),'Linewidth',3,'Linestyle','-');
    hold on;
    plot(T,Qdataset.gdebt_pr_CR.data,'Color',rgb('blue'),'Linewidth',3,'Linestyle',':');
    hold on;
    plot(T,Qdataset.gdebt_pr_PR.data,'Color',rgb('red'),'Linewidth',3,'Linestyle','-.');
    hold off;
grid on;
axis('tight');
ylabel('% of potential GDP')
legend('euro area', 'core', 'periphery');
export_fig run_data_transformation_chart_gross_debt.pdf -transparent
copyfile('run_data_transformation_chart_gross_debt.pdf','../latex/XXXX-XX_draft-live/run_data_transformation_chart_gross_debt.pdf')
copyfile('run_data_transformation_chart_gross_debt.pdf','../latex/XXXX-XX_slides-live/run_data_transformation_chart_gross_debt.pdf')
close(1);

end

%% ************************************************************************************
% Section 9: Creating an mfile for the estimation and shock decomposition
% (growth rates)
% ************************************************************************************
% Qdataset.POP_qq_CR = qgrowth(Qdataset.POP_lr_CR)*100;
% Qdataset.POP_qq_PR = qgrowth(Qdataset.POP_lr_PR)*100;

% Qdataset.GDP_qq_CR = qgrowth(Qdataset.GDP_lr_CR)*100;
% Qdataset.GDP_qq_PR = qgrowth(Qdataset.GDP_lr_PR)*100;
% Qdataset.GDP_dq_CR = qgrowth(Qdataset.GDP_df_CR)*100;
% Qdataset.GDP_dq_PR = qgrowth(Qdataset.GDP_df_PR)*100;
Qdataset.gcons_dq_CR = qgrowth(Qdataset.gcons_df_CR)*100;
Qdataset.gcons_dq_PR = qgrowth(Qdataset.gcons_df_PR)*100;
Qdataset.tinv_dq_CR = qgrowth(Qdataset.tinv_df_CR)*100;
Qdataset.tinv_dq_PR = qgrowth(Qdataset.tinv_df_PR)*100;
Qdataset.hhcons_dq_CR = qgrowth(Qdataset.hhcons_df_CR)*100;
Qdataset.hhcons_dq_PR = qgrowth(Qdataset.hhcons_df_PR)*100;

Qdataset.gcons_qq_CR = qgrowth(Qdataset.gcons_lr_CR)*100;
Qdataset.gcons_qq_PR = qgrowth(Qdataset.gcons_lr_PR)*100;
% Qdataset.ginv_qq_CR = qgrowth(Qdataset.ginv_lr_CR)*100;
% Qdataset.ginv_qq_PR = qgrowth(Qdataset.ginv_lr_PR)*100;    
Qdataset.gtrnsf_qq_CR = qgrowth(Qdataset.gtrnsf_cv_CR)*100;
Qdataset.gtrnsf_qq_PR = qgrowth(Qdataset.gtrnsf_cv_PR)*100;
% Qdataset.hhcons_qq_CR = qgrowth(Qdataset.hhcons_lr_CR)*100;
% Qdataset.hhcons_qq_PR = qgrowth(Qdataset.hhcons_lr_PR)*100;

% num_new_pct_qq = [Qdataset.GDP_qq_CR.data...
%                   Qdataset.GDP_qq_PR.data...
%                   Qdataset.gcons_qq_CR.data Qdataset.gcons_qq_PR.data...
%                   Qdataset.ginv_qq_CR.data Qdataset.ginv_qq_PR.data...
%                   Qdataset.gtrnsf_qq_CR.data Qdataset.gtrnsf_qq_PR.data...
%                   Qdataset.dtl_pr_CR.data Qdataset.dtl_pr_PR.data...
%                   Qdataset.itx_pr_CR.data Qdataset.itx_pr_PR.data...
%                   Qdataset.scf_pr_CR.data Qdataset.scf_pr_PR.data...
%                   Qdataset.sch_pr_CR.data Qdataset.sch_pr_PR.data...
%                   ];
          
num_new_qq = [Qdataset.GDP_qc_PR.data(2:end)-mean(Qdataset.GDP_qc_PR.data(2:end)) Qdataset.GDP_qc_CR.data(2:end)-mean(Qdataset.GDP_qc_CR.data(2:end))...
                  Qdataset.gpur_qc_PR.data(2:end)-mean(Qdataset.gpur_qc_PR.data(2:end)) Qdataset.gpur_qc_CR.data(2:end)-mean(Qdataset.gpur_qc_CR.data(2:end))...
                  Qdataset.ginv_qc_PR.data(2:end)-mean(Qdataset.ginv_qc_PR.data(2:end)) Qdataset.ginv_qc_CR.data(2:end)-mean(Qdataset.ginv_qc_CR.data(2:end))...
                  Qdataset.stn_qd_EA.data(2:end)-mean(Qdataset.stn_qd_EA.data(2:end))...
                  Qdataset.hhcons_qc_PR.data(2:end)-mean(Qdataset.hhcons_qc_PR.data(2:end)) Qdataset.hhcons_qc_CR.data(2:end)-mean(Qdataset.hhcons_qc_CR.data(2:end))...
                  Qdataset.pinv_qc_PR.data(2:end)-mean(Qdataset.pinv_qc_PR.data(2:end)) Qdataset.pinv_qc_CR.data(2:end)-mean(Qdataset.pinv_qc_CR.data(2:end))...
                  Qdataset.GDP_dq_PR.data(2:end)-mean(Qdataset.GDP_dq_PR.data(2:end)) Qdataset.GDP_dq_CR.data(2:end)-mean(Qdataset.GDP_dq_CR.data(2:end))...
                  ];

% txt_new_qq = {'EAB_ygap','EAA_ygap','EAB_cgy','EAA_cgy','EAB_igy','EAA_igy','EAB_try','EAA_try',...
%            'EAB_taun','EAA_taun',...
%            'EAB_tauc','EAA_tauc',...
%            'EAB_tauwf','EAA_tauwf',...
%            'EAB_tauwh','EAA_tauwh',...           
%            };

txt_new_qq = {'qYtot','fqYtot','qCgt','fqCgt','qIgt','fqIgt','qRECBt','qCtot','fqCtot','qIt','fqIt','qGDPdef','fqGDPdef'};
% txt_new_qq_estim = {'qYtot_estim','fqYtot_estim','qCgt_estim','fqCgt_estim','qIgt_estim','fqIgt_estim','RECBt_estim'};
% txt_new_fin_qq = txt_new_qq';

% data_counterfactual_savetomfile ('data_counterfactual_mfile', num, txt);

data_counterfactual_savetomfile ('data_counterfactual_mfile_qq', num_new_qq, txt_new_qq);
% data_counterfactual_savetomfile ('data_counterfactual_mfile_qq_estim', num_new_qq, txt_new_qq_estim);


%% Plotting the growth rates


var_list = {'POP_qq', 'POP_pr',...
            'GDP_qc', 'GDP_pr',...
            'GDP_df', 'GDP_dq',...    
            'hhcons_qc', 'hhcons_pr',...
            'pinv_qc', 'pinv_pr',...
            'pcomp_qc', 'pcomp_pr',...
            'pempl_qc', 'pempl_lr',...
            'pcomp_qe', 'pcomp_pe',...
            'gstm_qc', 'gstm_pr',...
            'ginc_qc', 'ginc_pr',...
            'gpur_qc', 'gpur_pr',...
            'ginv_qc', 'ginv_pr',...
            'wgs_qc', 'wgs_pr',...
            'gempl_qc', 'gempl_pr',...
            'wgs_qe', 'wgs_pe',...
            'taxprod_qc', 'taxprod_pr',...
            'tschhinc_qc', 'tschhinc_pr',...
            'sscfirm_qc', 'sscfirm_pr',...
            'sscfirm_qc', 'sscfirm_pr',...
            'unempl_pr',...
            'gdebt_qc', 'gdebt_pr',...
            'stn_df', 'stn_qd',...
            };

for i=1:length(var_list)
figure;

if strcmp(var_list{i},'stn_df') || strcmp(var_list{i},'stn_qd')
    plot(eval(strcat('Qdataset.',var_list{i},'_EA')),'Color',rgb('grey'),'Linewidth',1,'Linestyle','-');
else    
    plot(eval(strcat('Qdataset.',var_list{i},'_CR')),'Color',rgb('blue'),'Linewidth',1,'Linestyle','-');
    hold on;
    plot(eval(strcat('Qdataset.',var_list{i},'_PR')),'Color',rgb('red'),'Linewidth',1,'Linestyle','-');
end

axis('tight')
xtickangle(90)

h = gcf;

X = 21/2;                  %# these are dimensions of the slides
Y = 29.7/4;                  %# change proportions by dividing one of the dimentsions
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (widht & hieght)
ySize = Y - 2*yMargin;     %# figure size on paper (widht & hieght)

%# figure size displayed on screen (50% scaled, but same aspect ratio)
set(h, 'Units','centimeters', 'Position',[0 0 xSize ySize])
movegui(h, 'center')

%# figure size printed on paper
set(h, 'PaperUnits','centimeters')
set(h, 'PaperSize',[X Y])
set(h, 'PaperPosition',[xMargin yMargin xSize ySize])
set(h, 'PaperOrientation','portrait')

%# export to PDF and open file
print -dpdf -r0 aPlot.pdf
copyfile('aPlot.pdf',strcat('../latex/XXXX-XX_draft-live1/plot_',var_list{i},'.pdf'))
close(1);
end

% var_list = {'GDP_qq', 'GDP_pr',...
%             'GDP_df', 'GDP_dq',...    
%             'hhcons_qq', 'hhcons_pr',...
%             'gcons_qq', 'gcons_pr',...
%             'ginv_qq', 'ginv_pr',...
%             };
% 
%% looking at the deflators

ctry_list = {'CR', 'PR'};
color_list = {'blue', 'red'};
        
for i=1:length(ctry_list)
figure;
plot(eval(strcat('Qdataset.GDP_df_',ctry_list{i})),'Color',rgb(color_list{i}),'Linewidth',1,'Linestyle','-');
hold on;
plot(eval(strcat('Qdataset.hhcons_df_',ctry_list{i})),'Color',rgb(color_list{i}),'Linewidth',1,'Linestyle','-.');
hold on;
plot(eval(strcat('Qdataset.gcons_df_',ctry_list{i})),'Color',rgb(color_list{i}),'Linewidth',1,'Linestyle','--');
hold on;
plot(eval(strcat('Qdataset.tinv_df_',ctry_list{i})),'Color',rgb(color_list{i}),'Linewidth',1,'Linestyle',':');
axis('tight')
xtickangle(90)
lga = legend('GDP','HH cons.', 'public cons.', 'total inv.');
set(lga,'Location','SouthEast')

h = gcf;

X = 21/2;                  %# these are dimensions of the slides
Y = 29.7/4;                  %# change proportions by dividing one of the dimentsions
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (widht & hieght)
ySize = Y - 2*yMargin;     %# figure size on paper (widht & hieght)

%# figure size displayed on screen (50% scaled, but same aspect ratio)
set(h, 'Units','centimeters', 'Position',[0 0 xSize ySize])
movegui(h, 'center')

%# figure size printed on paper
set(h, 'PaperUnits','centimeters')
set(h, 'PaperSize',[X Y])
set(h, 'PaperPosition',[xMargin yMargin xSize ySize])
set(h, 'PaperOrientation','portrait')

%# export to PDF and open file
print -dpdf -r0 aPlot.pdf
copyfile('aPlot.pdf',strcat('../latex/XXXX-XX_draft-live1/plot_deflators_',ctry_list{i},'.pdf'))
close(1);
end

for i=1:length(ctry_list)
figure;
plot(eval(strcat('Qdataset.GDP_dq_',ctry_list{i})),'Color',rgb(color_list{i}),'Linewidth',1,'Linestyle','-');
hold on;
plot(eval(strcat('Qdataset.hhcons_dq_',ctry_list{i})),'Color',rgb(color_list{i}),'Linewidth',1,'Linestyle','-.');
hold on;
plot(eval(strcat('Qdataset.gcons_dq_',ctry_list{i})),'Color',rgb(color_list{i}),'Linewidth',1,'Linestyle','--');
hold on;
plot(eval(strcat('Qdataset.tinv_dq_',ctry_list{i})),'Color',rgb(color_list{i}),'Linewidth',1,'Linestyle',':');
axis('tight')
xtickangle(90)

h = gcf;

X = 21/2;                  %# these are dimensions of the slides
Y = 29.7/4;                  %# change proportions by dividing one of the dimentsions
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = X - 2*xMargin;     %# figure size on paper (widht & hieght)
ySize = Y - 2*yMargin;     %# figure size on paper (widht & hieght)

%# figure size displayed on screen (50% scaled, but same aspect ratio)
set(h, 'Units','centimeters', 'Position',[0 0 xSize ySize])
movegui(h, 'center')

%# figure size printed on paper
set(h, 'PaperUnits','centimeters')
set(h, 'PaperSize',[X Y])
set(h, 'PaperPosition',[xMargin yMargin xSize ySize])
set(h, 'PaperOrientation','portrait')

%# export to PDF and open file
print -dpdf -r0 aPlot.pdf
copyfile('aPlot.pdf',strcat('../latex/XXXX-XX_draft-live1/plot_deflatorsq_',ctry_list{i},'.pdf'))
close(1);
end

%% data extension of the Bundesbank dataset
[num,labels_Buba] = xlsread('data_24_09.xlsx','Data','A1:AN1');
QdatasetBuba = dseries(xlsread('data_24_09.xlsx','Data','A2:AN60'), dates('1999Q2'),labels_Buba);
Tempdataset = dseries(NaN,dates('1999Q1'):dates('2018Q4'));
[QdatasetBuba, Tempdataset] = align(QdatasetBuba, Tempdataset);
[Qdataset, Tempdataset] = align(Qdataset, Tempdataset);

for i=1:40
plot_single_var_Buba_extension(QdatasetBuba{QdatasetBuba.name{i}},Qdataset{Buba_2_ECB(QdatasetBuba.name{i})});
end

ECB_list    = {'GDP_qc_DE','GDP_d__DE','hhcons_qc_DE','pinv_qc_DE','exp_qc_DE','pempl_l__DE','pcomp_qe_DE','unempl_p__DE','gtrnsf_qc_DE','gcons_qc_DE','ginv_qc_DE','gempl_l__DE','wgs_qe_DE','gbb_p__DE','itx_p__DE','ZeroSeries','tschhinc_p__DE','sscfirm_p__DE','GDP_qc_DR','GDP_d__DR','hhcons_qc_DR','pinv_qc_DR','exp_qc_DR','pempl_l__DR','pcomp_qe_DR','unempl_p__DR','gtrnsf_qc_DR','stn_d__EA','gcons_qc_DR','ginv_qc_DR','gempl_l__DR','wgs_qe_DR','gbb_p__DR','itx_p__DR','ZeroSeries','tschhinc_p__DR','sscfirm_p__DR','GDP_qc_RW','GDP_df_RW','stn_d__RW'};

ECB_list_cp = {'GDP_qc_CR','GDP_d__CR','hhcons_qc_CR','pinv_qc_CR','exp_qc_DE','pempl_l__CR','pcomp_qe_CR','unempl_p__CR','gtrnsf_qc_CR','gcons_qc_CR','ginv_qc_CR','gempl_l__CR','wgs_qe_CR','gbb_p__CR','itx_p__CR','ZeroSeries','tschhinc_p__CR','sscfirm_p__CR','GDP_qc_PR','GDP_d__PR','hhcons_qc_PR','pinv_qc_PR','exp_qc_DR','pempl_l__PR','pcomp_qe_PR','unempl_p__PR','gtrnsf_qc_PR','stn_d__EA','gcons_qc_PR','ginv_qc_PR','gempl_l__PR','wgs_qe_PR','gbb_p__PR','itx_p__PR','ZeroSeries','tschhinc_p__PR','sscfirm_p__PR','GDP_qc_RW','GDP_df_RW','stn_d__RW'};

Buba_list = {'ger_gdp','ger_infl_gdp','ger_priv_cns','ger_priv_inv','ger_exp_euro','ger_priv_emp','ger_priv_wage','ger_u_rate','ger_transfers','ger_gov_cns','ger_gov_inv','ger_gov_emp','ger_gov_wage','ger_gov_deficit_ratio','ger_taxrate_cns','ger_taxrate_cap','ger_taxrate_lab_total','ger_taxrate_socer','eur_gdp','eur_infl_gdp','eur_priv_cns','eur_priv_inv','eur_exp_ger','eur_priv_emp','eur_priv_wage','eur_u_rate','eur_transfers','eur_i3m','eur_gov_cns','eur_gov_inv','eur_gov_emp','eur_gov_wage','eur_gov_deficit_ratio','eur_taxrate_cns','eur_taxrate_cap','eur_taxrate_lab_total','eur_taxrate_socer','row_gdp','row_infl_gdp','row_i3m'};

xlswrite('data_24_09_extended.xlsx',Buba_list,'Data','A1');
xlswrite('data_24_09_extended.xlsx',Qdataset{ECB_list{:}}(dates('1999Q2'):dates('2018Q4')).data,'Data','A2');

xlswrite('data_24_09_extended_cp.xlsx',Buba_list,'Data','A1');
xlswrite('data_24_09_extended_cp.xlsx',Qdataset{ECB_list_cp{:}}(dates('1999Q2'):dates('2018Q4')).data,'Data','A2');

fprintf('\nXls files for Dynare input created!\n');
