function res = cholette(varargin)
% PURPOSE: cholette
%          Temporal disaggregation with indicator.
% 		   Denton method, additive and proportional variants.
%--------------------------------------------------------------------------
% USAGE: cholette
% ------------------------------INPUTS------------------------------------%
% PURPOSE: Temporal disaggregation using the Cholette method.
% -------------------------------------------------------------------------
% SYNTAX: res = cholette(Y,x,ta,d,sc,op1,op2);
% -------------------------------------------------------------------------
% OUTPUT: res: a structure
%         res.meth  = 'Cholette';
%         res.N     = Number of low frequency data
%         res.ta    = Type of disaggregation
%         res.sc    = Frequency conversion
%         res.d     = Degree of differencing
%         res.y     = High frequency estimate
%         res.x     = High frequency indicator
%         res.U     = Low frequency residuals
%         res.u     = High frequency residuals
%         res.et    = Elapsed time
% -------------------------------------------------------------------------
% INPUT: Y: Nx1 ---> vector of low frequency data
%        x: nx1 ---> indicator
%        ta: type of disaggregation
%            ta=1 ---> sum (flow)
%            ta=2 ---> average (index)
%            ta=3 ---> last element (stock) ---> interpolation
%            ta=4 ---> first element (stock) ---> interpolation
%        d: objective function to be minimized: volatility of ...
%            d=0 ---> levels
%            d=1 ---> first differences
%            d=2 ---> second differences
%        sc: number of high frequency data points for each low frequency data point
%            Some examples:
%            sc=4 ---> annual to quarterly
%            sc=12 ---> annual to monthly
%            sc=3 ---> quarterly to monthly
%        op1: variants of the Cholette methodology
%            op1=1 ---> additive variant
%            op1=2 ---> proportional variant
%        anchor: regard or disregard historical available information
%            anchor=0 ---> disregard historical information
%            anchor=1 ---> take into account historical information
% -------------------------------------------------------------------------
% LIBRARY: denton_Guillermo,aggreg_Guillermo,dif_Guillermo,out_C_Guillermo,out_D_Guillermo,cholette_Guillermo 
% -------------------------------------------------------------------------
% SEE ALSO: tdprint, tdplot
% -------------------------------------------------------------------------
% REFERENCE: Denton, F.T. (1971) "Adjustment of monthly or quarterly
% series to annual totals: an approach based on quadratic minimization",
% Journal of the American Statistical Society, vol. 66, n. 333, p. 99-102.

% -------------------------------------------------------------------------
% written by:
%  Enrique M. Quilis
% Version 3.0 [December 2018]coming


t0 = clock;

% ------------------
% Renaming variables
% ------------------
Y=varargin{1};
x=varargin{2};
ta=varargin{3};
d=varargin{4};
sc=varargin{5};
op1=varargin{6};
anchor=varargin{7};
Y0=varargin{8};
x0=varargin{9};
 
% ------------------------
% Check for Correct Model.
% ------------------------
if anchor==1 && d==0 
   error ('*** It does not make sense to anchor when we are in Levels ***'); 
end

% -------------------------------------------------------------------------
% Checking d (minimization of the volatility of first differences or second
% differences).
% -------------
if ((d ~= 0) && (d ~= 1) && (d ~= 2) && (d ~= 3))
    error ('*** d must be either 0 or 1 or 2 or 3 ***');
end

% ---------------------------
% Computing input dimensions.
% ---------------------------
[N,M] = size(Y);
[n,m] = size(x);

% -------------------------------------------------------------------------
% Renaming variables for anchored models and computing input dimensions and 
% checking consistency among dimensions for initial conditions.
% -------------------------------------------------------------
  [NN,MM] = size(Y0);
  [nn,mm] = size(x0);
    if (mm > 1) || (MM > 1)
    error ('*** x0,Y0 HAS INCORRECT DIMENSION: col(x)>1 ***');
    elseif (NN > 3) || (nn > 3)
    error ('*** x0,Y0 HAS INCORRECT DIMENSION: row(x)>3 ***');
    else
    clear mm MM NN nn;
    end 

% --------------------------------------
% Checking consistency among dimensions.
% --------------------------------------
if ((m > 1) || (M > 1)) 
    error ('*** x HAS INCORRECT DIMENSION: col(x)>1 ***');
else
    clear m M ;
end

% -----------------------------------------------------------------------
% Defining combination of degree of differencing (d=0 or d=1 or d=2) and 
% variant (additive or proportional).
% -----------------------------------
FLAX = [1 2; 
        3 4;
        5 6];
  %      7 8];
flax = FLAX(d+1,op1);

% ----------------------------------------------------------------------- %
% --------------------- COMPUTING MODEL VARIABLES ----------------------- %
% ------------------------------------------------------------------------%

% ----------------------------------------
% Generation of aggregation matrix C: Nxn.
% ----------------------------------------
C = interp.aggreg(ta,N,sc,n,Y,x);

% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% Computing temporal aggregation matrix of the indicator (Nx1) and  low frequency residuals (Nx1).          
% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   if  sum(x-ones(length(x),1))~=0 % if there is indicator (both for additional or proportional) or if       %->  This way, we can use the matrixes of the original minimization 
       X = C*x;                    % indicator is zeros for additional approach, compute agreggation matrix  %->  problem with indicator when we do not have indicator. 
       U = Y - X;                                                                                            %->  The last analytical solution setting this aggregation matrices
   else                            % if indicator is vector of ones for proportional,                        %->  will be equivalent to the one we obtain when we develope the
       U=Y;                        % approach, do not compute agreggation matrix                             %->  analytical solution without indicator from the beggining
   end

% -----------------------------------------------------------------
% Computing difference matrix: nxn. (Note: initial conditions = 0).
% -----------------------------------------------------------------
switch flax
    case 1 %Level Additive : d=1.
        D = interp.dif(0,n,anchor);
    case 2 %Level Proportional : d=2.
        D = interp.dif(0,n,anchor);
    case 3 %First Differences Additive: d=3.
        D = interp.dif(1,n,anchor);
    case 4 %First Differences Proportional: d=4.
        D = interp.dif(1,n,anchor);
    case 5 %Second Differences Additive: d=5.
        D = interp.dif(2,n,anchor);
    case 6 %Second Differences Proportional: d=6.
        D = interp.dif(2,n,anchor);
%     case 7
%         D = interp.dif(3,n,anchor);
%     case 8  
%         D = interp.dif(3,n,anchor);
end %of switch flax.

%%  % ---------
    % CHOLETTE.
    % ---------

% ----------------------------------------------------------------------- %
% -------------------- COMPUTING ANALYTICAL SOLUTIONS ------------------- %
% ------------------------------------------------------------------------%
[years,obs]=size(C);
% ----------------------------------------------------
% Checking if we are in differences to apply Cholette.
% ----------------------------------------------------
if d==0 
   error ('*** To apply Cholette we need to be in differences always ***');
else 
    switch op1 
          case 1                                   
              Q=D'*D;                              % CHOLETTE ADDITIVE weighting matrix
          case 2                                   
              Q=inv(diag(x))'*(D'*D)*inv(diag(x)); % CHOLETTE PROPORTIONAL weighting matrix
    end
    
% -----------------------------------------------------------------------------------    
% CHOLETTE UNANCHORED. Stacked Partitioned Matrices. No inverse for Weighting Matrix.  
% -----------------------------------------------------------------------------------
     if anchor==0 

        matrix1=[Q C'; C zeros(years)];              %-> Matrices for the Analytical solution.            
        matrix2=[Q zeros(obs,years); C eye(years)];  %-> We can not invert the partitioned one
        matrix=matrix1\matrix2;                      %-> as in Denton because Q is singular
                                                
        if  sum(x-ones(length(x),1))==0         % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            y = matrix(1:obs,obs+1:end)*U;      %-> Here it is when it becomes again important 
        else                                    %-> the way we set up the temporal agreggation
            y = matrix*[x;U];                   %->  matrices  and low level residuals depending 
        end                                     %-> if we had indicator or not, and if not, if we  
            y=y(1:obs,:);                       %-> were in the additive or proportional method   
            u=y(obs+1:end,:);                   % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
% -----------------------------------------------------------------------------------------------------------------------------                           
% CHOLETTE ANCHORED. We can develope analytical solution (inverse exists for Weighting Matrix) or stack matrices (in comments).   
% -----------------------------------------------------------------------------------------------------------------------------                                  
     elseif anchor==1  
        
       switch flax  % INITIAL CONDITIONS (ANCHOR) MATRICES
             case 3 %First Differences Additive Cholette
                 IC=[(Y0(2,:)-x0(2,:)); zeros(obs-1,1)]; 
             case 4 %First Differences Proportional Cholette
                 IC=[(Y0(2,:)/x0(2,:)); zeros(obs-1,1)];
             case 5 %Second Differences Additive Cholette
                 IC=[2*(Y0(2,:)-x0(2,:))-(Y0(1,:)-x0(1,:)); -(Y0(2,:)-x0(2,:)); zeros(obs-2,1)];
             case 6 %Second Differences Proportional Cholette
                 IC=[2*(Y0(2,:)/x0(2,:))-(Y0(1,:)/x0(1,:)); -(Y0(2,:)/x0(2,:)); zeros(obs-2,1)];
%              case 7 %Second Differences Additive Cholette
%                  IC=[2*(Y0(3,:)-x0(3,:))-(Y0(1,:)-x0(1,:)); -(Y0(2,:)-x0(2,:)); -(Y0(3,:)-x0(3,:)); zeros(obs-2,1)];
%              case 8 %Second Differences Proportional Cholette
%                  IC=[2*(Y0(3,:)/x0(3,:))-(Y0(1,:)/x0(1,:)); -(Y0(2,:)/x0(2,:)); -(Y0(3,:)-x0(3,:)); zeros(obs-2,1)];
                 
       end
       
        if     op1==1 % CHOLETTE ANCHORED ADDITIVE
               u = inv(Q)*C' * inv(C*inv(Q)*C') * U;
               X0=(inv(Q)*D' - inv(Q)*C'*inv(C*inv(Q)*C')*C*inv(Q) * D') * IC;
               y=x+u+X0;
        elseif op1==2 % CHOLETTE ANCHORED PROPORTIONAL         
               u = inv(Q) * C' * inv(C*inv(Q)*C') * Y;
               X0=(inv(Q)*(inv(diag(x))'*D') - inv(Q)*C' * inv(C*inv(Q)*C') * C*inv(Q) * inv(diag(x))'*D')*IC;
               y=u+X0;
        else 
            error('op1 must be 1 or 2');
        end
        
     end
end



% -----------------------------------------------------------------------
% Loading the structure
% -----------------------------------------------------------------------
% Basic parameters
res.meth = 'Cholette';
switch op1
    case 1
        res.meth1 = 'Additive variant';
    case 2
        res.meth1 = 'Proportional variant';
end
res.N 	= N;
res.ta	= ta;
res.sc 	= sc;
res.d 	= d;
% -----------------------------------------------------------------------
% Series
res.y = y;
res.x = x;
res.U = U;
res.u = u;
% -----------------------------------------------------------------------
% Elapsed time
res.et  = etime(clock,t0);
