function backinterpTS = backCholette(lowfrequency,highfrequency,indicator,ta,d,sc,op1,anchor)

firstlow = lowfrequency.dates(1);
firsthigh = highfrequency.dates(1);
firstind = indicator.dates(1);

cutoff = min([lowfrequency.lastobservedperiod.time(1), highfrequency.lastobservedperiod.time(1), indicator.lastobservedperiod.time(1)]);
cutoffQ = dates(strcat(num2str(cutoff),'Q4'));
cutoffA = dates(strcat(num2str(cutoff),'Y'));

if firstobservedperiod(indicator) >= firsthigh
    firstlow = firstobservedperiod(indicator);
    firstlow = dates(strcat(num2str(firstlow.time(1)),'Y'));
    firsthigh = firstobservedperiod(indicator);
    firstind = firstobservedperiod(indicator);
end

revlow = dseries(flip(lowfrequency(firstlow:cutoffA).data),firstlow:cutoffA);
revhigh = dseries(flip(highfrequency(firsthigh:cutoffQ).data),firsthigh:cutoffQ);
revind = dseries(flip(indicator(firstind:cutoffQ).data),firstind:cutoffQ);

wrongwayTS = interp.cholette_d(revlow,revhigh,revind,ta,d,sc,op1,anchor);

firstww = wrongwayTS.dates(1);
lastww = wrongwayTS.dates(end);

backinterpTS = dseries(flip(wrongwayTS(firstww:lastww).data),firstww:lastww);

end