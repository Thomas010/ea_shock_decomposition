function INTERPOLATION_DSERIES=cholette_d(varargin)
% PURPOSE: Demo of cholette()
%          Temporal disaggregation with indicator.
% 		   Denton method, additive and proportional variants.
%--------------------------------------------------------------------------
% USAGE: cholette_d
% ------------------------------INPUTS------------------------------------%
% PURPOSE: Temporal disaggregation using the Cholette method.
% -------------------------------------------------------------------------
% SYNTAX: res = cholette(Y,x,ta,d,sc,op1,op2);
% -------------------------------------------------------------------------
% OUTPUT: res: a structure
%         res.meth  = 'Cholette';
%         res.N     = Number of low frequency data
%         res.ta    = Type of disaggregation
%         res.sc    = Frequency conversion
%         res.d     = Degree of differencing
%         res.y     = High frequency estimate
%         res.x     = High frequency indicator
%         res.U     = Low frequency residuals
%         res.u     = High frequency residuals
%         res.et    = Elapsed time
% -------------------------------------------------------------------------
% INPUT: Y: Nx1 ---> vector of low frequency data
%               ---> quarterly series
%        x: nx1 ---> indicator
%        ta: type of disaggregation
%            ta=1 ---> sum (flow)
%            ta=2 ---> average (index)
%            ta=3 ---> last element (stock) ---> interpolation
%            ta=4 ---> first element (stock) ---> interpolation
%        d: objective function to be minimized: volatility of ...
%            d=0 ---> levels
%            d=1 ---> first differences
%            d=2 ---> second differences
%        sc: number of high frequency data points for each low frequency data point
%            Some examples:
%            sc=4 ---> annual to quarterly
%            sc=12 ---> annual to monthly
%            sc=3 ---> quarterly to monthly
%        op1: variants of the Cholette methodology
%            op1=1 ---> additive variant
%            op1=2 ---> proportional variant
%        anchor: regard or disregard historical available information
%            anchor=0 ---> disregard historical information
%            anchor=1 ---> take into account historical information
% -------------------------------------------------------------------------
% LIBRARY: cholette,aggreg,dif 
% -------------------------------------------------------------------------
% SEE ALSO: tdprint, tdplot
% -------------------------------------------------------------------------
% REFERENCE: Denton, F.T. (1971) "Adjustment of monthly or quarterly
% series to annual totals: an approach based on quadratic minimization",
% Journal of the American Statistical Society, vol. 66, n. 333, p. 99-102.

% -------------------------------------------------------------------------
% written by:
%  Enrique M. Quilis
% Version 3.0 [December 2018]

%--------------------------------------------------------------------------
% Renaming Inputs
%--------------------------------------------------------------------------
lowfrequency=varargin{1};
highfrequency=varargin{2};
indicator=varargin{3};
ta=varargin{4};
d=varargin{5};
sc=varargin{6};
op1=varargin{7};
anchor=varargin{8};

%--------------------------------------------------------------------------
% Homogeneizing data 
%--------------------------------------------------------------------------
start=find(~isnan(lowfrequency), 1, 'first');
ending=find(~isnan(lowfrequency), 1, 'last');

initialyearA=lowfrequency.dates.time(start);
lastyearA = lowfrequency.dates.time(ending);

firstdate=strcat(num2str(initialyearA),'Q1');
lastdate=strcat(num2str(lastyearA),'Q4');

lowfrequency = lowfrequency(lowfrequency.dates(start):lowfrequency.dates(ending)); 
highfrequency = highfrequency(dates(firstdate):dates(lastdate));
indicator = indicator(dates(firstdate):dates(lastdate));

%--------------------------------------------------------------------------
% Retrieving dates for time spans
%--------------------------------------------------------------------------
check=highfrequency.data;
check=check(~isnan(check));

if (sum(1-check)==length(check) && sum(check)==0) || (sum(1-check)==0 && sum(check)==length(check(~isnan(check)))) %A18 DE SCF gets stuck here fore example
   firstdate=strcat(num2str(lowfrequency.firstdate.time(1,1)),'Q1');
   firstdate=dates(firstdate);
   initialdate=strcat(num2str(lowfrequency.firstdate.time(1,1)),'Q1');
   initialdate=dates(initialdate);
else
   firstdate=highfrequency.lastobservedperiod;
   initialdate=highfrequency.firstdate;
end

lastdate=dates(lastdate);

quarter=firstdate.time(1,2); 

firstyear=dates(strcat(num2str(firstdate.time(:,1)),'Y'));                                

lastyear = lowfrequency.lastobservedperiod;


%--------------------------------------------------------------------------
% Modifying annual figures in case of Partial Information
%--------------------------------------------------------------------------
if (sum(1-check)==length(check) && sum(check)==0) || (sum(1-check)==0 && sum(check)==length(check(~isnan(check)))) || ta~=1
    %nothing
else
   switch quarter
       case 1
           cumulative_sum=highfrequency(dates(firstdate));  
           cumulative_sum=dseries(cumulative_sum.data,firstyear);
           lowfrequency(firstyear)=lowfrequency(firstyear)-cumulative_sum;  
       case 2
           cumulative_sum=cumsum((highfrequency(dates(firstdate-1):dates(firstdate))));  
           cumulative_sum=dseries(cumulative_sum.data(2,:),firstyear);
           lowfrequency(firstyear)=lowfrequency(firstyear)-cumulative_sum;    
       case 3 
           cumulative_sum=cumsum((highfrequency(dates(firstdate-2):dates(firstdate))));  
           cumulative_sum=dseries(cumulative_sum.data(end),firstyear);
           remaining=lowfrequency(firstyear)-cumulative_sum;  
           firstyear=firstyear+1;
           firstdate=firstdate+1;
           remaining=dseries(remaining.data,firstdate);
           highfrequency(firstdate)=remaining;
           lowfrequency=lowfrequency(dates(firstyear):dates(lastyear));
       case 4
           disp('*******************************')
           disp('  No Partial Information Case  ')
           disp('*******************************')
           firstyear=firstyear+1;
   end
end

% ------------------------------------------------------------------------%
% --- CALLING THE FUNCTION: output is loaded in a structure called res ---%
% ------------------------------------------------------------------------%
Y=lowfrequency((dates(firstyear):dates(lastyear)));
Y=Y.data;
if (sum(1-check)==length(check) && sum(check)==0) || (sum(1-check)==0 && sum(check)==length(check(~isnan(check))))
   x=indicator((dates(firstdate):dates(lastdate)));
   Y0=dseries(0,dates(firstdate-1):dates(firstdate));
   x0=dseries(0,dates(firstdate-(d-1)):dates(firstdate));
else
   x=indicator((dates(firstdate+1):dates(lastdate)));
   Y0=highfrequency((dates(firstdate-(d-1)):dates(firstdate)));
   x0=indicator((dates(firstdate-(d-1)):dates(firstdate)));
end
x=x.data;
Y0=Y0.data;
x0=x0.data;

res_Cholette = interp.cholette(Y,x,ta,d,sc,op1,anchor,Y0,x0);

%-------------------------------------------------------------------------
% Creating dseries out of the interpolated observations and merging with
% historical data
%--------------------------------------------------------------------------
if (sum(1-check)==length(check) && sum(check)==0) || (sum(1-check)==0 && sum(check)==length(check(~isnan(check))))
   INTERPOLATION_DSERIES=dseries(res_Cholette.y,dates(initialdate):dates(lastdate));   
else
   highfrequency=highfrequency(dates(initialdate):dates(firstdate));
   INTERPOLATION=[highfrequency.data; res_Cholette.y];
   INTERPOLATION_DSERIES=dseries(INTERPOLATION,dates(initialdate):dates(lastdate));
end

end



