function [C] = aggreg(ta,N,sc,n,Y,x)
% PURPOSE: Generate a temporal aggregation matrix
% ------------------------------------------------------------
% SYNTAX: C = aggreg(op1,N,sc);
% ------------------------------------------------------------
% OUTPUT: C: N x (sc x N) temporal aggregation matrix
% ------------------------------------------------------------
% INPUT:   
%       ta: type of disaggregation
%            ta=1 ---> sum (flow)
%            ta=2 ---> average (index)
%            ta=3 ---> last element (stock) ---> interpolation
%            ta=4 ---> first element (stock) ---> interpolation
%        sc: number of high frequency data points for each low frequency data point
%            Some examples:
%            sc=4 ---> annual to quarterly
%            sc=12 ---> annual to monthly
%            sc=3 ---> quarterly to monthly
%         N: number of low frequency data
% ------------------------------------------------------------
% LIBRARY: aggreg_v
% ------------------------------------------------------------
% SEE ALSO: temporal_agg
% ------------------------------------------------------------
% NOTE: Use aggreg_v_X for extended interpolation

% written by:
%  Enrique M. Quilis

% Version 1.1 [December 2018]

% -------------------------------------------------
% Generation of aggregation matrix C=I(N) <kron> c.
% -------------------------------------------------
c = interp.aggreg_v(ta,sc);

% -------------------------------------------------------
% Temporal aggregation matrix for no-partial information.
% -------------------------------------------------------
C = kron(eye(N),c);

% --------------------------------------------------------
% Checking if we are in no-partial or partial information.
% --------------------------------------------------------
if sc==4
   HF=n/4;
elseif sc==12
   HF=n/12; 
elseif sc==3
   HF=n/3;
else
   error ('*** check desagreggation interval; possibly it is incorrect ***');
end

if (N-HF)~=0 % if in partial information, modify C
    subperiods=length(Y)*sc-length(x);
    C=C(:,(subperiods+1):end);
end