openfig([pwd '/figures/peri_shock_decomposition']);
set(gca, 'YTick', 4:12:76)
set(gca, 'YTickLabel', 2000:3:2018)
set(gca, 'FontSize', 12)
set(gca, 'LineWidth', 5)
gca.Children(1).String = 'Fiscal shocks';
gca.Children(3).String = 'Other shocks';
gca.Children(5).String = sprintf('Measurement \n      shocks');
gca.Children(7).String = 'Initial values';


X = 12;                  %# A3 paper size
Y =16;                  %# A3 paper size
xMargin = 0;               %# left/right margins from page borders
yMargin = 0;               %# bottom/top margins from page borders
xSize = 16;
ySize = 12;
%# figure size displayed on screen (50% scaled, but same aspect ratio)
set(fig, 'Units','centimeters', 'Position',[0 0 xSize ySize]/2);
movegui(fig, 'center');


%# figure size printed on paper
set(fig, 'PaperUnits','centimeters');
set(fig, 'PaperSize',[X Y]);
set(fig, 'PaperPosition',[xMargin yMargin xSize ySize]);
set(h, 'PaperOrientation','portrait');

%# export to PDF and open file
saveas(figure(1),[pwd '/figures/peri_shock_decomposition.pdf']);
saveas(figure(1),[pwd '/figures/core_shock_decomposition.pdf']);
