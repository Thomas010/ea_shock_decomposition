%% -------------------- DATA TRANSFORMATION FILE --------------------%%
%--------------------
% Clearing workspace.
%--------------------    
clear all; close all; clc;
%------------------
% Required package/path
%------------------
% Add some paths
addpath([pwd '\retrieve']);
addpath([pwd '\matlab'])
addpath([pwd '\export_fig_routines']);
addpath([pwd '\data']);

% calling a specific user input not tracked by GIT
call_credentials;
call_paths;

% Call Iris
addpath(iris_path);
iris.startup

% Call our project environment
envi = environment.setup();
%% defining lists

Ctry.Core    = ["AT","BE","DE","FI","FR","NL"];
Ctry.Core_EC = ["AUT","BEL","DEU","FIN","FRA","NLD"];
Ctry.Peri    = ["ES","GR","IE","IT","PT"];
Ctry.Peri_EC = ["ESP","GRC","IRL","ITA","PRT"];
Ctry.ALL     = [Ctry.Core Ctry.Peri];
Ctry.ALL_EC  = [Ctry.Core_EC Ctry.Peri_EC];
Ctry.Agg     = {'CR', 'PR', 'EA'};
Ctry.Grand   = [Ctry.ALL Ctry.Agg 'DR'];

range_Q = qq(1999):qq(2020,4);
range_A = yy(1999):yy(2020);
InterpRange_Q = qq(2000):qq(2019,4);
InterpRange_A = yy(2000):yy(2019);
%% ----------------------------------------
%  retrieve data from MNA database
%  ---------------------------------------- 

inx_Q = envi.Main{:, "Database"}=="MNA_Q";
inx_A = envi.Main{:, "Database"}=="MNA_A";

MNA.Q.Name = reshape(string(envi.Main(inx_Q, :).Name), 1, [ ]);
MNA.A.Name = reshape(string(envi.Main(inx_A, :).Name), 1, [ ]);
MNA.Q.Source = reshape(string(envi.Main(inx_Q, :).Source), 1, [ ]);
MNA.A.Source = reshape(string(envi.Main(inx_A, :).Source), 1, [ ]);
MNA.Q.KeySource = unique(reshape(string(envi.Main(inx_Q, :).KeySource), 1, [ ]));
MNA.A.KeySource = unique(reshape(string(envi.Main(inx_A, :).KeySource), 1, [ ]));
MNA.DB = unique(reshape(string(envi.Main(inx_Q, :).URL), 1, [ ]));

MNA.Q.FameNames = textual.xlist( ...
    ".", ...
    MNA.Q.KeySource(2), ... ...
    Ctry.ALL, ...
    MNA.Q.Source ...
    );
% Some adjustments are needed due to a lack of homogeneity inside a single
% database
MNA.Q.FameNames(contains(MNA.Q.FameNames, "SAL")) = strrep(MNA.Q.FameNames(contains(MNA.Q.FameNames, "SAL")), "MNA.Q", "ENA.Q");
MNA.Q.FameNames(contains(MNA.Q.FameNames, "FR.W2.S1.S1._Z.SAL")) = strrep(MNA.Q.FameNames(contains(MNA.Q.FameNames, "FR.W2.S1.S1._Z.SAL")), ".Q.Y.", ".Q.S.");
MNA.Q.FameNames(contains(MNA.Q.FameNames, "GR.W2.S1.S1._Z.SAL")) = strrep(MNA.Q.FameNames(contains(MNA.Q.FameNames, "GR.W2.S1.S1._Z.SAL")), ".Q.Y.", ".Q.S.");
MNA.Q.FameNames(contains(MNA.Q.FameNames, "PT.W2.S1.S1._Z.SAL")) = strrep(MNA.Q.FameNames(contains(MNA.Q.FameNames, "PT.W2.S1.S1._Z.SAL")), ".Q.Y.", ".Q.S.");
MNA.Q.FameNames(contains(MNA.Q.FameNames, "FR.W2.S1.S1.D.D11")) = strrep(MNA.Q.FameNames(contains(MNA.Q.FameNames, "FR.W2.S1.S1.D.D11")), ".Q.Y.", ".Q.S.");
MNA.Q.FameNames(contains(MNA.Q.FameNames, "DE.W2.S1.S1.D.D11")) = strrep(MNA.Q.FameNames(contains(MNA.Q.FameNames, "DE.W2.S1.S1.D.D11")), ".Q.Y.", ".Q.S.");

MNA.Q.OutputNames = textual.xlist( ...
    "_", ...
    Ctry.ALL, ...
    MNA.Q.Name...
    );

MNA.A.FameNames = textual.xlist( ...
    ".", ...
    MNA.A.KeySource, ...
    Ctry.ALL, ...
    MNA.A.Source ...
    );

MNA.A.OutputNames = textual.xlist( ...
   "_", ...
    Ctry.ALL, ...
    MNA.A.Name...
    );

fprintf('\nRetrieving MNA data...\n');   
MNA.Q.Qdataset = seriesFromFame(MNA.DB, MNA.Q.FameNames, range_Q, credentials, "OutputName=", MNA.Q.OutputNames, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
MNA.A.Adataset = seriesFromFame(MNA.DB, MNA.A.FameNames, range_A, credentials, "OutputName=", MNA.A.OutputNames, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
fprintf('\nDone!\n'); 

MNA.Q.NotRetrieved = setdiff(MNA.Q.OutputNames,fieldnames(MNA.Q.Qdataset));
MNA.A.NotRetrieved = setdiff(MNA.A.OutputNames,fieldnames(MNA.A.Adataset));

%% ----------------------------------------
%  retrieve data from QGFS database
%  ---------------------------------------- 
inx = envi.Main{:, "Database"}=="QGFS";

QGFS.Name = reshape(string(envi.Main(inx, :).Name), 1, [ ]);
QGFS.Source = reshape(string(envi.Main(inx, :).Source), 1, [ ]);
QGFS.KeySource = unique(reshape(string(envi.Main(inx, :).KeySource), 1, [ ]));
QGFS.DB = unique(reshape(string(envi.Main(inx, :).URL), 1, [ ]));

QGFS.FameNames = textual.xlist( ...
    ".", ...
    QGFS.KeySource, ...
    Ctry.ALL, ...
    QGFS.Source ...
    );

QGFS.OutputNames = textual.xlist( ...
   "_", ...
    Ctry.ALL, ...
    QGFS.Name...
    );

fprintf('\nRetrieving QGFS data...\n');   
QGFS.Qdataset = seriesFromFame(QGFS.DB, QGFS.FameNames, range_Q, credentials, "OutputName=", QGFS.OutputNames, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
fprintf('\nDone!\n');

QGFS.NotRetrieved = setdiff(QGFS.OutputNames,fieldnames(QGFS.Qdataset));
%% ----------------------------------------
%  retrieve data from GFS database
%  ---------------------------------------- 
inx = envi.Main{:, "Database"}=="GFS";

GFS.Name = reshape(string(envi.Main(inx, :).Name), 1, [ ]);
GFS.Source = reshape(string(envi.Main(inx, :).Source), 1, [ ]);
GFS.KeySource = unique(reshape(string(envi.Main(inx, :).KeySource), 1, [ ]));
GFS.DB = unique(reshape(string(envi.Main(inx, :).URL), 1, [ ]));

GFS.FameNames = textual.xlist( ...
    ".", ...
    GFS.KeySource(1), ...
    Ctry.ALL, ...
    GFS.Source ...
    );
GFS.FameNames(contains(GFS.FameNames, "W0.S13.S1.N.C.D612")) = strrep(GFS.FameNames(contains(GFS.FameNames, "W0.S13.S1.N.C.D612")), "E09.A", "GFS.A");

GFS.OutputNames = textual.xlist( ...
   "_", ...
    Ctry.ALL, ...
    GFS.Name...
    );

fprintf('\nRetrieving GFS data...\n');   
GFS.Adataset = seriesFromFame(GFS.DB, GFS.FameNames, range_A, credentials, "OutputName=", GFS.OutputNames, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
fprintf('\nDone!\n');

GFS.NotRetrieved = setdiff(GFS.OutputNames,fieldnames(GFS.Adataset));
%% ----------------------------------------
%  retrieve data from AMECO database
%  ---------------------------------------- 
inx = envi.Main{:, "Database"}=="AMECO";

AMECO.Name = reshape(string(envi.Main(inx, :).Name), 1, [ ]);
AMECO.Source = reshape(string(envi.Main(inx, :).Source), 1, [ ]);
AMECO.KeySource = unique(reshape(string(envi.Main(inx, :).KeySource), 1, [ ]));
AMECO.DB = unique(reshape(string(envi.Main(inx, :).URL), 1, [ ]));

AMECO.FameNames = textual.xlist( ...
    ".", ...
    AMECO.KeySource, ...
    Ctry.ALL_EC, ...
    AMECO.Source ...
    );

AMECO.OutputNames = textual.xlist( ...
   "_", ...
    Ctry.ALL, ...
    AMECO.Name...
    );

fprintf('\nRetrieving AMECO data...\n');   
AMECO.Adataset = seriesFromFame(AMECO.DB, AMECO.FameNames, range_A, credentials, "OutputName=", AMECO.OutputNames, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
fprintf('\nDone!\n');

AMECO.NotRetrieved = setdiff(AMECO.OutputNames,fieldnames(AMECO.Adataset));

%% ----------------------------------------
%  retrieve data from ECB database
%  ---------------------------------------- 
inx = envi.Main{:, "Database"}=="ECB";

ECB.Name = reshape(string(envi.Main(inx, :).Name), 1, [ ]);
ECB.Source = reshape(string(envi.Main(inx, :).Source), 1, [ ]);
ECB.KeySource = unique(reshape(string(envi.Main(inx, :).KeySource), 1, [ ]));
ECB.DB = unique(reshape(string(envi.Main(inx, :).URL), 1, [ ]));

ECB.FameNames = textual.xlist( ...
    ".", ...
    ECB.KeySource, ...
    Ctry.ALL, ...
    ECB.Source ...
    );

ECB.OutputNames = textual.xlist( ...
   "_", ...
    Ctry.ALL, ...
    ECB.Name...
    );

fprintf('\nRetrieving ECB data...\n');   
ECB.Qdataset = seriesFromFame(ECB.DB, ECB.FameNames, range_Q, credentials, "OutputName=", ECB.OutputNames, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
fprintf('\nDone!\n');

ECB.NotRetrieved = setdiff(ECB.OutputNames,fieldnames(ECB.Qdataset));

%% ----------------------------------------
%  retrieve data from LFS database
%  ---------------------------------------- 
inx = envi.Main{:, "Database"}=="LFS";

LFS.Name = reshape(string(envi.Main(inx, :).Name), 1, [ ]);
LFS.Source = reshape(string(envi.Main(inx, :).Source), 1, [ ]);
LFS.KeySource = unique(reshape(string(envi.Main(inx, :).KeySource), 1, [ ]));
LFS.DB = unique(reshape(string(envi.Main(inx, :).URL), 1, [ ]));

LFS.FameNames = textual.xlist( ...
    ".", ...
    LFS.KeySource, ...
    Ctry.ALL, ...
    LFS.Source ...
    );

LFS.OutputNames = textual.xlist( ...
   "_", ...
   Ctry.ALL, ...
   LFS.Name ...
    );
fprintf('\nRetrieving LFS data...\n');   
LFS.Qdataset = seriesFromFame(LFS.DB, LFS.FameNames, range_Q, credentials, "OutputName=", LFS.OutputNames, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
fprintf('\nDone!\n');
LFS.NotRetrieved = setdiff(LFS.OutputNames,fieldnames(LFS.Qdataset));

%% ----------------------------------------
%  retrieve data from Output database
%  ---------------------------------------- 
inx = envi.Main{:, "Database"}=="OUT";

OUT.Name = reshape(string(envi.Main(inx, :).Name), 1, [ ]);
OUT.Source = reshape(string(envi.Main(inx, :).Source), 1, [ ]);
OUT.KeySource = unique(reshape(string(envi.Main(inx, :).KeySource), 1, [ ]));
OUT.DB = unique(reshape(string(envi.Main(inx, :).URL), 1, [ ]));

OUT.FameNames = textual.xlist( ...
    "_", ...
    OUT.KeySource, ...
    Ctry.ALL, ...
    OUT.Source ...
    );

OUT.OutputNames = textual.xlist( ...
   "_", ...
   Ctry.ALL, ...
   OUT.Name ...
    );
fprintf('\nRetrieving OUT data...\n');   
OUT.Adataset = seriesFromFame(OUT.DB, OUT.FameNames, range_A, credentials, "OutputName=", OUT.OutputNames, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
fprintf('\nDone!\n');
OUT.NotRetrieved = setdiff(OUT.OutputNames,fieldnames(OUT.Adataset));

%% --------------------------------------------------------------------
%  retrieve annual SSC data (could be include inside the GFS retrieval)
%  --------------------------------------------------------------------
GFS.SSC.FameNames = textual.xlist( ...
    ".", ...
    "GFS.A.N", ...
    ["DE","AT","IE","IT"], ...
    ["W0.S13.S1.N.C.D611._Z._Z._Z.XDC._Z.S.V.N._T",...
    "W0.S13.S1.N.C.D613._Z._Z._Z.XDC._Z.S.V.N._T"] ...
    );

GFS.SSC.OutputNames = textual.xlist( ...
    "_", ...
    ["DE","AT","IE","IT"],...
    ["SCF_CV","SCH_CV"]...
);

fprintf('\nRetrieving annual SSC data...\n');
GFS.SSC.Adataset = seriesFromFame(GFS.DB, GFS.SSC.FameNames, range_A, credentials, "OutputName=", GFS.SSC.OutputNames,"ExcludeStatus=", [ ], "IncludeStatus=", @all);
fprintf('\nDone!\n'); 

%% ---------------------------------------------
%  Merging datasets and creating a main database
%  ---------------------------------------------

Qdataset = dbmerge(QGFS.Qdataset, MNA.Q.Qdataset, ECB.Qdataset, LFS.Qdataset);
Adataset = dbmerge(GFS.Adataset, MNA.A.Adataset, AMECO.Adataset, OUT.Adataset, GFS.SSC.Adataset);

MainDb = struct();
oldnames_Q = fieldnames(Qdataset);
newnames_Q = cellfun(@(x) split(x,'_'),oldnames_Q,'UniformOutput',false);

oldnames_A = fieldnames(Adataset);
newnames_A = cellfun(@(x) split(x,'_'),oldnames_A,'UniformOutput',false);

%% ---------------------------------------------
%  Merging datasets and creating a main database
%  ---------------------------------------------

for country = Ctry.ALL
    for i = 1:numel(oldnames_Q)
          MainDb.Q.(newnames_Q{i}{1}).(newnames_Q{i}{2}).(newnames_Q{i}{3}) = Qdataset.(oldnames_Q{i});
    end
    for j = 1:numel(oldnames_A)
          MainDb.A.(newnames_A{j}{1}).(newnames_A{j}{2}).(newnames_A{j}{3}) = Adataset.(oldnames_A{j});
    end
end


%% ------------------------
%  Computing employers SSC
%  ------------------------
for country = Ctry.ALL
    MainDb.A.(country).SSCFIRM.CV = MainDb.A.(country).SSCFIRMA.CV - MainDb.A.(country).SSCFIRMI.CV;
end

%% ---------------------------
%  Multiply Real Potential GDP
%  ---------------------------
for country = Ctry.ALL
    MainDb.A.(country).POTGDP.LR = MainDb.A.(country).POTGDP.LR * 1000;
end

%% --------------------------------
%  Correcting outliers for IRELAND
%  --------------------------------
for unit = ["CV", "LR"]
    % GDP ADJUSTMENTS
    MainDb.Q.IE.GDP.(unit)(qq(2016,4)) = (MainDb.Q.IE.GDP.(unit)(qq(2016,3))+MainDb.Q.IE.GDP.(unit)(qq(2017,1)))/2; % Is that useful?
    % removing the permanent increase from 2015 by extending the cumulated
    % growth factors after this date
    gRateReference = MainDb.Q.IE.GDP.(unit);
    MainDb.Q.IE.GDP.(unit)(qq(2015,1)) = MainDb.Q.IE.GDP.(unit)(qq(2014,4))*MainDb.Q.IE.GDP.(unit)(qq(2014,4))/MainDb.Q.IE.GDP.(unit)(qq(2014,3));
    for date = qq(2015,2):MainDb.Q.IE.GDP.(unit).end
        growthRate = gRateReference(date)/gRateReference(date-1);
        MainDb.Q.IE.GDP.(unit)(date) = growthRate* MainDb.Q.IE.GDP.(unit)(date-1);
    end
    
    % TINV ADJUSTMENTS
    % assuming something more reasonable for Ireland given that investment
    % is shooting through the roof, we assume the growht of Austrian
    % investment, another small open economy
    gRateReference = MainDb.Q.AT.TINV.(unit);
    for date = qq(2015,1):MainDb.Q.IE.TINV.(unit).end
        growthRate = gRateReference(date)/gRateReference(date-1);
        MainDb.Q.IE.TINV.(unit)(date) = growthRate* MainDb.Q.IE.TINV.(unit)(date-1);
    end
end

%% ------------------------------------------------
%  Correcting outliers for ITALY, GREECE and SPAIN
%  ------------------------------------------------
MainDb.Q.IT.GINV.CV(qq(2002,4)) = (MainDb.Q.IT.GINV.CV(qq(2002,3))+MainDb.Q.IT.GINV.CV(qq(2003,1)))/2;
MainDb.Q.GR.GINV.CV(qq(2017,3)) = (MainDb.Q.GR.GINV.CV(qq(2017,2))+MainDb.Q.GR.GINV.CV(qq(2017,4)))/2;
MainDb.Q.ES.GINV.CV(qq(2015,3)) = (MainDb.Q.ES.GINV.CV(qq(2015,2))+MainDb.Q.ES.GINV.CV(qq(2015,4)))/2;

%% ------------------------------------
%  Correcting outliers for NETHERLANDS
%  ------------------------------------
% we do not adjust GDP as it seems to be unaffected
for unit = ["CV", "LR"]
    MainDb.Q.NL.TINV.(unit)(qq(2015,1)) =  2/3*MainDb.Q.NL.TINV.(unit)(qq(2014,4)) + 1/3*MainDb.Q.NL.TINV.(unit)(qq(2015,3));
    MainDb.Q.NL.TINV.(unit)(qq(2015,2)) =  1/3*MainDb.Q.NL.TINV.(unit)(qq(2014,4)) + 2/3*MainDb.Q.NL.TINV.(unit)(qq(2015,3));
end

%% --------------------------------
%  Correcting outliers for FINLAND
%  -------------------------------
% backcasting the series in line with the debt accumulation eq.
% gdebt(t)=gdebt(t-1)-gbb(t) -> gdebt(t-1)=gdebt(t)+gbb(t)
% this assumes DDA=0, which should be true only over time
for date = qq(1999,4):-1:qq(1999,1)
        MainDb.Q.FI.GDEBT.CV(date) = MainDb.Q.FI.GDEBT.CV(date+1) + MainDb.Q.FI.GBBRAW.CV(date+1);
end
%% ----------------------------------------------------------------------------
%  Fill last year of SCT split for IT (only if last available data is not Q4)
%  ----------------------------------------------------------------------------
lastQuarter = dat2char(MainDb.Q.FR.SCT.CV.end);
if lastQuarter(end) ~= '4'
    fill_SSC_IT_iris
end
%% ----------------------------------
%  SSC Split for IT, DE, IE and AT
%  ----------------------------------
for item = ["SCF", "SCH"]
    for country = ["DE", "IE", "AT", "IT"] 
        try
            anchor = MainDb.Q.(country).(item).CV; 
        catch
            anchor = [ ]; % Only IT does not have anchor
        end
        MainDb.Q.(country).(item).CV = genip(...
            MainDb.A.(country).(item).CV, ...
            Frequency.QUARTERLY,...
            2, ...
            "sum",...
            "Indicator.Model=", "Ratio", ...
            "Indicator.Level=", MainDb.Q.(country).SCT.CV, ...
            "Range=", InterpRange_A,...
            "Hard.Level=", anchor...
            );
    end
end


%% --------------
%  Interpolation
%  --------------

% First we run Sum variables
inx = envi.Main{:, "Type"}=="Sum";
listSum = envi.Main(inx, :).Name;
listSum = reshape(string(listSum), 1, [ ]);

for item = listSum
    indicatorName = envi.Main{item, "Indicator"};
    indicatorName = cellfun(@(x) split(x,'_'),indicatorName,'UniformOutput',false);
    item = cellfun(@(x) split(x,'_'),item,'UniformOutput',false);
    for country = Ctry.ALL
        try
            anchor = MainDb.Q.(country).(item{1}{1}).(item{1}{2});
        catch
            anchor = [ ];
        end
        try
            Indicator = MainDb.Q.(country).(indicatorName{1}{1}).(indicatorName{1}{2});
        catch
            Indicator = [ ];
        end
        
        MainDb.Q.(country).(item{1}{1}).(item{1}{2}) = genip( ...
            MainDb.A.(country).(item{1}{1}).(item{1}{2}),... 
            Frequency.QUARTERLY,...
            2,...
            "Sum", ...
            "Indicator.Model=", "Ratio", ...
            "Indicator.Level=", Indicator, ...
            "Hard.Level=", anchor, ...
            "Range=", InterpRange_A ...
        );
    end
end


% Then we run Mean variables
inx = envi.Main{:, "Type"}=="Mean";
listMean = envi.Main(inx, :).Name;
listMean = reshape(string(listMean), 1, [ ]);

for item = listMean
    indicatorName = envi.Main{item, "Indicator"};
    indicatorName = cellfun(@(x) split(x,'_'),indicatorName,'UniformOutput',false);
    item = cellfun(@(x) split(x,'_'),item,'UniformOutput',false);
    for country = Ctry.ALL
        try
            anchor = MainDb.Q.(country).(item{1}{1}).(item{1}{2});
        catch
            anchor = [ ];
        end
        try
            Indicator = MainDb.Q.(country).(indicatorName{1}{1}).(indicatorName{1}{2});
        catch
            Indicator = [ ];
        end
        
        MainDb.Q.(country).(item{1}{1}).(item{1}{2}) = genip( ...
            MainDb.A.(country).(item{1}{1}).(item{1}{2}), ...
            Frequency.QUARTERLY,...
            2,...
            "Mean", ...
            "Indicator.Model=", "Ratio", ...
            "Indicator.Level=", Indicator, ...
            "Hard.Level=", anchor, ...
            "Range=", InterpRange_A ...
        );
    end
end

%% --------------------------------------------------------------
% Aggregating the data for the core, periphery and the euro area
% ---------------------------------------------------------------

inx = envi.Main{:, "Aggregation"}==1;

listAgg = envi.Main(inx, :).Name;
listAgg = reshape(string(listAgg), 1, [ ]);
listAgg = cellfun(@(x) split(x,'_'),listAgg,'UniformOutput',false);

for item = listAgg
   % Aggregate Core Countries 
   MainDb.Q.CR.(item{1}{1}).(item{1}{2}) = tseries(MainDb.Q.FR.(item{1}{1}).(item{1}{2}).start:MainDb.Q.FR.(item{1}{1}).(item{1}{2}).end, 0);
   for country =  Ctry.Core
       MainDb.Q.CR.(item{1}{1}).(item{1}{2}) = MainDb.Q.(country).(item{1}{1}).(item{1}{2}) + MainDb.Q.CR.(item{1}{1}).(item{1}{2});
   end
   % Aggregate Peri Countries 
   MainDb.Q.PR.(item{1}{1}).(item{1}{2}) = tseries(MainDb.Q.FR.(item{1}{1}).(item{1}{2}).start:MainDb.Q.FR.(item{1}{1}).(item{1}{2}).end, 0);
   for country =  Ctry.Peri
       MainDb.Q.PR.(item{1}{1}).(item{1}{2}) = MainDb.Q.(country).(item{1}{1}).(item{1}{2}) + MainDb.Q.PR.(item{1}{1}).(item{1}{2});
   end
   % Aggregate EA
   MainDb.Q.EA.(item{1}{1}).(item{1}{2}) = MainDb.Q.CR.(item{1}{1}).(item{1}{2})+ MainDb.Q.PR.(item{1}{1}).(item{1}{2});
   
   % Aggregate All Countries (equivalent to EA, useful?)
   MainDb.Q.DR.(item{1}{1}).(item{1}{2}) = tseries(MainDb.Q.FR.(item{1}{1}).(item{1}{2}).start:MainDb.Q.FR.(item{1}{1}).(item{1}{2}).end, 0);
   for country =  Ctry.ALL
       MainDb.Q.DR.(item{1}{1}).(item{1}{2}) = MainDb.Q.(country).(item{1}{1}).(item{1}{2}) + MainDb.Q.DR.(item{1}{1}).(item{1}{2});
   end
end

%% ---------------------------------------------------------------------
% Calculating necessary series (e.g. ratios to potential GDP, deflators)
% ----------------------------------------------------------------------
list.DF = ["HHCONS_LR", "GCONS_LR","TINV_LR"];
list.DF = cellfun(@(x) split(x,'_'),list.DF,'UniformOutput',false);

list.LR = ["HHCONS_CV","PINV_CV", "GPUR_CV","GSTM_CV", "GINC_CV", "GINV_CV", "GTRNSF_CV", "GCONS_CV"]; 
list.LR = cellfun(@(x) split(x,'_'),list.LR,'UniformOutput',false);

list.LC = ["GDP_LR","GCONS_LR","HHCONS_LR", "PINV_LR", "PEMPL_LR", "GINV_LR", "GEMPL_LR", "GTRNSF_LR"];
list.LC = cellfun(@(x) split(x,'_'),list.LC,'UniformOutput',false);

list.QC = ["GDP_LC","HHCONS_LC","PINV_LC", "GPUR_LR", "GSTM_LR", "GINC_LR", "GCONS_LC", "GINV_LC",...
          "GTRNSF_LC", "TSCHHINC_CV", "SSCFIRM_CV", "GDEBT_CV"];
list.QC = cellfun(@(x) split(x,'_'),list.QC,'UniformOutput',false);

list.PR = ["PINV_LR","GPUR_LR","GSTM_LR", "GINC_LR", "GCONS_LR", "GINV_LR", "GTRNSF_LR","INP_CV","PDF_CV"];
list.PR = cellfun(@(x) split(x,'_'),list.PR,'UniformOutput',false);

for country = Ctry.Grand
    % Create new series which have to be initialise first
    MainDb.Q.(country).GDP.DF = MainDb.Q.(country).GDP.CV/MainDb.Q.(country).GDP.LR*100;
    MainDb.Q.(country).GCONS.LR = MainDb.Q.(country).GCONS.CV/MainDb.Q.(country).GDP.LR*100;
    MainDb.Q.(country).POP.QQ = log(MainDb.Q.(country).POP.LR / MainDb.Q.(country).POP.LR{-1});
    MainDb.Q.(country).PINV.CV = MainDb.Q.(country).TINV.CV - MainDb.Q.(country).GINV.CV;
    MainDb.Q.(country).WGS.CV = MainDb.Q.(country).GCOMP.CV - MainDb.Q.(country).IMPSSC.CV;
    MainDb.Q.(country).PCOMP.CV = MainDb.Q.(country).TCOMP.CV - MainDb.Q.(country).WGS.CV;
    MainDb.Q.(country).GPUR.CV = MainDb.Q.(country).GINC.CV + MainDb.Q.(country).GSTM.CV;
    MainDb.Q.(country).TSCHHINC.CV = MainDb.Q.(country).TAXHHINC.CV + MainDb.Q.(country).SSCHHINC.CV;
    MainDb.Q.(country).DTL.CV = 0.7 * MainDb.Q.(country).DTX.CV ;
    MainDb.Q.(country).PDF.CV = MainDb.Q.(country).GBBRAW.CV + MainDb.Q.(country).INP.CV;
    MainDb.Q.(country).GBB.CV = MainDb.Q.(country).GBBRAW.CV + (0 * MainDb.Q.(country).CTRNSF.CV) + MainDb.Q.(country).NAFA.CV;
    MainDb.Q.(country).PEMPL.LR = MainDb.Q.(country).TEMPL.LR -MainDb.Q.(country).GEMPL.LR;    
    
    % Create series in real term (LR)
    for item = list.LR
        if string(item{1}{1}) ~= "GPUR" && string(item{1}{1}) ~= "GSTM" && string(item{1}{1}) ~= "GINC"
            MainDb.Q.(country).(item{1}{1}).LR = MainDb.Q.(country).(item{1}{1}).(item{1}{2})/MainDb.Q.(country).GDP.DF*100;
        else
            MainDb.Q.(country).HHCONS.DF = MainDb.Q.(country).HHCONS.CV/MainDb.Q.(country).HHCONS.LR*100;            
            MainDb.Q.(country).(item{1}{1}).LR = MainDb.Q.(country).(item{1}{1}).(item{1}{2})/MainDb.Q.(country).HHCONS.DF*100;            
        end
    end
    
    % Compute POTGDV in current term (CV)
    MainDb.Q.(country).POTGDP.CV = MainDb.Q.(country).POTGDP.LR * MainDb.Q.(country).GDP.DF/100;

    % Create deflated series (DF)
    for item = list.DF

        MainDb.Q.(country).(item{1}{1}).DF = MainDb.Q.(country).(item{1}{1}).CV/MainDb.Q.(country).(item{1}{1}).(item{1}{2})*100;

    end
    
    MainDb.Q.(country).GDP.DQ = log(MainDb.Q.(country).GDP.DF / MainDb.Q.(country).GDP.DF{-1});
    
    % Create per capita series (LC)
    for item = list.LC
           
        MainDb.Q.(country).(item{1}{1}).LC = MainDb.Q.(country).(item{1}{1}).(item{1}{2})/MainDb.Q.(country).POP.(item{1}{2});
        
    end
    
    % Create difference of log series (QC)
    for item = list.QC
        if string(item{1}{2}) ~= "CV" 
            MainDb.Q.(country).(item{1}{1}).QC = log(MainDb.Q.(country).(item{1}{1}).(item{1}{2})/MainDb.Q.(country).(item{1}{1}).(item{1}{2}){-1});
        else
            MainDb.Q.(country).(item{1}{1}).QC = log(MainDb.Q.(country).(item{1}{1}).(item{1}{2})/MainDb.Q.(country).(item{1}{1}).(item{1}{2}){-1})*100 - MainDb.Q.(country).POP.QQ;            
        end
    end

    % Create POTGDP ratio series (PR)
    for item = list.PR

        MainDb.Q.(country).(item{1}{1}).PR = MainDb.Q.(country).(item{1}{1}).(item{1}{2})/MainDb.Q.(country).POTGDP.(item{1}{2})*100;

    end
    % Rest of PR variables
    MainDb.Q.(country).GDP.PR = (MainDb.Q.(country).GDP.LR/MainDb.Q.(country).POTGDP.LR - 1) * 100;
    MainDb.Q.(country).TAXPROD.PR = MainDb.Q.(country).TAXPROD.CV/MainDb.Q.(country).HHCONS.CV * 100;
    MainDb.Q.(country).TSCHHINC.PR = MainDb.Q.(country).TSCHHINC.CV/MainDb.Q.(country).TCOMP.CV;
    MainDb.Q.(country).SSCFIRM.PR = MainDb.Q.(country).SSCFIRM.CV/MainDb.Q.(country).TCOMP.CV;
    MainDb.Q.(country).DTX.PR = MainDb.Q.(country).DTX.CV/MainDb.Q.(country).TCOMP.CV * 100;
    MainDb.Q.(country).SCT.PR = MainDb.Q.(country).SCT.CV/MainDb.Q.(country).TCOMP.CV * 100;
    MainDb.Q.(country).SCH.PR = MainDb.Q.(country).SCH.CV/MainDb.Q.(country).TCOMP.CV * 100;
    MainDb.Q.(country).SCF.PR = MainDb.Q.(country).SCF.CV/MainDb.Q.(country).TCOMP.CV * 100;
    MainDb.Q.(country).DTL.PR = MainDb.Q.(country).DTL.CV/MainDb.Q.(country).TCOMP.CV * 100;
    MainDb.Q.(country).ITX.PR = MainDb.Q.(country).TAXPROD.CV/MainDb.Q.(country).HHCONS.CV; % Same as TAXPROD.PR without *100
    MainDb.Q.(country).UNEMPL.PR = MainDb.Q.(country).UNEMPL.LR/MainDb.Q.(country).ACTIVEPOP.LR;
    MainDb.Q.(country).GBB.PR = MainDb.Q.(country).GBB.CV/MainDb.Q.(country).GDP.CV;
    MainDb.Q.(country).NAFA.PR = MainDb.Q.(country).NAFA.CV/MainDb.Q.(country).GDP.CV;
    MainDb.Q.(country).CTRNSF.PR = MainDb.Q.(country).CTRNSF.CV/MainDb.Q.(country).GDP.CV;
    MainDb.Q.(country).GDEBT.PR = MainDb.Q.(country).GDEBT.CV/MainDb.Q.(country).GDP.CV * 100;
    
    % PE and QE variables
    MainDb.Q.(country).WGS.PE = MainDb.Q.(country).WGS.CV/MainDb.Q.(country).GEMPL.LR;
    MainDb.Q.(country).PCOMP.PE = MainDb.Q.(country).PCOMP.CV/MainDb.Q.(country).PEMPL.LR;
    MainDb.Q.(country).WGS.QE = log(MainDb.Q.(country).WGS.PE/MainDb.Q.(country).WGS.PE{-1});
    MainDb.Q.(country).PCOMP.QE = log(MainDb.Q.(country).PCOMP.PE/MainDb.Q.(country).PCOMP.PE{-1});
end

% Loading STN data, Export data and RoW data
MainDb.Q.EA.STN.DF = tseries(qq(1999), xlsread('data/LemkeVladu_SSR_1999-2018_adaptive.xlsx', 'SSR quarterly'));
MainDb.Q.DE.EXP.CV = tseries(qq(1999), xlsread('data/data_non-fame.xlsx','Data','B7:B86'));
MainDb.Q.DR.EXP.CV = tseries(qq(1999), xlsread('data/data_non-fame.xlsx','Data','C7:C86'));
MainDb.Q.RW.GDP.QC = tseries(qq(1999), xlsread('RoW_from_EXT.xlsx','Calculations_GDP','C24:C103'));
MainDb.Q.RW.GDP.DF = tseries(qq(1999), xlsread('RoW_Haver.xlsx','Data','C6:C85'));
MainDb.Q.RW.STN.DF = tseries(qq(1999), xlsread('RoW_Haver.xlsx','FFR','B6:B85'));

MainDb.Q.EA.STN.QD = MainDb.Q.EA.STN.DF - MainDb.Q.EA.STN.DF{-1};

% Calculation of export 
for country = ["DE", "DR"]
    MainDb.Q.(country).EXP.LR = MainDb.Q.(country).EXP.CV / MainDb.Q.(country).GDP.DF * 100;
    MainDb.Q.(country).EXP.LC = MainDb.Q.(country).EXP.LR / MainDb.Q.(country).POP.LR;
    MainDb.Q.(country).EXP.QC = log(MainDb.Q.(country).EXP.LC / MainDb.Q.(country).EXP.LC{-1});
end

%% -------------------
% Calculation stage 2
% --------------------

list.L = ["PEMPL_LC", "GEMPL_LC"];
list.L = cellfun(@(x) split(x,'_'),list.L,'UniformOutput',false);

list.P = ["UNEMPL_PR", "GBB_PR", "ITX_PR","TSCHHINC_PR", "SSCFIRM_PR"];
list.P = cellfun(@(x) split(x,'_'),list.P,'UniformOutput',false);

for country = ["DE","DR","CR","PR"]
    MainDb.Q.(country).GDP.D = MainDb.Q.(country).GDP.DQ - mean(MainDb.Q.(country).GDP.DQ);
    
    for item = list.L
        
        MainDb.Q.(country).(item{1}{1}).L = MainDb.Q.(country).(item{1}{1}).(item{1}{2}) - mean(MainDb.Q.(country).(item{1}{1}).(item{1}{2}));
        
    end
    
    for item = list.P
        
        MainDb.Q.(country).(item{1}{1}).P = MainDb.Q.(country).(item{1}{1}).(item{1}{2}) - mean(MainDb.Q.(country).(item{1}{1}).(item{1}{2}));
        
    end
end


MainDb.Q.EA.STN.D = (MainDb.Q.EA.STN.DF - mean(MainDb.Q.EA.STN.DF)) / 100;

MainDb.Q.RW.GDP.QC = (MainDb.Q.RW.GDP.QC - mean(MainDb.Q.RW.GDP.QC))/100;
MainDb.Q.RW.GDP.DF = MainDb.Q.RW.GDP.DF - mean(MainDb.Q.RW.GDP.DF);
MainDb.Q.RW.STN.D = MainDb.Q.RW.STN.DF - mean(MainDb.Q.RW.STN.DF);

%% -------------------------------------------------------
% Flatening the MainDb struct to saving into a CSV format
% --------------------------------------------------------

country = fieldnames(MainDb.A);
for i = 1:numel(country)
    item = fieldnames(MainDb.A.(country{i}));
    
    for j = 1:numel(item)
        unit = fieldnames(MainDb.A.(country{i}).(item{j}));
        
        for k = 1:numel(unit)
            
            Data.A.(strjoin({country{i},item{j},unit{k}},'_')) = MainDb.A.(country{i}).(item{j}).(unit{k});
            
        end
    end
end

country = fieldnames(MainDb.Q);
for i = 1:numel(country)
    item = fieldnames(MainDb.Q.(country{i}));
    
    for j = 1:numel(item)
        unit = fieldnames(MainDb.Q.(country{i}).(item{j}));
        
        for k = 1:numel(unit)
            
            Data.Q.(strjoin({country{i},item{j},unit{k}},'_')) = MainDb.Q.(country{i}).(item{j}).(unit{k});
            
        end
    end
end

%% ---------------------------------
% Save the data and clear workspace
% ----------------------------------

databank.toCSV(Data.Q,    fullfile('data', 'Qdataset.csv'),  Inf);
databank.toCSV(Data.A,    fullfile('data', 'Adataset.csv'),  Inf);
 
clear QGFS.Qdataset MNA.Q.Qdataset ECB.Qdataset LFS.Qdataset Qdataset...
       GFS.Adataset MNA.A.Adataset AMECO.Adataset OUT.Adataset GFS.SSC.Adataset Adataset...
       MainDb list Data


