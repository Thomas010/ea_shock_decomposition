% seriesFromFame  Retrieve databank of time series from FAME via HTTPS request
%{
% Syntax
%--------------------------------------------------------------------------
%
%
%     [outputDb, info] = seriesFromFame(databasePath, fameNames, range, credentials, ...)
%
%
% Input Arguments
%--------------------------------------------------------------------------
%
%
% __`databasePath`__ [ string ]
%
%>    Path to the databank.
%
%
% __`fameNames`__ [ string ]
%
%>    An array of strings specifying the names of time series to retrieve,
%>    the names may include Fame wildcards.
%
%
% __`range`__ [ DateWrapper ]
%
%>    Date range on which the time series will be retrieved; all the time
%>    series specified in the `fameNames` must have the same date frequency as
%>    the `range` (hence, only single-frequency request are allowed).
%
%
% __`credentials`__ [ string ]
%
%
%>    Array of two strings: ["user-login", "user-password"]
%
%
% Output Arguments
%--------------------------------------------------------------------------
%
%
% __`outputDb`__ [ struct | Dictionary ]
%
%>    Databank (struct or Dictionary) with the time series requested.
%
%
% __`json`__ [ cell ]
%
%>    Cell array with original JSON objects returned in response to the
%>    HTTPS request.
%
%
% __`request`__ [ string ]
%
%
%>    Array of strings representing the HTTPS request sent to the server.
%
%
% Options
%--------------------------------------------------------------------------
%
%
% __`AddToDatabank=[ ]`__ [ empty | struct | Dictionary ]
%
%>    Add the requested series to an existing databank; `[ ]` means a new
%>    databank is created.
%
%
% __`Dataflow="OTHER"`__ [ string ]
%
%>    The dataflow specification based on which the Fame HTTPS request will
%>    be created.
%
%
% __`ExcludeStatus=["O", "M", "L", "H", "Q"]`__ [ string ]
%
%>    SDMX status of observations (`OBS_STATUS`) that will be excluded
%>    from the `outputDb`; the default value lists each status that has the
%>    meaning of a missing observation.
%
%
% __`IncludeStatus=["", "A", "F", "P"]`__ [ string | @all ]
%
%>    SDMX status of observations (`OBS_STATUS`) that will be included
%>    in the `outputDb` (after applying the `ExcludeStatus=` option first);
%>    an empty string included in the `IncludeStatus=` means that also
%>    the data points returned witout the `OBS_STATUS` field will be
%>    included; `@all` means any status except those listed in
%>    `ExcludeStatus=` will be included.
%
%
% __`MaxRecords=intmax( )`__ [ numeric ]
%
%>    The maximum number of data points to retrieve; this number will be
%>    included the Fame HTTPS request; must be non-negative.
%
%
% __`OutputNames=@auto`__ [ `@auto` | string | function_handle ]
%
%>    Names under which the requested series will be stored in the
%>    `outputDb`: 
%>
%>    * `@auto` means the series will be stored under their Fame
%>    keys; 
%>
%>    * a string array of the same size as the `fameNames` list means that
%>    the k-th series from the `fameNames` will be assigned the k-th name
%>    from the `OutputNames=` options; this method does not work if
%>    `fameNames` include wildcars;
%>
%>    * a function handle means this function will be applied to each
%>    key to obtain the corresponding output name (note that this method is
%>    necessary whenever the input `fameNames` list includes wildcards).
%
%
% __`OutputType='struct'`__ [ `'struct'` | `'Dictionary'` ]
%
%>    Type of the output databank: `'struct'` means the standard Matlab
%>    structure; `'Dictionary` means an IrisT implementation of `'struct'`
%>    like behavior with names including dots, `'.'`, allowed.
%
%}

% -Written by [IrisToolbox] Solutions Team @ OGResearch Ltd for the European Central Bank
% -Copyright (c) 2007-2020 [IrisToolbox] Solutions Team

% Version 1.1.0 Added option Test=
% Version 1.0.0

function [outputDb, info] = seriesFromFame( ...
    databasePath, fameNames, dateRange, credentials, varargin ...
)

import matlab.net.*
import matlab.net.http.*

%( Input parser
persistent pp
if isempty(pp)
    pp = extend.InputParser('seriesFromFame');

    addRequired(pp, 'databasePath', @(x) isstring(x) || ischar(x));
    addRequired(pp, 'fameNames', @(x) (isstring(x) || ischar(x) || iscellstr(x)) && (numel(string(x))==numel(unique(string(x)))));
    addRequired(pp, 'dateRange', @DateWrapper.validateProperRangeInput);
    addRequired(pp, 'credentials', @(x) isstring(x) && numel(x)==2);

    addParameter(pp, 'AddToDatabank', [ ], @(x) isempty(x) || validate.databank(x));
    addParameter(pp, 'Dataflow', "OTHER", @(x) isstring(x) || ischar(x));
    addParameter(pp, 'ExcludeStatus', ["O", "M", "L", "H", "Q"], @(x) isempty(x) || isstring(x));
    addParameter(pp, 'IncludeStatus', ["", "A", "F", "P"], @(x) isequal(x, @all) || isstring(x));
    addParameter(pp, 'MaxRecords', intmax( ), @(x) validate.roundScalar(x, 0, Inf)); 
    addParameter(pp, 'MaxLengthRequest', 2^11, @(x) validate.roundScalar(x, 0, Inf));
    addParameter(pp, 'OutputNames', @auto, @(x) isequal(x, @auto) || isstring(x) || ischar(x) || iscellstr(x) || isa(x, 'function_handle'));
    addParameter(pp, 'OutputType', 'struct', @(x) validate.anyString(x, 'struct', 'Dictionary'));
    addParameter(pp, 'StoreStatus', false, @validate.logicalScalar);
    addParameter(pp, 'ResolutionByStatus', ["", "A", "F", "P", "M", "L"]);
    addParameter(pp, 'Test', [ ], @(x) isempty(x) || (isstruct(x) && isfield(x, "OBS_VALUE") && isfield(x, "OBS_DATE") && isfield(x, "SERIES_KEY")));
end
parse(pp, databasePath, fameNames, dateRange, credentials, varargin{:});
opt = pp.Options;
%)

databasePath = string(databasePath);
fameNames = reshape(string(fameNames), 1, [ ]);
dateRange = double(dateRange);
if ~isa(opt.OutputNames, 'function_handle')
    if any(contains(fameNames, ["*", "?"]))
        hereReportOutputNamesForWildcards( );
    end
    opt.OutputNames = reshape(string(opt.OutputNames), 1, [ ]);
    if numel(fameNames)~=numel(opt.OutputNames)
        hereReportInvalidOutputNamesDimensions( );
    end
end

isLive = isempty(opt.Test);

%--------------------------------------------------------------------------

outputDb = databank.backend.ensureTypeConsistency( ...
    opt.AddToDatabank, opt.OutputType ...
);

rangeString = DateWrapper.toIsoString(dateRange([1, end]));
freq = DateWrapper.getFrequencyAsNumeric(dateRange(1));
if isLive
    fixedRequest = locallyEncodeFixedRequest( ...
        databasePath, rangeString, credentials, opt ...
    );
    httpOptions = matlab.net.http.HTTPOptions( ...
        'ConnectTimeout', 9999, ...
        'CertificateFilename', '' ...
    );
end

if nargout>=2
    info = hereInitializeInfoStruct( );
end

%
% Split the input list of `fameNames` and iterate as follows:
% Take as many items from the `fameNames` list as can fit within the HTTPS
% request length limits, return the remainder and use that in the next
% iteration. To this end, create a `runningList` that shrinks over time. At
% the same time keep the original input `fameNames` because they are needed
% in mapping the input names into output names.
%
if isLive
    runningList = fameNames;
else
    runningList = "Test";
end

while ~isempty(runningList)
    if isLive
        [request, runningList] = locallyEncodeURI(fixedRequest, runningList, opt);
        response = send(RequestMessage( ), request, httpOptions);
        if isstruct(response.Body.Data)
            jsonData = response.Body.Data.quote;
        else
            jsonDataquote = jsondecode(char(response.Body.Data));
            jsonData = jsonDataquote.quote;
        end 
    else
        request = "Test";
        response = struct( );
        response.Body.Data.quote = opt.Test;
        jsonData = opt.Test;
        runningList = string.empty(1, 0);
    end

    hereValidateJsonData( );

    [jsonData, jsonDataExcluded, status] = locallyPreprocessData(jsonData, opt);

    [outputDb, jsonDataExcluded] = locallyConvertToSeries(outputDb, jsonData, status, freq, fameNames, jsonDataExcluded, request, opt);

    if nargout>=2
        info.Request(end+1) = request;
        info.Response{end+1} = response;
        info.JSONData{end+1} = jsonData;
        info.JSONDataExcluded{end+1} = jsonDataExcluded;
    end
end

return

    function info = hereInitializeInfoStruct( )
        %(
        info = struct( );
        info.Request = string.empty(1, 0);
        info.Response = cell.empty(1, 0);
        info.JSONData = cell.empty(1, 0);
        info.JSONDataExcluded = cell.empty(1, 0);
        %)
    end%


    function hereValidateJsonData( )
        %(
        if isempty(jsonData)
            hereReportEmptyData( );
        end

        if isstruct(jsonData)
            if ~isfield(jsonData, 'OBS_VALUE')
                hereReportMissingObservations( );
            end
        else
            if ~all(cellfun(@(x) isfield(x, 'OBS_VALUE'), jsonData))
                hereReportMissingObservations( );
            end
        end

        return

            function hereReportEmptyData( )
                thisError = [
                    "GetSeriesFromFame:InvalidDataReturned"
                    "Request to this Fame databank returned empty data: %s"
                ];
                throw(exception.Base(thisError, 'error'), databasePath);
            end%


            function hereReportMissingObservations( )
                thisError = [
                    "GetSeriesFromFame:InvalidDataReturned"
                    "Request to this Fame databank did not return any OBS_VALUE fields: %s"
                ];
                throw(exception.Base(thisError, 'error'), databasePath);
            end%
        %)
    end%


    function hereReportOutputNamesForWildcards( )
        %(
        thisError = [
            "GetSeriesFromFame:InvalidOutputNames"
            "The input list of Fame keys includes wildcards; in that case, "
            "the option OutputNames= cannot be assigned a list of strings "
            "but only a function handle. "
        ];
        throw(exception.Base(thisError, 'error'));
        %)
    end%

        
    function hereReportInvalidOutputNamesDimensions( )
        %(
        thisError = [
            "GetSeriesFromFame:InvalidOutputNames"
            "The dimensions of the list assigned to the option OutputNames= "
            "must match the dimensions of the list of Fame keys. "
        ];
        throw(exception.Base(thisError, 'error'));
        %)
    end%
end% 


%
% Local Functions
%


function fixedRequest = locallyEncodeFixedRequest(databasePath, range, credentials, opt)
    %(
    fixedRequest = ...
        "https://stat-prod.ecb.de:7443/FWS/fwsreadseriesparallel" ...
        + "?dataflow=" + string(opt.Dataflow) ...
        + "&database=" + databasePath ...
        + "&startDate=" + range(1) ...
        + "&endDate=" + range(end) ...
        + "&obsStatusCheck=False&obsConfCheck=False&obsComCheck=False" ...
        + "&maxRecords=" + string(opt.MaxRecords) ...
        + "&username=" + string(urlencode(credentials(1))) ...
        + "&password=" + string(urlencode(credentials(2))) ...
        + "&fameWildcard=" ;
    %)
end%


function [request, runningList] = locallyEncodeURI(fixedRequest, runningList, opt)
    %(
    delimiter = string(urlencode(";"));
    checkSum = strlength(fixedRequest) + cumsum(strlength(runningList) + strlength(delimiter));
    pos = find(checkSum<opt.MaxLengthRequest, 1, 'Last');
    if isempty(pos)
        pos = 1;
    end
    includeFameNames = runningList(1:pos);
    request = fixedRequest + join(includeFameNames, delimiter);
    runningList(1:pos) = [ ];
    %)
end%


function [db, jsonExcluded] = locallyConvertToSeries(db, json, allStatus, freq, inputFameNames, jsonExcluded, request, opt)
    %(
    if isstruct(json)
        allSeriesKeys = string({json.SERIES_KEY});
        allValues = [json.OBS_VALUE];
        allDates = string({json.OBS_DATE});
    else
        allSeriesKeys = cellfun(@(x) string(x.SERIES_KEY), json);
        allValues = cellfun(@(x) x.OBS_VALUE, json);
        allDates = cellfun(@(x) string(x.OBS_DATE), json);
    end
    
    if isempty(allDates)
        hereReportAllEmpty( );
        return
    end

    allDates = DateWrapper.fromIsoStringAsNumeric(freq, allDates);

    warnEmpty = string.empty(1, 0);
    for key = reshape(unique(allSeriesKeys), 1, [ ])
        outputName = locallyMapKeyToOutputName(key, inputFameNames, opt);
        inx = allSeriesKeys==key;
        values = reshape(allValues(inx), [ ], 1);
        dates = reshape(allDates(inx), [ ], 1);
        status = reshape(allStatus(inx), [ ], 1);
        if isempty(values)
            warnEmpty(end+1) = string(key); %#ok<AGROW>
        elseif numel(unique(floor(dates)))~=nnz(inx)
            hereReportMultipleValues( );
            jsonExcluded = [jsonExcluded; json(inx)]; %#ok<AGROW>
            values = double.empty(0, 1);
            dates = double.empty(0, 1);
            status = string.empty(0, 1);
        end
        x__ = Series(dates, values, "--SkipInputParser");
        if opt.StoreStatus
            s__ = Series(dates, status, "--SkipInputParser");
            x__ = assignUserData(x__, "Status", s__);
        end
        if isstruct(db)
            db.(outputName) = x__;
        else 
            store(db, outputName, x__);
        end
    end
    
    if ~isempty(warnEmpty)
        hereReportEmptySeries( );
    end
    
    return

        function hereReportAllEmpty( )
            message = [
                "GetSeriesFromFame:AllEmptySeries"
                "This request did not return any "
                "observations with whitelisted status: %s "
            ];
            throw(exception.Base(message, 'warning'), request);            
        end%

    
        function hereReportMultipleValues( )
            message = [
                "GetSeriesFromFame:RepeatedDates"
                "This time series data returned from Fame contain mutliple "
                "observations with whitelisted status for the same date: %s "
            ];
            throw(exception.Base(message, 'warning'), key);
        end%
    
    
        function hereReportEmptySeries( )
            message = [
                "GetSeriesFromFame:EmptySeries"
                "Request for this time series did not return any "
                "observations with whitelisted status: %s "
            ];
            throw(exception.Base(message, 'warning'), warnEmpty);
        end%
    %)
end%


function outputName = locallyMapKeyToOutputName(key, inputFameNames, opt)
    %(
    if isequal(opt.OutputNames, @auto)
        outputName = key;
    elseif isa(opt.OutputNames, 'function_handle')
        %
        % Run a user-supplied function, typically a replace(...),
        % on the Fame name to get the output name.
        % 
        outputName = opt.OutputNames(key);
    else
        inx = key==inputFameNames;
        if ~any(inx)
            %
            % This happens when the input list of Fame keys includes wild
            % cards; in that case the OutputName= must specified as a
            % function handle with a replace function in it.
            %
            hereReportKeyNotFound( );
        end
        outputName = opt.OutputNames(inx);
    end
    
    return

        function hereReportKeyNotFound( )
            thisError = [
                "GetSeriesFromFame:KeyNotFound"
                "This Fame key was not found in the input list of keys "
                "when trying to create the output name for it: %s "
            ];
            throw(exception.Base(thisError, 'error'), key); %#ok<*GTARG>
        end%
    %)
end%


function [jsonData, jsonDataExcluded, status] = locallyPreprocessData(jsonData, opt)
% locallyPreprocessData  Collect status of all records, collect flags of
% numeric values, exclude records that have missing value status, exclude
% records that do not match status requested by the user, and exclude
% records whose value is not numeric
    %(
    status = repmat("_", size(jsonData));
    inxNumericValue = false(size(jsonData));
    for i = 1 : numel(jsonData)
        if isstruct(jsonData)
            jsonData__ = jsonData(i);
        else
            jsonData__ = jsonData{i};
        end
        if isfield(jsonData__, 'OBS_STATUS')
            status(i) = jsonData__.OBS_STATUS;
        end
        inxNumericValue(i) = isfield(jsonData__, 'OBS_VALUE') && isnumeric(jsonData__.OBS_VALUE);
    end
    status = reshape(status, 1, [ ]);
    status(status=="") = "_";
    inxNumericValue = reshape(inxNumericValue, 1, [ ]);

    inxExclude = false(1, numel(jsonData));
    if ~isempty(opt.ExcludeStatus)
        opt.ExcludeStatus = reshape(opt.ExcludeStatus, 1, [ ]);
        inxExclude = inxExclude | contains(status, opt.ExcludeStatus);
    end

    if ~isequal(opt.IncludeStatus, @all)
        includeStatus = reshape(opt.IncludeStatus, 1, [ ]);
        includeStatus(includeStatus=="") = "_";
        inxExclude = inxExclude | ~contains(status, includeStatus);
    end
    inxExclude = inxExclude | ~inxNumericValue;

    jsonDataExcluded = jsonData(inxExclude);
    jsonData(inxExclude) = [ ];
    status(inxExclude) = [ ];
    status(status=="_") = "";
    %)
end%

