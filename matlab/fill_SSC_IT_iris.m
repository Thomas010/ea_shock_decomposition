temp = MainDb.Q.IT.SCT.CV;
lastQuarter = dat2char(temp.end);
empty_Qrt = 4 - str2num(lastQuarter(end));
empty_Qrt_range = str2dat(lastQuarter)+1:str2dat(lastQuarter)+empty_Qrt;
for date = empty_Qrt_range
    % extending the series with the average growth rate over last 5 quarters
    temp = [temp; tseries(date, temp(date-1)*(temp(date-1)/temp(date-6))^(1/5))];
end
       
%% Use SCT Growth Rate of last available year for Next Year's Estimate (IT)
% calculation of the reference growth rate
SCTgrowthRate = sum(temp(temp.end-3:temp.end))/sum(temp(temp.end-7:temp.end-4));
% extension for SCH and SCF
for item =["SCH", "SCF"]
    MainDb.A.IT.(item).CV = [MainDb.A.IT.(item).CV; tseries(MainDb.A.IT.(item).CV.end+1,...
                            SCTgrowthRate*MainDb.A.IT.(item).CV(end))];
end