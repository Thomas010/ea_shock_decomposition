global series_date_range_q
Qdataset_temp_0 = Qdataset{'[.*IT_sct_cv]'};
lastDate = Qdataset_temp_0.lastobservedperiod;
lastDate_Qdataset = Qdataset.lastdate;
empty_Qrt = 4 - lastDate.time(1,2); % remaining X quarters to have data for an entire year <-> need inidicator to fully match the annual series
Qdataset_temp_0 = [Qdataset_temp_0; dseries(NaN(length(Qdataset.lastdate+1:series_date_range_q(end)),vobs(Qdataset_temp_0)), Qdataset.lastdate+1:series_date_range_q(end), Qdataset_temp_0.name)];
for i = 1:empty_Qrt
    % extending the series with the average growth rate over last 5 quarters
    Qdataset_temp_1 = lag(Qdataset_temp_0(lastDate)*(Qdataset_temp_0/lag(Qdataset_temp_0,5))^(1/5),1);
    Qdataset_temp_0 = [Qdataset_temp_0(firstobservedperiod(Qdataset_temp_0):lastobservedperiod(Qdataset_temp_0));...
                       Qdataset_temp_1(firstobservedperiod(Qdataset_temp_1):lastdate(Qdataset_temp_1))];
                      % the last date reference should be lastdate because
                      % we would like to keep NaN to be overwritten by our
                      % calculation, otherwise there are no empty
                      % observations to be overwritten and error appears
    lastDate = Qdataset_temp_0.lastobservedperiod;
end
       
%% Use SCT Growth Rate of last available year for Next Year's Estimate (IT and CY)
% calculation of the reference growth rates
Qdataset_temp = Qdataset{'[.*IT_sct_cv]'};
Qdataset_temp_cum = Qdataset_temp_0+lag(Qdataset_temp_0,1)+lag(Qdataset_temp_0,2)+lag(Qdataset_temp_0,3);
Qdataset_temp_ann = dseries(Qdataset_temp_cum.data(4:4:end,:),dates('1999Y'):(dates('1999Y')+length(Qdataset_temp_cum.data(4:4:end,:))-1),Qdataset_temp_cum.name);
Qdataset_temp_ann = Qdataset_temp_ann.rename(strcat(Qdataset_temp.name,{'_growth'}));

% Warning : if countries does not contain CY, then CY data are
% based on IT data
% extension for sch
Adataset_temp_0 = Adataset{'[.*IT_sch_cv]'};
Adataset_temp_0 = align(Adataset_temp_0,dseries(NaN,series_date_range_a));
Adataset_temp_1 = lag(Adataset_temp_0,1)*Qdataset_temp_ann/lag(Qdataset_temp_ann,1);
Adataset_temp_0 = [Adataset_temp_0(firstobservedperiod(Adataset_temp_0):lastobservedperiod(Adataset_temp_0));...
                   Adataset_temp_1(lastobservedperiod(Adataset_temp_1))];

Adataset = merge(Adataset, Adataset_temp_0);

% extension for scf
Adataset_temp_0 = Adataset{'[.*IT_scf_cv]'};
Adataset_temp_0 = align(Adataset_temp_0,dseries(NaN,series_date_range_a));
Adataset_temp_1 = lag(Adataset_temp_0,1)*Qdataset_temp_ann/lag(Qdataset_temp_ann,1);
Adataset_temp_0 = [Adataset_temp_0(firstobservedperiod(Adataset_temp_0):lastobservedperiod(Adataset_temp_0));...
                   Adataset_temp_1(lastobservedperiod(Adataset_temp_1))];

Adataset = merge(Adataset, Adataset_temp_0);
Qdataset{'[.*IT_sct_cv]'} = Qdataset_temp_0;