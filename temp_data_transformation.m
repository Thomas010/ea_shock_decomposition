%% -------------------- DATA TRANSFORMATION MAIN FILE --------------------%%
%--------------------
% Clearing workspace.
%--------------------    
clear all; close all; clc;
%------------------
% Required package/path
%------------------
% Add some paths
addpath([pwd '\retrieve']);
addpath([pwd '\matlab'])
addpath([pwd '\export_fig_routines']);

% calling a specific user input not tracked by GIT
call_credentials;
call_paths;

% Call Iris
addpath(iris_path);
iris.startup

% Call Dynare package
addpath(dynare_path);
dynare_config
%% Defining databases
db_MNA   = '/global/disdatadg/esdb/unrestricted/internal/esa2010.db';
db_QGFS  = '/global/proddatadg/sdpe/production/EFS/common/GFS/GFSQ_SA_ECB.db';
db_AMECO = '/global/disdatadg/esdb/unrestricted/other/ameco.db';
db_ECB   = '/global/disdatadg/esdb/unrestricted/ecb/ecb.db';
db_GFS   = '/global/disdatadg/esdb/unrestricted/ecb/gfs.db';
db_OUT   = '/dissuser/ec_fpo/common/databases/output_database.db';
db_LFS   = '/global/disdatadg/esdb/unrestricted/internal/lfs.db';

%% defining lists
aCtryList_Core    = {'AT','BE','DE','FI','FR','NL'};
aCtryList_Core_EC = {'AUT','BEL','DEU','FIN','FRA','NLD'};
aCtryList_Peri    = {'ES','GR','IE','IT','PT'};
aCtryList_Peri_EC = {'ESP','GRC','IRL','ITA','PRT'};
aCtryList_ALL     = [aCtryList_Core aCtryList_Peri];
aCtryList_ALL_EC  = [aCtryList_Core_EC aCtryList_Peri_EC];
aCtryList_Agg     = {'CR', 'PR', 'EA'};
aCtryList_Grand   = [aCtryList_ALL aCtryList_Agg 'DR'];

aItemList{1 } = "POP_lr";      		aItemListSource{1 } = "W0.S1.S1._Z.POP._Z._Z._Z.PS._Z.N";   			aKeyListSource{1 } = "ENA.A.N";                     
aItemList{2 } = "potGDP_lr";   		aItemListSource{2 } = "1.0.0.0.OVGDP";                                  aKeyListSource{2 } = "AME.A"  ;
aItemList{3 } = "GDP_cv";      		aItemListSource{3 } = "W2.S1.S1.B.B1GQ._Z._Z._Z.EUR.V.N";               aKeyListSource{3 } = "MNA.Q.Y";
aItemList{4 } = "GDP_lr";      		aItemListSource{4 } = "W2.S1.S1.B.B1GQ._Z._Z._Z.EUR.LR.N";              aKeyListSource{4 } = "MNA.Q.Y";  
aItemList{5 } = "gcons_cv";    		aItemListSource{5 } = "W0.S13.S1.D.P3._Z._Z._T.XDC.V.N";                aKeyListSource{5 } = "MNA.Q.Y";
aItemList{6 } = "gcons_lr";    		aItemListSource{6 } = "W0.S13.S1.D.P3._Z._Z._T.XDC.L";                  aKeyListSource{6 } = "MNA.Q.Y";
aItemList{7 } = "ginc_cv";     		aItemListSource{7 } = "W0.S13.S1.N.D.P2._Z._Z._T.XDC._Z.S.V.N._T";      aKeyListSource{7 } = "GFS.Q.Y";
aItemList{8 } = "gstm_cv";     		aItemListSource{8 } = "W0.S13.S1.N.D.D632._Z._Z._T.XDC._Z.S.V.N._T";    aKeyListSource{8 } = "GFS.Q.Y";
aItemList{9 } = "ginv_cv";     		aItemListSource{9 } = "W0.S13.S1.N.D.P51G._Z._Z._T.XDC._Z.S.V.N._T";    aKeyListSource{9 } = "GFS.Q.Y"; 
aItemList{10} = "tinv_cv";     		aItemListSource{10} = "W0.S1.S1.D.P51G.N11G._T._Z.EUR.V.N";             aKeyListSource{10} = "MNA.Q.Y";
aItemList{11} = "tinv_lr";     		aItemListSource{11} = "W0.S1.S1.D.P5.N1G._T._Z.XDC.LR.N";               aKeyListSource{11} = "MNA.Q.Y";  
aItemList{12} = "hhcons_cv";   		aItemListSource{12} = "W0.S1M.S1.D.P31._Z._Z._T.EUR.V.N";               aKeyListSource{12} = "MNA.Q.Y"; 
aItemList{13} = "hhcons_lr";   		aItemListSource{13} = "W0.S1M.S1.D.P31._Z._Z._T.XDC.LR.N";              aKeyListSource{13} = "MNA.Q.Y";
aItemList{14} = "gtrnsf_cv";   		aItemListSource{14} = "W0.S13.S1.N.D.D62._Z._Z._T.XDC._Z.S.V.N._T";     aKeyListSource{14} = "GFS.Q.Y";
aItemList{15} = "templ_lr";    		aItemListSource{15} = "W2.S1.S1._Z.SAL._Z._T._Z.PS._Z.N";               aKeyListSource{15} = "ENA.Q.Y"; %"ENA.Q.S."
aItemList{16} = "gempl_lr";    		aItemListSource{16} = "LNG";                                           aKeyListSource{16} = "ZZZ0_G19_A_N_";           aIndic{16 } = aItemList{15};
aItemList{17} = "dtx_cv";      		aItemListSource{17} = "W0.S13.S1.N.C.D5._Z._Z._Z.XDC._Z.S.V.N._T";      aKeyListSource{17} = "GFS.Q.Y";                 
aItemList{18} = "itx_cv";      		aItemListSource{18} = "W0.S13.S1.N.C.D2._Z._Z._Z.XDC._Z.S.V.N._T";      aKeyListSource{18} = "GFS.Q.Y"; 
aItemList{19} = "taxprod_cv";  		aItemListSource{19} = "_Z.SZV._Z.N.C.D21._Z._Z._Z.XDC._Z.S.V.N._T";     aKeyListSource{19} = "E09.A.N";                 aIndic{19 } = aItemList{12};
aItemList{20} = "tcomp_cv";    		aItemListSource{20} = "W2.S1.S1.D.D11._Z._T._Z.EUR.V.N";                aKeyListSource{20} = "MNA.Q.Y.";% "MNA.Q.S"
aItemList{21} = "taxhhinc_cv"; 		aItemListSource{21} = "_Z.SZV._Z.N.C.D51M._Z._Z._Z.XDC._Z.S.V.N._T";    aKeyListSource{21} = "E09.A.N";                 aIndic{21 } = aItemList{20};
aItemList{22} = "sschhinc_cv"; 		aItemListSource{22} = "_Z.SZV._Z.N.C.D613._Z._Z._Z.XDC._Z.S.V.N._T";    aKeyListSource{22} = "E09.A.N";                 aIndic{22 } = aItemList{20};
aItemList{23} = "sscfirma_cv"; 		aItemListSource{23} = "_Z.SZV._Z.N.C.D611._Z._Z._Z.XDC._Z.S.V.N._T";    aKeyListSource{23} = "E09.A.N";
aItemList{24} = "sscfirmi_cv"; 		aItemListSource{24} = "_Z.SZV._Z.N.C.D612._Z._Z._Z.XDC._Z.S.V.N._T";    aKeyListSource{24} = "E09.A.N"; 
aItemList{25} = "sct_cv";      		aItemListSource{25} = "W0.S13.S1.N.C.D61._Z._Z._Z.XDC._Z.S.V.N._T";     aKeyListSource{25} = "GFS.Q.Y";
aItemList{26} = "scf_cv";      		aItemListSource{26} = "W0.S13.S1.N.C.D611._Z._Z._Z.XDC._Z.S.V.N._T";    aKeyListSource{26} = "GFS.Q.Y"; 
aItemList{27} = "sch_cv";      		aItemListSource{27} = "W0.S13.S1.N.C.D613._Z._Z._Z.XDC._Z.S.V.N._T";    aKeyListSource{27} = "GFS.Q.Y";   
aItemList{28} = "gcomp_cv";    		aItemListSource{28} = "W0.S13.S1.N.D.D1._Z._Z._T.XDC._Z.S.V.N._T";      aKeyListSource{28} = "GFS.Q.Y";  
aItemList{29} = "impssc_cv";   		aItemListSource{29} = "W0.S13.S1.N.C.D612._Z._Z._Z.XDC._Z.S.V.N._T";    aKeyListSource{29} = "GFS.A.N";                 aIndic{29 } = aItemList{28};
aItemList{30} = "gbbraw_cv";   		aItemListSource{30} = "W0.S13.S1._Z.B.B9._Z._Z._Z.XDC._Z.S.V.N._T";     aKeyListSource{30} = "GFS.Q.Y";
aItemList{31} = "nafa_cv";     		aItemListSource{31} = "W0.S13.S1.N.D.NP._Z._Z._T.XDC._T.S.V.N._T";      aKeyListSource{31} = "QSA.Q.N";
aItemList{32} = "ctrnsf_cv";   		aItemListSource{32} = "W0.S12.S1.N.C.D9N._Z._Z._Z.XDC._T.S.V.N._T";     aKeyListSource{32} = "QSA.Q.N";
aItemList{33} = "inp_cv";      		aItemListSource{33} = "W0.S13.S1.C.D.D41._Z._Z._T.XDC._Z.S.V.N._T";     aKeyListSource{33} = "GFS.Q.Y";
aItemList{34} = "unempl_lr";   		aItemListSource{34} = "S.UNEMPL.TOTAL0.15_74.T";                        aKeyListSource{34} = "LFSI.Q";
aItemList{35} = "activepop_lr";		aItemListSource{35} = "S.ACTIVE.TOTAL0.15_74.T";                        aKeyListSource{35} = "LFSI.Q";
aItemList{36} = "gdebt_cv";    		aItemListSource{36} = "W0.S13.S1.C.L.LE.GD.T._Z.XDC._T.F.V.N._T";       aKeyListSource{36} = "GFS.Q.Y";
aItemList{37} = "sscfirm_cv";    	aItemListSource{37} = "";                                               aKeyListSource{37} = "";                        aIndic{37 } = aItemList{20};

aItemList_MNA_Q  = aItemList([3:6 10:13 15 20]);              aItemListSource_MNA_Q = aItemListSource([3:6 10:13 15 20]);            
aItemList_MNA_A  = aItemList(1);                              aItemListSource_MNA_A = aItemListSource(1);
aItemList_QGFS  = aItemList([7:9 14 17:18 25:28 30 33 36]);  aItemListSource_QGFS = aItemListSource([7:9 14 17:18 25:28 30 33 36]);
aItemList_GFS   = aItemList([19 21:24 29]);                  aItemListSource_GFS = aItemListSource([19 21:24 29]);
aItemList_AMECO = aItemList(2);                              aItemListSource_AMECO = aItemListSource(2);
aItemList_OUT   = aItemList(16);                             aItemListSource_OUT = aItemListSource(16);
aItemList_ECB   = aItemList(31:32);                          aItemListSource_ECB = aItemListSource(31:32);
aItemList_LFS   = aItemList(34:35);                          aItemListSource_LFS = aItemListSource(34:35);

global series_date_range_q
range_Q = qq(1999):qq(2020,4);
range_A = yy(1999):yy(2020);
series_date_range_a = dates(dat2char(range_A(1))):dates(dat2char(range_A(end)));
series_date_range_q = dates(dat2char(range_Q(1))):dates(dat2char(range_Q(end)));
%% ----------------------------------------
%  retrieve data from MNA database
%  ---------------------------------------- 

fameNames_MNA_Q = textual.xlist( ...
    ".", ...
    "MNA.Q.Y", ... ...
    aCtryList_ALL, ...
    aItemListSource_MNA_Q ...
    );
% Some adjustments are needed due to a lack of homogeneity inside a single
% database
fameNames_MNA_Q(contains(fameNames_MNA_Q, "SAL")) = strrep(fameNames_MNA_Q(contains(fameNames_MNA_Q, "SAL")), "MNA.Q", "ENA.Q");
fameNames_MNA_Q(contains(fameNames_MNA_Q, "FR.W2.S1.S1._Z.SAL")) = strrep(fameNames_MNA_Q(contains(fameNames_MNA_Q, "FR.W2.S1.S1._Z.SAL")), ".Q.Y.", ".Q.S.");
fameNames_MNA_Q(contains(fameNames_MNA_Q, "GR.W2.S1.S1._Z.SAL")) = strrep(fameNames_MNA_Q(contains(fameNames_MNA_Q, "GR.W2.S1.S1._Z.SAL")), ".Q.Y.", ".Q.S.");
fameNames_MNA_Q(contains(fameNames_MNA_Q, "PT.W2.S1.S1._Z.SAL")) = strrep(fameNames_MNA_Q(contains(fameNames_MNA_Q, "PT.W2.S1.S1._Z.SAL")), ".Q.Y.", ".Q.S.");
fameNames_MNA_Q(contains(fameNames_MNA_Q, "FR.W2.S1.S1.D.D11")) = strrep(fameNames_MNA_Q(contains(fameNames_MNA_Q, "FR.W2.S1.S1.D.D11")), ".Q.Y.", ".Q.S.");
fameNames_MNA_Q(contains(fameNames_MNA_Q, "DE.W2.S1.S1.D.D11")) = strrep(fameNames_MNA_Q(contains(fameNames_MNA_Q, "DE.W2.S1.S1.D.D11")), ".Q.Y.", ".Q.S.");

OutputNames_MNA_Q = textual.xlist( ...
    "_", ...
    aCtryList_ALL, ...
    aItemList_MNA_Q...
    );

fameNames_MNA_A = textual.xlist( ...
    ".", ...
    "ENA.A.N", ...
    aCtryList_ALL, ...
    aItemListSource_MNA_A ...
    );

OutputNames_MNA_A = textual.xlist( ...
   "_", ...
    aCtryList_ALL, ...
    aItemList_MNA_A...
    );

fprintf('\nRetrieving MNA data...\n');   
Qdataset_MNA = seriesFromFame(db_MNA, fameNames_MNA_Q, range_Q, credentials, "OutputName=", OutputNames_MNA_Q, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
Qdataset_MNA = utils.IrisTs2dseries(Qdataset_MNA, Frequency.QUARTERLY);

Adataset_MNA = seriesFromFame(db_MNA, fameNames_MNA_A, range_A, credentials, "OutputName=", OutputNames_MNA_A, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
Adataset_MNA = utils.IrisTs2dseries(Adataset_MNA, Frequency.YEARLY);
fprintf('\nDone!\n'); 

notRetrieved_MNA_Q = setdiff(OutputNames_MNA_Q,Qdataset_MNA.name);
notRetrieved_MNA_A = setdiff(OutputNames_MNA_A,Adataset_MNA.name);

%Qdataset_MNA = [Qdataset_MNA dseries(NaN(length(Qdataset_MNA.dates),length(notRetrieved_MNA_Q)), Qdataset_MNA.dates, notRetrieved_MNA_Q)];

%% ----------------------------------------
%  retrieve data from QGFS database
%  ---------------------------------------- 

fameNames_QGFS = textual.xlist( ...
    ".", ...
    "GFS.Q.Y", ...
    aCtryList_ALL, ...
    aItemListSource_QGFS ...
    );

OutputNames_QGFS = textual.xlist( ...
   "_", ...
    aCtryList_ALL, ...
    aItemList_QGFS...
    );

fprintf('\nRetrieving QGFS data...\n');   
Qdataset_QGFS = seriesFromFame(db_QGFS, fameNames_QGFS, range_Q, credentials, "OutputName=", OutputNames_QGFS, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
Qdataset_QGFS = utils.IrisTs2dseries(Qdataset_QGFS, Frequency.QUARTERLY);
fprintf('\nDone!\n');

notRetrieved_QGFS = setdiff(OutputNames_QGFS,Qdataset_QGFS.name);
Qdataset_QGFS = [Qdataset_QGFS dseries(NaN(length(Qdataset_QGFS.dates),length(notRetrieved_QGFS)), Qdataset_QGFS.dates, notRetrieved_QGFS)];
%% ----------------------------------------
%  retrieve data from GFS database
%  ---------------------------------------- 

fameNames_GFS = textual.xlist( ...
    ".", ...
    "E09.A.N", ...
    aCtryList_ALL, ...
    aItemListSource_GFS ...
    );
fameNames_GFS(contains(fameNames_GFS, "W0.S13.S1.N.C.D612")) = strrep(fameNames_GFS(contains(fameNames_GFS, "W0.S13.S1.N.C.D612")), "E09.A", "GFS.A");

OutputNames_GFS = textual.xlist( ...
   "_", ...
    aCtryList_ALL, ...
    aItemList_GFS...
    );

fprintf('\nRetrieving GFS data...\n');   
Adataset_GFS = seriesFromFame(db_GFS, fameNames_GFS, range_A, credentials, "OutputName=", OutputNames_GFS, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
Adataset_GFS = utils.IrisTs2dseries(Adataset_GFS, Frequency.YEARLY);
fprintf('\nDone!\n');

notRetrieved_GFS = setdiff(OutputNames_GFS,Adataset_GFS.name);
%% ----------------------------------------
%  retrieve data from AMECO database
%  ---------------------------------------- 

fameNames_AMECO = textual.xlist( ...
    ".", ...
    "AME.A", ...
    aCtryList_ALL_EC, ...
    aItemListSource_AMECO ...
    );

OutputNames_AMECO = textual.xlist( ...
   "_", ...
    aCtryList_ALL, ...
    aItemList_AMECO...
    );

fprintf('\nRetrieving AMECO data...\n');   
Adataset_AMECO = seriesFromFame(db_AMECO, fameNames_AMECO, range_A, credentials, "OutputName=", OutputNames_AMECO, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
Adataset_AMECO = utils.IrisTs2dseries(Adataset_AMECO, Frequency.YEARLY);
fprintf('\nDone!\n');

notRetrieved_AMECO = setdiff(OutputNames_AMECO,Adataset_AMECO.name);

%% ----------------------------------------
%  retrieve data from ECB database
%  ---------------------------------------- 

fameNames_ECB = textual.xlist( ...
    ".", ...
    "QSA.Q.N", ...
    aCtryList_ALL, ...
    aItemListSource_ECB ...
    );

OutputNames_ECB = textual.xlist( ...
   "_", ...
    aCtryList_ALL, ...
    aItemList_ECB...
    );

fprintf('\nRetrieving ECB data...\n');   
Qdataset_ECB = seriesFromFame(db_ECB, fameNames_ECB, range_Q, credentials, "OutputName=", OutputNames_ECB, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
Qdataset_ECB = utils.IrisTs2dseries(Qdataset_ECB, Frequency.QUARTERLY);
fprintf('\nDone!\n');

notRetrieved_ECB = setdiff(OutputNames_ECB,Qdataset_ECB.name);

%% ----------------------------------------
%  retrieve data from LFS database
%  ---------------------------------------- 

fameNames_LFS = textual.xlist( ...
    ".", ...
    "LFSI.Q", ...
    aCtryList_ALL, ...
    aItemListSource_LFS ...
    );

OutputNames_LFS = textual.xlist( ...
   "_", ...
   aCtryList_ALL, ...
   aItemList_LFS ...
    );
fprintf('\nRetrieving LFS data...\n');   
Qdataset_LFS = seriesFromFame(db_LFS, fameNames_LFS, range_Q, credentials, "OutputName=", OutputNames_LFS, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
Qdataset_LFS = utils.IrisTs2dseries(Qdataset_LFS, Frequency.QUARTERLY);
fprintf('\nDone!\n');
notRetrieved_LFS = setdiff(OutputNames_LFS,Qdataset_LFS.name);

%% ----------------------------------------
%  retrieve data from Output database
%  ---------------------------------------- 

fameNames_OUT = textual.xlist( ...
    "_", ...
    "ZZZ0_S20_A_N", ...
    aCtryList_ALL, ...
    aItemListSource_OUT ...
    );

OutputNames_OUT = textual.xlist( ...
   "_", ...
   aCtryList_ALL, ...
   aItemList_OUT ...
    );
fprintf('\nRetrieving OUT data...\n');   
Adataset_OUT = seriesFromFame(db_OUT, fameNames_OUT, range_A, credentials, "OutputName=", OutputNames_OUT, "ExcludeStatus=", [ ], "IncludeStatus=", @all);
Adataset_OUT = utils.IrisTs2dseries(Adataset_OUT, Frequency.YEARLY);
fprintf('\nDone!\n');
notRetrieved_OUT = setdiff(OutputNames_OUT,Adataset_OUT.name);

%% --------------------------------------------------------------------
%  retrieve annual SSC data (could be include inside the GFS retrieval)
%  --------------------------------------------------------------------
fameNames = textual.xlist( ...
    ".", ...
    "GFS.A.N", ...
    ["DE","AT","IE","IT"], ...
    ["W0.S13.S1.N.C.D611._Z._Z._Z.XDC._Z.S.V.N._T",...
    "W0.S13.S1.N.C.D613._Z._Z._Z.XDC._Z.S.V.N._T"] ...
    );

outputNames = textual.xlist( ...
    "_", ...
    ["DE","AT","IE","IT"],...
    ["scf_cv","sch_cv"]...
);

fprintf('\nRetrieving annual SSC data...\n');
Adataset_SSC = seriesFromFame(db_GFS, fameNames, range_A, credentials, "OutputName=", outputNames,"ExcludeStatus=", [ ], "IncludeStatus=", @all);
fprintf('\nDone!\n'); 
Adataset_SSC = utils.IrisTs2dseries(Adataset_SSC, Frequency.YEARLY);

%% ------------------------
%  Merging datasets
%  ------------------------
Qdataset = merge(merge(merge(Qdataset_QGFS, Qdataset_MNA), Qdataset_ECB),Qdataset_LFS);
Adataset = merge(merge(merge(merge(Adataset_GFS, Adataset_MNA), Adataset_AMECO),Adataset_OUT), Adataset_SSC);
%% Cleaning
% clear Qdataset_QGFS Qdataset_MNA Qdataset_ECB Qdataset_LFS...
%         Adataset_GFS Adataset_MNA Adataset_AMECO Adataset_OUT Adataset_SSC
%% ------------------------
%  Computing employers SSC
%  ------------------------
for i=1:length(aCtryList_ALL)
    ssfName = convertStringsToChars(strcat(aCtryList_ALL{i},"_", aItemList{37}));
    ssf_a_Name = convertStringsToChars(strcat(aCtryList_ALL{i},"_", aItemList{23}));
    ssf_i_Name = convertStringsToChars(strcat(aCtryList_ALL{i},"_", aItemList{24}));
    evalc(strcat('Adataset{''',ssfName, '''} = Adataset.', ssf_a_Name, '-Adataset.', ssf_i_Name));
end

%% ---------------------------
%  Multiply Real Potential GDP
%  ---------------------------
Adataset{'[.*potGDP_lr]'} = Adataset{'[.*potGDP_lr]'}*1000;

%% --------------------------
%  Correcting outliers
%  --------------------------
% adjustment for the outliers in Ireland
Qdataset.IE_GDP_cv(dates('2016Q4')) = (Qdataset.IE_GDP_cv(dates('2016Q3')).data+Qdataset.IE_GDP_cv(dates('2017Q1')).data)/2;

% removinging the permanent increase
temp0 = Qdataset.IE_GDP_cv;
temp1 = temp0(dates('2015Q1'):lastdate(temp0));
temp2 = temp0(firstdate(temp0):dates('2015Q1'));
temp2(dates('2015Q1')) = temp2(dates('2014Q4')).data*(temp2(dates('2014Q4')).data/temp2(dates('2014Q3')).data);
temp3 = chain(temp2,temp1);
Qdataset = merge(Qdataset, temp3);

% adjustment for the outliers in Ireland
Qdataset.IE_GDP_lr(dates('2016Q4')) = (Qdataset.IE_GDP_lr(dates('2016Q3')).data+Qdataset.IE_GDP_lr(dates('2017Q1')).data)/2;

% removinging the permanent increase
temp0 = Qdataset.IE_GDP_lr;
temp1 = temp0(dates('2015Q1'):lastdate(temp0));
temp2 = temp0(firstdate(temp0):dates('2015Q1'));
temp2(dates('2015Q1')) = temp2(dates('2014Q4')).data*(temp2(dates('2014Q4')).data/temp2(dates('2014Q3')).data);
temp3 = chain(temp2,temp1);
Qdataset = merge(Qdataset, temp3);

% adjustment for the outlier in Italy
Qdataset.IT_ginv_cv(dates('2002Q4')) = (Qdataset.IT_ginv_cv(dates('2002Q3')).data+Qdataset.IT_ginv_cv(dates('2003Q1')).data)/2;
Qdataset.GR_ginv_cv(dates('2017Q3')) = (Qdataset.GR_ginv_cv(dates('2017Q2')).data+Qdataset.GR_ginv_cv(dates('2017Q4')).data)/2;
Qdataset.ES_ginv_cv(dates('2015Q3')) = (Qdataset.ES_ginv_cv(dates('2015Q2')).data+Qdataset.ES_ginv_cv(dates('2015Q4')).data)/2;

% adjustment for outliers in the Netherlands
% we do not adjust GDP as it seems to be unaffected
Qdataset.NL_tinv_cv(dates('2015Q1')) = 2/3*Qdataset.NL_tinv_cv(dates('2014Q4')).data+1/3*Qdataset.NL_tinv_cv(dates('2015Q3')).data;
Qdataset.NL_tinv_cv(dates('2015Q2')) = 1/3*Qdataset.NL_tinv_cv(dates('2014Q4')).data+2/3*Qdataset.NL_tinv_cv(dates('2015Q3')).data;

% assuming something more reasonable for Ireland given that investment
% is shooting through the roof, we assume the growht of Austrian
% investment, another small open economy
temp1 = Qdataset.AT_tinv_cv(dates('2014Q4'):lastdate(Qdataset.AT_tinv_cv));
temp2 = Qdataset.IE_tinv_cv(firstdate(Qdataset.IE_tinv_cv):dates('2014Q4'));
temp3 = chain(temp2,temp1);
Qdataset = merge(Qdataset, temp3);

Qdataset.NL_tinv_lr(dates('2015Q1')) = 2/3*Qdataset.NL_tinv_lr(dates('2014Q4')).data+1/3*Qdataset.NL_tinv_lr(dates('2015Q3')).data;
Qdataset.NL_tinv_lr(dates('2015Q2')) = 1/3*Qdataset.NL_tinv_lr(dates('2014Q4')).data+2/3*Qdataset.NL_tinv_lr(dates('2015Q3')).data;

% assuming something more reasonable for Ireland given that investment
% is shooting through the roof, we assume the growht of Austrian
% investment, another small open economy
temp1 = Qdataset.AT_tinv_lr(dates('2014Q4'):lastdate(Qdataset.AT_tinv_lr));
temp2 = Qdataset.IE_tinv_lr(firstdate(Qdataset.IE_tinv_lr):dates('2014Q4'));
temp3 = chain(temp2,temp1);
Qdataset = merge(Qdataset, temp3);

% backcasting the series in line with the debt accumulation eq.
% gdebt(t)=gdebt(t-1)-gbb(t) -> gdebt(t-1)=gdebt(t)+gbb(t)
% this assumes DDA=0, which should be true only over time
Qdataset.FI_gdebt_cv(dates('1999q4')) = Qdataset.FI_gdebt_cv(dates('2000q1')).data + Qdataset.FI_gbbraw_cv(dates('2000q1')).data;
Qdataset.FI_gdebt_cv(dates('1999q3')) = Qdataset.FI_gdebt_cv(dates('1999q4')).data + Qdataset.FI_gbbraw_cv(dates('1999q4')).data;
Qdataset.FI_gdebt_cv(dates('1999q2')) = Qdataset.FI_gdebt_cv(dates('1999q3')).data + Qdataset.FI_gbbraw_cv(dates('1999q3')).data;
Qdataset.FI_gdebt_cv(dates('1999q1')) = Qdataset.FI_gdebt_cv(dates('1999q2')).data + Qdataset.FI_gbbraw_cv(dates('1999q2')).data;

%% ----------------------------------
%  SSC interpolation for IT
%  ----------------------------------  
lastQuarter = char(Qdataset.dates(end));
if lastQuarter(end) ~= '4'
    fill_SSC_IT
end

aSerList = Qdataset{'[.*IT_sc[hf]_cv]'}.name;
for i = 1:length(aSerList)
    evalc(['res = interp.cholette_d(Adataset.' aSerList{i} '(series_date_range_a),[dseries(0,series_date_range_q)],Qdataset.' aSerList{i}(1:end-6) 'sct_cv(series_date_range_q),1,2,4,2,0);']);
    res = res.set_names(strcat(aSerList{i}));
    Qdataset{aSerList{i}} = res;
end

%% ----------------------------------
%  SSC backcasting for DE, IE and AT
%  ----------------------------------
aSerList = Qdataset{'[.*(DE|IE|AT)_sc[hf]_cv]'}.name;
for i = 1:length(aSerList)
    evalc(['res = interp.backCholette(Adataset.' aSerList{i} ',Qdataset.' aSerList{i} ',Qdataset.' aSerList{i}(1:end-6) 'sct_cv,1,2,4,2,1);']);
    res = res.set_names(strcat(aSerList{i}));
    Qdataset{aSerList{i}} = chain(res, Qdataset{aSerList{i}});
end

%% ------------------------
%  Interpolation with BFL
%  ------------------------
aItem_BFL = aItemList(1:2);
for i = 1:numel(aItem_BFL)
        for j = 1:numel(aCtryList_ALL)
            aSerName = convertStringsToChars(strcat(aCtryList_ALL{j},"_", aItem_BFL{i}));
            series_date_range = firstobservedperiod(Adataset{aSerName}):lastobservedperiod(Adataset{aSerName});
            
             if regexp(aSerName, '.*potGDP_')
                  evalc(strcat('res = interp.bfl(Adataset.', aSerName, '(series_date_range).data,1,2,4)'));
             else
                  evalc(strcat('res = interp.bfl(Adataset.', aSerName, '(series_date_range).data,2,2,4)'));
             end
             Qdataset{aSerName} = dseries(res.y, strcat(num2str(series_date_range_a.time(1)),'Q1'), aSerName);
        end
end
%% -------------------------------------------
%  Interpolation with Cholette
%  -------------------------------------------
aItem_Cholette = aItemList([16 19 21:22 29 37]);
aIndic_Cholette = aIndic([16 19 21:22 29 37]);
for i = 1:numel(aItem_Cholette)
        for j = 1:numel(aCtryList_ALL)
            aSerName = convertStringsToChars(strcat(aCtryList_ALL{j},"_", aItem_Cholette{i}));
            aIndicName =  convertStringsToChars(strcat(aCtryList_ALL{j},"_", aIndic_Cholette{i}));
            series_date_range_yy = firstobservedperiod(Adataset{aSerName}):lastobservedperiod(Adataset{aSerName});
            series_date_range_qq = firstobservedperiod(Qdataset{aIndicName}):lastobservedperiod(Qdataset{aIndicName});
            if regexp(aSerName, '.*gempl_')
                lastYear = char(series_date_range_yy(end));
                lastQuarter = char(series_date_range_qq(end));
                if string(lastYear(1:4)) == string(lastQuarter(1:4)) && lastQuarter(end) == '4'
                     evalc(strcat('res = interp.cholette_d(Adataset.', aSerName, '(series_date_range_yy),[dseries(0,series_date_range_qq)], Qdataset.',aIndicName,'(series_date_range_qq),2,2,4,2,0);'));
                else
                     lastYear = char(series_date_range_yy(end-1));
                     lastQuarter = char(series_date_range_qq(end-str2num(lastQuarter(end))));
                     series_date_range_yy = firstobservedperiod(Adataset{aSerName}):dates(lastYear);
                     series_date_range_qq = firstobservedperiod(Qdataset{aIndicName}):dates(lastQuarter);
                     evalc(strcat('res = interp.cholette_d(Adataset.', aSerName, '(series_date_range_yy),[dseries(0,series_date_range_qq)], Qdataset.',aIndicName,'(series_date_range_qq),2,2,4,2,0);'));

                end
                
            else
                evalc(strcat('res = interp.cholette_d(Adataset.', aSerName, '(series_date_range_a),[dseries(0,series_date_range_q)], Qdataset.',aIndicName,'(series_date_range_q),1,2,4,2,0);'));
            end
            Qdataset{aSerName} = dseries(res.data,res.dates, aSerName);
        end
end

% Qdataset.save('Cholette', 'csv');

%% ************************************************************************************
% Aggregating the data for the core, periphery and the euro area
% ************************************************************************************

ser_agg_list = {'POP_lr',...
                'gcons_cv',...
                'ginv_cv',...
                'tinv_cv',...
                'tinv_lr',...
                'hhcons_cv',...
                'gtrnsf_cv',...
                'GDP_cv',...
                'GDP_lr',...
                'potGDP_lr',...
                'taxprod_cv',...
                'taxhhinc_cv',...
                'sschhinc_cv',...
                'sscfirm_cv',...
                'dtx_cv',...
                'itx_cv',...
                'sct_cv',...
                'scf_cv',...
                'sch_cv',...
                'tcomp_cv',...
                'gcomp_cv',...      
                'impssc_cv',...     
                'gbbraw_cv',...
                'nafa_cv',...
                'ctrnsf_cv',...
                'inp_cv',...
                'gdebt_cv',...
                'templ_lr',...
                'gempl_lr',...
                'ginc_cv',...
                'gstm_cv',...              
                'unempl_lr',...       
                'activepop_lr',...       
                };

for i=1:length(ser_agg_list)
	evalc(['Qdataset.CR_' ser_agg_list{i} '=Qdataset.AT_' ser_agg_list{i} '+Qdataset.BE_' ser_agg_list{i} '+Qdataset.DE_' ser_agg_list{i} '+Qdataset.FI_' ser_agg_list{i} '+Qdataset.FR_' ser_agg_list{i} '+Qdataset.NL_' ser_agg_list{i}]);
	evalc(['Qdataset.PR_' ser_agg_list{i} '=Qdataset.ES_' ser_agg_list{i} '+Qdataset.GR_' ser_agg_list{i} '+Qdataset.IE_' ser_agg_list{i} '+Qdataset.IT_' ser_agg_list{i} '+Qdataset.PT_' ser_agg_list{i}]);
	evalc(['Qdataset.EA_' ser_agg_list{i} '=Qdataset.CR_' ser_agg_list{i} '+Qdataset.PR_' ser_agg_list{i}]);
	evalc(['Qdataset.DR_' ser_agg_list{i} '=Qdataset.AT_' ser_agg_list{i} '+Qdataset.BE_' ser_agg_list{i} '+Qdataset.DE_' ser_agg_list{i} '+Qdataset.FI_' ser_agg_list{i} '+Qdataset.FR_' ser_agg_list{i} '+Qdataset.NL_' ser_agg_list{i} '+Qdataset.ES_' ser_agg_list{i} '+Qdataset.GR_' ser_agg_list{i} '+Qdataset.IE_' ser_agg_list{i} '+Qdataset.IT_' ser_agg_list{i} '+Qdataset.PT_' ser_agg_list{i}]);
end