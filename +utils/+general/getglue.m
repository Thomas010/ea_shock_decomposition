function glue = getglue( )

if isappdata(0, 'PRP_Glue')
    glue = getappdata(0, 'PRP_Glue');
else
    glue = '_';
end

end%

