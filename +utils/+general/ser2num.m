function [dbn, ind] = ser2num(dbs, range)

num = db2array(dbs);
fn  = fieldnames(dbs);
for i = 1:length(fn)
  dbn.(fn{i}) = num(:,i);
end

dbr = dbrange(dbs);
ind = find(dbr == range(1)) : find(dbr == range(end));