function y = dbsumprod(dbin1, dbin2, names1, names2, shift)

if nargin < 5
  shift = 0;
end

n = length(names1);

for i = 1:n
  if i == 1
    y = dbin1.(names1{i}) * dbin2.(names2{i}){shift};
  else
    y = y + dbin1.(names1{i}) * dbin2.(names2{i}){shift};
  end
end