function dbout = dbfcopy(dbsin, dbout, varnamesin, varnamesout, fun)

if isstruct(dbsin) || isa(dbsin, 'Dictionary')
    dbsin = {dbsin};
end

if nargin < 3 || isempty(varnamesin)
    varnamesin = fieldnames(dbsin{1});
end

if nargin < 4 || isempty(varnamesout)
    varnamesout = varnamesin;
end

varnamesin = cellstr(varnamesin);
varnamesout = cellstr(varnamesout);

if isscalar(dbsin) && size(varnamesin, 2) > 1
    dbsin = repmat(dbsin, 1, size(varnamesin, 2));
end

if nargin < 5 % No function applied, simply copy

    % With no functions, there can only be one dbsin
    for i = 1:length(varnamesout)
        dbout.(varnamesout{i}) = dbsin{1}.(varnamesin{i});
    end

else

    for i = 1:length(varnamesout)
        args = cell(length(dbsin), 1);
        for j = 1:length(dbsin)
            args{j} = dbsin{j}.(varnamesin{i,j});
        end
        dbout.(varnamesout{i}) = fun(args{:});
    end

end
