function names = wildlist(dbin, rexp)

if ischar(rexp)
  rexp = {rexp};
end

fn = fieldnames(dbin);

names = {};
for i = 1:length(rexp)
  ind   = regexp(fn, rexp{i});
  ind   = ~cellfun(@isempty, ind);
  names = [names; fn(ind)];
end
