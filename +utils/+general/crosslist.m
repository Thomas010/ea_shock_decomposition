function names = crosslist(glue, varargin)

for i = 1:length(varargin)
  if isnumeric(varargin{i})
    varargin{i} = cellstr(int2str(varargin{i}(:)));
  elseif ischar(varargin{i})
    varargin{i} = {varargin{i}};
  end
end

N   = length(varargin);
LL  = cellfun(@length, varargin);

names = cell(prod(LL), 1);

cntr = 0;
ind  = ones(N,1);

while true
  
  for i = 1:N
    if 1 == i
      tmp = varargin{i}{ind(i)};
    else
      tmp = [tmp, glue, varargin{i}{ind(i)}];
    end
  end
  
  cntr = cntr + 1;
  names{cntr} = tmp;
  
  nind = nextind(ind, LL);
  if all(nind == ind)
    break
  else
    ind = nind;
  end
  
end

if length(names) == 1
  names = names{:};
end

function ind = nextind(ind, LL)

for i = length(ind) : -1 : 1
  if ind(i) < LL(i)
    ind(i) = ind(i) + 1;
    ind(i+1:end) = 1;
    break
  end
end