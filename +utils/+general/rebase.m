function y = rebase(x, yr)

if get(x,'freq') == 4
  range = qq(yr,1):qq(yr,4);
else
  range = yr;
end

y = 100 * x / mean(x{range});