function y = extend(x, Range, Method, byWhat)

fd = get(x, 'first');
ld = get(x, 'last');

if isa(Range, 'DateWrapper')
  
  if Range(1) < fd
    dir = 'backw';
    Range = Range(1) : fd-1;
  elseif ld < Range(end)
    dir = 'forw';
    Range = ld+1 : Range(end);
  else
    y = x;
    return
  end
  
elseif ischar(Range)
  
  % Add here a check that byWhat is a series
  
  switch Range
    case 'backw'
      Range = get(byWhat, 'first') : fd-1;
      dir = 'backw';
    case 'forw'
      Range = ld+1 : get(byWhat, 'last');
      dir = 'forw';
  end
  
end

switch Method
  
  case 'const'
    
    switch byWhat
      
      case 'edge'
        
        switch dir
          case 'backw'
            y = [Series(Range, x(fd)); x];
          case 'forw'
            y = [x; Series(Range, x(ld))];
        end
        
      case 'mean'
        
        switch dir
          case 'backw'
            y = [Series(Range, mean(x)); x];
          case 'forw'
            y = [x; Series(Range, mean(x))];
        end
        
    end
    
  case 'pct'
    
    if ischar(byWhat)
      
      y = pct(x);
      
      switch byWhat
        
        case 'edge'
          
          switch dir
            case 'backw'
              y = apply(@(x,t,y) x(t+1) / (1 + y(t+1)/100), x, fliplr(Range), Series(Range+1, y(fd+1)));
            case 'forw'
              y = apply(@(x,t,y) x(t-1) * (1 + y(t)/100),   x, Range, Series(Range, y(ld)));
          end
          
        case 'mean'
          
          switch dir
            case 'backw'
              y = apply(@(x,t,y) x(t+1) / (1 + y(t+1)/100), x, fliplr(Range), Series(Range+1, mean(y)));
            case 'forw'
              y = apply(@(x,t,y) x(t-1) * (1 + y(t)/100),   x, Range, Series(Range, mean(y)));
          end
          
      end
      
    elseif isa(byWhat, 'Series')
      
      switch dir
        case 'backw'
          y = apply(@(x,t,y) x(t+1) / (1 + y(t+1)/100), x, fliplr(Range), byWhat);
        case 'forw'
          y = apply(@(x,t,y) x(t-1) * (1 + y(t)/100),   x, Range, byWhat);
      end
      
    end
    
  case 'diff'
    
    if ischar(byWhat)
      
      y = diff(x);
      
      switch byWhat
        
        case 'edge'
          
          switch dir
            case 'backw'
              y = apply(@(x,t,y) x(t+1) - y(t+1), x, fliplr(Range), Series(Range+1, y(fd+1)));
            case 'forw'
              y = apply(@(x,t,y) x(t-1) + y(t),   x, Range, Series(Range, y(ld)));
          end
          
        case 'mean'
          
          switch dir
            case 'backw'
              y = apply(@(x,t,y) x(t+1) - y(t+1), x, fliplr(Range), Series(Range+1, mean(y)));
            case 'forw'
              y = apply(@(x,t,y) x(t-1) + y(t),   x, Range, Series(Range, mean(y)));
          end
          
      end
      
    elseif isa(byWhat, 'Series')
      
      switch dir
        case 'backw'
          y = apply(@(x,t,y) x(t+1) - y(t+1), x, fliplr(Range), byWhat);
        case 'forw'
          y = apply(@(x,t,y) x(t-1) + y(t),   x, Range, byWhat);
      end
      
    end
    
  case 'ypct'
    
    if ischar(byWhat)
      
      error('Please provide a series of year-on-year growth rates for the "ypct" extension method.')
      
    elseif isa(byWhat, 'Series')
      
      switch dir
        case 'backw'
          error('The "ypct" extension method can only be used for forward extension.')
        case 'forw'
          p = x.FrequencyAsNumeric;
          switch p
            case 12
              y = apply(@(x,t,y) x(t-12)  * (1 + y(t)/100), x, Range, byWhat);
            case 4
              y = apply(@(x,t,y) x(t-4)   * (1 + y(t)/100), x, Range, byWhat);
          end
      end
      
    end
    
end