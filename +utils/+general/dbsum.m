function y = dbsum(db, names, ignore)

if nargin < 3
  ignore = false;
end

if nargin < 2
  names = fieldnames(db);
end

if ignore
  
  c = 0;
  for i = 1:length(names)
    if isfield(db, names{i})
      c = c + 1;
      if c == 1
        y = db.(names{i});
      else
        y = y + db.(names{i});
      end
    end
  end
  
else
  
  for i = 1:length(names)
    if i == 1
      y = db.(names{i});
    else
      y = y + db.(names{i});
    end
  end
  
end