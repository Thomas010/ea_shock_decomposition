function dbout = dbjoin(dbin1, dbin2, dbout, varnames)

for i = 1:size(varnames, 1)
  dbout.(varnames{i,3}) = [dbin1.(varnames{i,1}); dbin2.(varnames{i,2})];
end