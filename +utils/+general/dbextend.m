function dbin = dbextend(dbin, varnames, direction, targdate, method)

if isempty(varnames)
  varnames = fieldnames(dbin);
end

if ischar(varnames)
  varnames = {varnames};
end

for i = 1:length(varnames)
  
  vari = dbin.(varnames{i});
  
  switch direction
    
    case 'backward'
      
      if length(targdate) == 1
        d0 = targdate(1);
        d1 = get(vari, 'first');
      else
        d0 = targdate(1);
        d1 = targdate(end) + 1;
      end
      
      if d0 < d1
        
        ld = get(vari, 'last');
        
        tmp   = vari(d1:ld);
        tmp0  = [nan(d1 - d0,1); tmp(1)];
        
        switch method
          
          case 'constant'
            tmp0(:) = tmp0(end);
          case 'growth'
            gr = nanmean(pct(vari));
            for t = length(tmp0)-1: -1 : 1
              tmp0(t) = tmp0(t+1) / (1 + gr/100);
            end
          otherwise
            error('Wrong method')
            
        end
        
        tmp = [tmp0(1:end-1); tmp];
        dbin.(varnames{i}) = Series(d0, tmp);
        
      end
      
    case 'forward'
      
      if length(targdate) == 1
        d0 = get(vari, 'last');
        d1 = targdate(1);
      else
        d0 = targdate(1) - 1;
        d1 = targdate(end);
      end
      
      if d0 < d1
        
        fd = get(vari, 'first');
        
        tmp   = vari(fd:d0);
        tmp0  = [tmp(end); nan(d1 - d0,1)];
        
        switch method
          
          case 'constant'
            tmp0(:) = tmp0(1);
          case 'growth'
            gr = nanmean(pct(vari{fd:d0}));
            for t = 2 : length(tmp0)
              tmp0(t) = tmp0(t-1) * (1 + gr/100);
            end
          otherwise
            error('Wrong method')
            
        end
        
        tmp = [tmp; tmp0(2:end)];
        dbin.(varnames{i}) = Series(fd, tmp);
        
      end
      
    otherwise 
      
      error('Wrong direction')
      
  end
  
end
