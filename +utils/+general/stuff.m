function str = stuff(str, start, remove, insert)

str = [str(1:start-1), str(start+remove:end)];

str = [str(1:start-1), insert, str(start:end)];