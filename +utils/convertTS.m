function TS = convertTS(ts,varargin)

    defaultDirection = 'tseries'; % converts from dseries object to tseries object
    p = inputParser;
    addOptional(p,'direction',defaultDirection,@ischar);
    parse(p,varargin{:});
    
    switch p.Results.direction
        case 'tseries'
            TS = tseries(char(strings(firstdate(ts))),ts.data,ts.name);
        case 'dseries'
            if class(ts) == 'Series'
                TS = dseries(ts.data, dates(char(DateWrapper.toCellstr(ts.start))), char(ts.Comment));
            else
                all_fields = fields(ts);
                start_date = dates(char(DateWrapper.toCellstr(ts.(all_fields{1}).start)));
                TS = dseries(NaN(1,length(fieldnames(ts))), start_date);
                for i = 1:length(fieldnames(ts))
                    column = strcat('Variable_',num2str(i));
                    TS{column} = dseries(ts.(all_fields{i}).data, dates(char(DateWrapper.toCellstr(ts.(all_fields{i}).start))));
                    TS       = TS.rename(column,all_fields{i});
                end
            end
            
                    
    end
end

