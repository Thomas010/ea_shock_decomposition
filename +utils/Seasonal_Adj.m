function [AdjustedTS name] = Seasonal_Adj(ts)
% addpath('D:\Software\4.6-unstable~ecb-e3f1ea7e3cb2d6961d3225dc57d9fba40b54267b\matlab');
% dynare_config;

tmp = ts;

    if nansum(tmp.qdiff.data)~=0
        o = x13(tmp);
        o.transform('function','auto');
        o.regression('variables','()');
        o.outlier('types', '(AO LS)');
        o.forecast('maxlead', 6, 'print', 'none', 'save', 'FCT');
        o.estimate('print','(roots regcmatrix)');
        o.automdl('savelog','amd');
        o.x11('seasonalma','MSR', 'save', 'd11');
        
        o.run();
        o.clean();
         r_X13 = o.results;
         if isfield(r_X13,'d11')
             AdjustedTS=r_X13.d11;  %seasonal adj.
             name = r_X13.name;
         else
             disp(['WARNING: ' tmp.name ' was not SA!'])
             AdjustedTS=tmp;
         end
         o.clean();
         clear o;
     else
         disp(['INFO: ' tmp.name ' was not SA as it is constant...'])
         AdjustedTS=tmp;
    end
     
% addpath('D:\Software\4.6_latest\matlab');
% dynare_config;

end

