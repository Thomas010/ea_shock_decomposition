function dbFreq = aggregateAcrossAreas(dbFreq, aggregateArea, selectCountries, selectItems)

for item = selectItems
    dbFreq.(aggregateArea).(item) = 0;
    for country = setdiff(selectCountries, aggregateArea)
        dbFreq.(aggregateArea).(item) = dbFreq.(aggregateArea).(item) + dbFreq.(country).(item);
    end
end

end%

