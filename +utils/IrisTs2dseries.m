function TS = IrisTs2dseries(ts,frequency)
dates_ = databank.range(ts, "Frequency=", frequency);
names =  databank.filter(ts, "Filter=", @(x) isa(x, "Series") && x.Frequency == frequency);
data =   databank.toDoubleArray(ts, names, dates_);
TS = dseries(data, dates(dat2char(dates_(1))), names);
end