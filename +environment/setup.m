function environment = setup(varargin)

environment = struct( );
environment.Main = locallySetup( );

end%


function main_table = locallySetup( )
    main_table = readtable( ...
        '+environment/interpolation.xlsx' ...
        , 'ReadRowNames', true ...
        , 'TextType', 'string' ...
    );
end%

